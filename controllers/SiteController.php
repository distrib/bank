<?php
namespace app\controllers;

use app\models\User;
use app\modules\menu\actions\DownloadFileAction;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\UserProfile;
use app\models\forms\SignupForm;
use app\models\search\CardSearch;
use app\models\Card;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [
                    'signup', 'signin', 'preloader', 'download', 'bug-messages', 'request-password-reset',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'download'=>[
                'class'=>DownloadFileAction::className()
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $card_number = randString( 16, array("0123456789"));
        $pin = randString( 4, array("0123456789"));

        $card = Card::findOne(['card_number'=>$card_number]);
        while( $card ){
            $card_number = randString( 16, array("0123456789"));
            $card = Card::findOne(['card_number'=>$card_number]);
        }

        $card = new Card();
        $card->user_id = Yii::$app->user->identity->getId();
        $card->card_number = $card_number;
        $card->month = rand( 1, 12);
        $card->year = 2020;
        $card->pin = Yii::$app->security->generatePasswordHash($pin);
        $card->save();

        return $this->render('requestWindowInner',[
            'title' => 'PIN-код: '.$pin.' Номер карты: '.$card->card_number,
            'text' => 'Обычно здесь шлеться письмо на почту пользователю. Упрощаем этот шаг и показываем здесь.',
            'link' => '/',
            'class' => 'registrated',
            'linkText' => 'Ок',
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionPreloader()
    {
        $this->layout = 'dark';
        return $this->render('preloader');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionSignin()
    {
        $this->layout = 'dark';

        if (!Yii::$app->user->isGuest) {
            $this->redirect('/');
            //return 'You are already sign in';
        }

        $model = new LoginForm();
        if( $model->load(Yii::$app->request->post()) ) {

            if( $model->validate() && $model->login() ){

                $this->redirect('/');
            }else{

                $errorCopy = $model->errors;
                $errorStr = array_shift($errorCopy)[0];

                return $this->render('requestWindow',[
                    'title' => 'Ошибка!',
                    'text' => $errorStr,
                    'link' => '/signin'
                ]);
            }

        } else {

            $model->password = '';
            return $this->render('signin', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = 'dark';

        if (!Yii::$app->user->isGuest) {
            $this->redirect('/');
        }

        $model = new SignupForm();
        $profile = new UserProfile();

        if ($model->load(Yii::$app->request->post())) {

            $signupRes = $model->signup();

            if( count($model->errors) ) {

                return $this->render('requestWindow', [
                    'title' => array_shift($model->errors),
                    'text' => 'Пожалуйста, проверьте корректность данных или обратитесь в службу поддержки',
                    'link' => '/signup',
                    'class' => 'vin',
                ]);

            }else {

                return $this->render('requestWindow',[
                    'title' => 'Пароль: 123456789',
                    'text' => 'Обычно здесь шлеться письмо с ссылкой для активации учетной записи и установки пароля из кабинета. 
                        Так как возможны проблемы отсылки почтовых сообщений в связи с правилами политики безопасности компаниии
                        - упрощаем этот шаг',
                    'link' => '/',
                    'class' => 'registrated',
                ]);

            }
        }

        $profile->gender = $profile::GENDER_MALE;

        return $this->render('signup', [
            'model' => $model,
            'profile' => $profile,
            'mError' => $model->errors,
        ]);
    }

    public function actionBugMessages()
    {
        $this->layout = 'dark';

        return $this->render('requestWindow',[
            'title' => 'Обычно здесь форма обратной связи.',
            'text' => 'В рамках данной задачи она отсутствует.',
            'link' => '/',
            'class' => 'bug-messages',
        ]);

    }

    public function actionRequestPasswordReset()
    {
        $this->layout = 'dark';

        return $this->render('requestWindow',[
            'title' => 'Обычно здесь форма сброса пароля.',
            'text' => 'Функциональность убрана в связи с возможным отсутствием возможности почтовых рассылок',
            'link' => '/',
            'class' => 'bug-messages',
        ]);

    }

}
