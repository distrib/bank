<?php
namespace  app\controllers\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;
use yii\helpers\Url;

class AccessBehavior extends Behavior{

    public  function events()
    {
        //return parent::events();
        return [
            Controller::EVENT_BEFORE_ACTION => 'checkAccess'
        ];
    }

    public function checkAccess()
    {
        if( Yii::$app->user->isGuest ){

            Yii::$app->response->redirect(['/']);
            return false;
        }
    }
}