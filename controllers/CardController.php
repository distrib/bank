<?php
namespace app\controllers;

use app\modules\menu\actions\DownloadFileAction;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Card;
use app\models\CashFlow;
use app\models\search\CashFlowSearch;
use yii\helpers\ArrayHelper;


/**
 * Site controller
 */
class CardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'download'=>[
                'class'=>DownloadFileAction::className()
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionAdd($id)
    {
        $model = new CashFlow();

        //Yii::$app->user->identity->getId();

        if( $model->load(Yii::$app->request->post()) ){
            $model->sender = Card::getDefaultCard()->id;
            $model->recipient = $id;

            //echo "<pre>"; var_dump($model); echo "</pre>";
            if( $model->save() ){
                return $this->render('../site/requestWindowInner',[
                    'title' => 'Счет пополнен',
                    'text' => 'Для пополнения не требуется  ввод pin-кода',
                    'link' => '/',
                    'class' => 'registrated',
                    'linkText' => 'Ок',
                ]);
            }
        }

        return $this->render('add', [
            'model' => $model,
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionMinus($id)
    {
        $model = new CashFlow();
        $card = new Card();
        $error = false;
        //Yii::$app->user->identity->getId();

        if( $model->load(Yii::$app->request->post()) && $card->load(Yii::$app->request->post()) ){
            // TO-DO при рефакторинге сделать агрегированный метод проверок по карте с выдачей ошибок
            // TO-DO сделать в моделе метод перевода средств между картами
            if( Card::isThisUserCard($id, Yii::$app->user->identity->getId()) )
            {
                if( Card::checkPinByCard($id, $card->pass) )
                {
                    $model->sender = $id;
                    $model->recipient = Card::getDefaultCard()->id;

                    $total = CashFlow::getTotalByCard($id);
                    if ($total) {
                        $minus = $model->sum;
                        $afterTotal = $total["total"] - $minus;

                        if ($afterTotal > 0 || $afterTotal === 0) {
                            if ($model->save()) {
                                return $this->render('../site/requestWindowInner', [
                                    'title' => 'Со счета сняты деньги',
                                    'text' => '',
                                    'link' => '/',
                                    'class' => 'registrated',
                                    'linkText' => 'Ок',
                                ]);
                            }
                        }
                        else $error = 'Недостаточно средств. Баланс: ' . $total["total"] . '$';
                    }
                    else $error = 'Недостаточно средств';
                }
                else $error = 'Неверный pin';
            }
            else $error = 'Вы не являетесь владельцем карты';
        }

        return $this->render('minus', [
            'model' => $model,
            'error' => $error,
            'card' => $card,
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionHistory($id)
    {
        $searchModel = new CashFlowSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionSend($id)
    {
        $model = new CashFlow();
        $card = new Card();
        $error = false;
        //Yii::$app->user->identity->getId();

        if( $model->load(Yii::$app->request->post()) && $card->load(Yii::$app->request->post()) ){
            // TO-DO при рефакторинге сделать агрегированный метод проверок по карте с выдачей ошибок
            // TO-DO сделать в моделе метод перевода средств между картами
            if( Card::isThisUserCard($id, Yii::$app->user->identity->getId()) )
            {
                if( Card::checkPinByCard($id, $card->pass) )
                {
                    $model->sender = $id;
                    $model->recipient = $card->card_number;

                    $total = CashFlow::getTotalByCard($id);
                    if ($total) {
                        $minus = $model->sum;
                        $afterTotal = $total["total"] - $minus;

                        if ($afterTotal > 0 || $afterTotal === 0) {
                            if ($model->save()) {
                                return $this->render('../site/requestWindowInner', [
                                    'title' => 'Деньги переведены',
                                    'text' => '',
                                    'link' => '/',
                                    'class' => 'registrated',
                                    'linkText' => 'Ок',
                                ]);
                            }
                        }
                        else $error = 'Недостаточно средств. Баланс: ' . $total["total"] . '$';
                    }
                    else $error = 'Недостаточно средств';
                }
                else $error = 'Неверный pin';
            }
            else $error = 'Вы не являетесь владельцем карты';
        }

        $cardList = ArrayHelper::map(Card::find()->where(['<>', 'id', $id])->all(),'id','card_number');

        return $this->render('send', [
            'model' => $model,
            'error' => $error,
            'card' => $card,
            'cardList' => $cardList,
        ]);
    }
}
