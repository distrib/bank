<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AdditionalAsset extends AssetBundle
{
    public $sourcePath = '@npm';

    public $css = [
        //'bootstrap/dist/css/bootstrap-theme.min.css',
        '//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css',
        '//malihu.github.io/custom-scrollbar/mCSB_buttons.png',
    ];
    public $js = [
        'bootstrap/dist/js/bootstrap.bundle.min.js',
        '//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
