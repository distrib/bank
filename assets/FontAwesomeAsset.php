<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/Font-Awesome';

    public $css = [
        'css/font-awesome.min.css'
    ];
    public $js = [

    ];
}
