<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class InputmaskAsset extends AssetBundle
{
    public $sourcePath = '@bower/inputmask';

    public $css = [];
    public $js = [
        'dist/jquery.inputmask.bundle.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',
    ];
}
