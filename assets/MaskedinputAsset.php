<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MaskedinputAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery.maskedinput';

    public $css = [];
    public $js = [
        'src/jquery.maskedinput.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
