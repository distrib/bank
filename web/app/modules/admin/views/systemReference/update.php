<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SystemReference */

$this->title = 'Update System Reference: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'System References', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="system-reference-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
