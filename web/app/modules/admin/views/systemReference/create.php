<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SystemReference */

$this->title = 'Create System Reference';
$this->params['breadcrumbs'][] = ['label' => 'System References', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-reference-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
