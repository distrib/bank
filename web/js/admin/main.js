var MRZ = MRZ || {};

MRZ.content = {
    get: function get(modal, params) {

        if( params.windowId.length > 0 ){

            var action = params.windowId;
            if(this.hasOwnProperty(action)){
                $result = this[action](modal,params);
                return $result;
            }else{
                data = 'No modal window with this name';
                data = this.closeWrapper(data);
                modal.find('.modal-body').empty();
                modal.find('.modal-body').append(data);
                return false;
            }

        }else{
            data = 'Not set modal window to show';
            data = this.closeWrapper(data);
            modal.find('.modal-body').empty();
            modal.find('.modal-body').append(data);

            return false;
        }
    },

    closeWrapper: function closeWrapper (str){
        var close = '<button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
            '                        <span aria-hidden="true">&times;</span>\n' +
            '                    </button>';

        return close + str;
    },

    userCarsEdit: function userCarsEdit (modal,params){
        //var carId = href.substring(href.indexOf("=")+1, href.length);
        var obj = this;
        var outData = '';

        $.post(params.href, outData,function( data ) {
            if( !data.length ){
                data = 'Error getting windows';
            }

            data = obj.closeWrapper(data);
            modal.find('.modal-body').empty();
            modal.find('.modal-body').append(data);
        });

        return true;
    },

    userCarsSubmit: function userCarsSubmit(modal,params) {
        var obj = this;

        if( params.form ){
            outData = params.form;
        }else{
            outData = '';
        }

        $.post(params.href, outData,function( data ) {
            if( data.success ){
                var url;
                if( !location.href.indexOf("tab_") ){
                    url = location.href;
                }else{
                    url = location.href+'&tab_2=active';
                }
                location.href = url;

                //location.href = location.pathname+'/?tab_2=active'
                //location.reload();
            }else{
                if( !data.length ){
                    data = 'Error getting windows';
                }

                data = obj.closeWrapper(data);
                modal.find('.modal-body').empty();
                modal.find('.modal-body').append(data);
            }
        });

        return true;
    }
};

$(document).ready(function(){

    $(document).on("click", '#adm-cars a[aria-label="Редактировать"]', function(event){
        event.preventDefault();

        var button = $(event.currentTarget);
        var href = button[0].href;
        var modal = $('#allModal');
        var params = {};

        params.windowId = 'userCarsEdit';
        params.href = href;

        $('#allModal').modal('show');
        modal.find('.modal-dialog').attr("window-id", params.windowId);

        MRZ.content.get(modal,params);
    });

    $(document).on("submit", '#adm-cars-edit', function(event){
        event.preventDefault();

        var form = $(event.currentTarget);
        var params = {};
        params.form = form.serializeArray();
        params.windowId = form.data('window-id');
        params.href = form[0].action;
        var modal = form.parents('.modal');

        MRZ.content.get(modal,params);
    });

    $('#allModal').on('show.bs.modal', function(event) {
        /*var button = $(event.relatedTarget);
        var params = button.data();
        var modal = $(this);
        modal.find('.modal-dialog').attr("window-id", params.windowId);

        MRZ.content.get(modal,params);*/
    });

});

