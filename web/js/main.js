var MRZ = MRZ || {};

MRZ.content = {
    get: function get(modal, params) {

        modal.find('.modal-body').empty();

        if( params.windowId.length > 0 ){
            var action = params.windowId;
            if(this.hasOwnProperty(action)){
                var result = this[action](modal,params);
                return result;
            }else{
                var data = 'No modal window with this name';
                data = this.closeWrapper(data);

                modal.find('.modal-body').append(data);
                return false;
            }

        }else{
            var data = 'Not set modal window to show';
            data = this.closeWrapper(data);
            modal.find('.modal-body').append(data);

            return false;
        }
    },

    closeWrapper: function closeWrapper (str){
        var close = '<button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
            '                        <span aria-hidden="true">&times;</span>\n' +
            '                    </button>';

        return close + str;
    },

    editProfile: function editProfile(modal,params) {
      ajaxGetParams(params.type, params.key, params.targetProfile,modal);
    },

    deleteProfile: function deleteProfile(modal,params) {
        $.ajax({
            type: 'POST',
            url: '/cabinet/delete_self/',
            data: params,
            //dataType: "json",
            success: function (data) {
                modal.find('.modal-body').html(data);
            },
            error: function (error) {
                //console.log(error);
            }
        });
    },

    notifications: function notifications(modal,params) {
        $.ajax({
            type: 'POST',
            url: '/ajax/get-notifications',
            data: params,
            //dataType: "json",
            success: function (data) {
                console.log(data);
                modal.find('.modal-body').html(data.html);

                $(".modal-notification-wrapper").mCustomScrollbar();
            },
            error: function (error) {
                //console.log(error);
            }
        });
    },

    inactiveNotification: function notifications(modal,params) {
        $.ajax({
            type: 'POST',
            url: '/ajax/inactive-notification',
            data: params,
            //dataType: "json",
            success: function (data) {
                //console.log(data);
                //modal.find('.modal-body').html(data.html);
            },
            error: function (error) {
                //console.log(error);
            }
        });
    },

};

function ajaxGetCards() {
    $.ajax({
        type: 'POST',
        url: '/ajax/get-cards',

        dataType: "json",

        success: function (data) {
            $('#privateData-card').html(data.privateData);
            $('#profile-card').html(data.profile);
        },
        error: function (error) {
            console.log(error);

        }
    });
}

function ajaxGetClearFields() {
    $.ajax({
        type: 'POST',
        url: '/ajax/get-clear-fields',

        dataType: "json",

        success: function (data) {
            $('.div-clear-fields').html(data.clear);
        },
        error: function (error) {
            console.log(error);

        }
    });
}

function FirstVisit() {
    if ($('.cabinet-interests.mobile').length > 0) {
        $.ajax({
            type: 'POST',
            url: '/ajax/set-first-visit',

            dataType: "json",

            success: function (data) {
                setTimeout(function(){
                    var html = $('.cabinet-interests.mobile').detach();
                    $('#allModal').find('.modal-body').append(html);
                    $('#allModal').find('.modal-dialog').attr('window-id','interests');
                    $('#allModal').find('.cabinet-interests.mobile').show();
                    $('#allModal').modal('show');
                }, 4000);
            },
            error: function (error) {
                console.log(error);

            }
        });
    }
}

function ajaxInterests(formData) {
    $.ajax({
        type: 'POST',
        url: '/ajax/set-interests',
        data: formData,
        dataType: "json",

        success: function (data) {
            $('#allModal').find('.modal-body').empty();
            $('#allModal').find('.modal-dialog').attr('window-id','notifications');
            $('#allModal').find('.modal-body').append(data);
        },
        error: function (error) {
            console.log(error);

        }
    });
}

function ajaxSaveParams(form,modal) {

    $.ajax({
        type: 'POST',
        url: '/ajax/save-data',
        data: form,
        dataType: "json",

        success: function (data) {

            if (data.status == 200) {

                if (modal) modal.modal('hide');
                ajaxGetCards();
                ajaxGetClearFields();
                //TODO тут обновить блоки с данными
            } else {

                $('#profile-form-error-id').html(data.message);
            }

        },
        error: function (error) {
            console.log(error);

        }
    });
}

function checkNotifications() {

    $.ajax({
        type: 'POST',
        url: '/ajax/get-notification-count',
        dataType: "json",

        success: function (data) {

            if ($('#activities-notification-href').length > 0) {

                if(data == 1){
                    var nnotifications = 'уведомление';
                }else if( data>1 && data<5 ) {
                    var nnotifications = 'уведомления';
                }else{
                    var nnotifications = 'уведомлений';
                }

                $('#activities-notification-href').html('<span>'+data+'</span> '+nnotifications);

            }

            var avatar = $('.avatar');
            var mobile = $('.header-menu__btn');

            if( parseInt(data)>0 ){
                if( !avatar.hasClass('messaged') ){
                    avatar.addClass('messaged');
                }

                if( !mobile.hasClass('messaged') ){
                    mobile.addClass('messaged');
                }
            }else{
                avatar.removeClass('messaged');
                mobile.removeClass('messaged');
            }
        },
        error: function (error) {
            console.log(error);

        }
    });
}

function ajaxGetParams(type, key, targetProfile, modal) {

    $.ajax({
        type: 'POST',
        url: '/ajax/get-popup-content',
        data: {
            type: type,
            key: key,
            targetProfile: targetProfile,

        },
        dataType: "json",

        success: function (data) {
            modal.find('.modal-body').html(data.html);
        },
        error: function (error) {
            console.log(error);

        }
    });

}

function GetNotificationModal() {
    var modal = $('#allModal');
    var counterElem = $('#activities-notification-href');
    var counter = parseInt(counterElem.find('span').html());

    modal.find('.modal-dialog').attr("window-id", 'notifications');

    needModal = counterElem.find('span').data('notification');
    if( needModal ){
        if( counter ){
            MRZ.content.notifications(modal,[]);
            $("#allModal").modal('show');
        }
    }
}

$(document).ready(function(){

    $(document).on("click", '#sign-up input[name="agreement"]', function(event){
        if( $(this).is(':checked') ){
            $(this).parents('#sign-up').find('button[type="submit"]').attr('disabled',false);
            $(this).parents('#sign-up').find('.agreement-wrapper').removeClass('not-checked');
        }else{
            $(this).parents('#sign-up').find('button[type="submit"]').attr('disabled',true);
            $(this).parents('#sign-up').find('.agreement-wrapper').addClass('not-checked');
        }
    });

    $(document).on("click", '#reset-password-form input[name="agreement"]', function(event){
        if( $(this).is(':checked') ){
            $(this).parents('#reset-password-form').find('button[type="submit"]').attr('disabled',false);
            $(this).parents('#reset-password-form').find('.agreement-wrapper').removeClass('not-checked');
        }else{
            $(this).parents('#reset-password-form').find('button[type="submit"]').attr('disabled',true);
            $(this).parents('#reset-password-form').find('.agreement-wrapper').addClass('not-checked');
        }
    });

    $('#profile-clear-fields .sub-item-wrapper').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $('.events-card-page .images').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true
    });

    $('.special-offerings-card__image .images').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true
    });

    if( $('.cabinet-cards .my-car__list .my-car__item').length>1 ){
        $('.cabinet-cards .my-car__list').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false
        });
    }

    $(document).on("click", '.cabinet-interests .card', function(event){
        var parent = $(this).parents('.cabinet-interests');
        var id = $(this).data('id');

        parent.find('.card[data-id="'+id+'"]').each(function() {
            $(this).toggleClass('active');
            if ($(this).hasClass('active'))
                $(this).find('input').val(id);
            else
                $(this).find('input').val('');
        });
    });

    $(document).on("click", 'a.slow', function(event){
        var id  = $(this).attr('href');

        if( id[0] == '#' ){
            event.preventDefault();

            var id  = $(this).attr('href');
            var top = $(id).offset().top;

            $('body,html').animate({scrollTop: top}, 1500);
        }
    });

    $(".phone-mask").mask("+7 999 999-99-99");

    $(document).on("click", '.signup-opener, .signin-opener', function(event){
        var button = $(event.currentTarget);
        var params = button.data();
        var modal = button.parents('.modal');
        modal.find('.modal-dialog').attr("window-id", params.windowId);

        MRZ.content.get(modal,params);
    });

    $('#allModal').on('show.bs.modal', function(event) {
        if(event.relatedTarget != undefined){
            var button = $(event.relatedTarget);
            var params = button.data();
            var modal = $(this);
            modal.find('.modal-dialog').attr("window-id", params.windowId);

            MRZ.content.get(modal,params);
        }
    });

    $('#allModal').on('submit', '#modal-form-profile', function(event) {
        event.preventDefault();

        ajaxSaveParams($(this).serialize(),$('#allModal'));
    });


    $('#allModal').on('submit', '#modal-first-interests-form', function(event) {
        event.preventDefault();

        ajaxInterests($(this).serialize());
    });

    $('.div-clear-fields').on('submit', '#profile-clear-fields', function(event) {
        event.preventDefault();

        ajaxSaveParams($(this).serialize(),false);


    });

    $('.header-menu__btn').on('click', function(){
        $('body').toggleClass('mobile-menu__visible');
    })

    $(document).on("click", '.modal-notification-wrapper .mn-category', function(event){
        event.preventDefault();

        $(this).toggleClass('opened');
        var arrow = $(this).find('i');
        if( arrow.hasClass('fa-chevron-down') ){
            arrow.removeClass('fa-chevron-down').addClass('fa-chevron-right');
        }else{
            arrow.removeClass('fa-chevron-right').addClass('fa-chevron-down');
        }
        $(this).parents('.mn-category-wrapper').find('.items').toggle();
    });

    $(document).on("click", '.modal-notification-wrapper .clear', function(event){
        event.preventDefault();

        var modal = $('#allModal');
        var params = {};
        params.id = $(this).data('id');

        MRZ.content.inactiveNotification(modal,params);

        if( $(this).parents('.mn-category-wrapper').find('.item').length>1 ){
            $(this).parent().remove();
        }else{
            $(this).parents('.mn-category-wrapper').remove();
        }

        checkNotifications();
    });

    FirstVisit();
    GetNotificationModal();

    $(".special-offerings-card__text .text").mCustomScrollbar();
    $(".event-card__text .text").mCustomScrollbar();
});