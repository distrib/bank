<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="card-form-add">
    <div class="container">
        <div class="row">
            <div class="col-4 align-self-center mr-auto ml-auto">

                <?php $form = ActiveForm::begin([
                    'id' => 'card-form-editor',
                ]); ?>
                <div style="color: red;">
                    <?= $error?>
                </div>
                <br>
                <div>
                    <?//= $form->errorSummary($model); ?>
                    <?= $form->field($model, 'sum')->textInput()->label('Введите сумму пополнения') ?>
                </div>


                <div class="form-group text-right">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
