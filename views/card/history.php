<?php
/* @var $this yii\web\View */

use yii\bootstrap4\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'TRANS LOYD';
$url = $systemReference->picture['base_url'].$systemReference->picture['path'];
?>

<div class="ti-text-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?
                $columns = [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute'=>'sender',
                        'format'=>'raw',
                        'filter' => false,
                        'value'=>function($model){
                            return $model->senderCard->user->profile->firstname.'('.$model->senderCard->card_number.')';
                        },
                        'label' => "Отправитель"
                    ],
                    [
                        'attribute'=>'recipient',
                        'format'=>'raw',
                        'filter' => false,
                        'value'=>function($model){
                            return $model->recipientCard->user->profile->firstname.'('.$model->recipientCard->card_number.')';
                        },
                        'label' => "Получатель"
                    ],
                    [
                        'attribute'=>'sum',
                        'format'=>'text',
                        'filter' => false,
                    ],
                    [
                        'attribute'=>'updated_at',
                        'format'=>['date', 'php:d.m.Y H:j:s'],
                        'filter' => false,
                        'label' => 'Дата операции'
                    ],
                ];?>

                <?echo \kartik\export\ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'exportConfig' => [
                        \kartik\export\ExportMenu::FORMAT_TEXT => false,
                        \kartik\export\ExportMenu::FORMAT_HTML => false,
                        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
                        \kartik\export\ExportMenu::FORMAT_PDF =>  false,
                        \kartik\export\ExportMenu::FORMAT_CSV =>  false
                    ],

                ]);

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => [
                        'class' => 'grid-view table-responsive'
                    ],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-admin-event-to-users',
                    ],
                    'rowOptions'=> function ($model, $key, $index, $grid){
                        //if($model->application_status == 0){ return ['class'=>'new']; }
                    },
                    'columns' => $columns,
                ]); ?>

            </div>
        </div>
    </div>
</div>
