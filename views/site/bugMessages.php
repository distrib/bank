<?php

/* @var $this yii\web\View */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use app\models\LoginForm;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;

$this->title = 'Не удается войти?';
?>
<div class="preload-wrapper">
    <div class="backgrond"></div>

    <div class="pl-topper light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="logo">
                        <a href="/" class="ls-wrapper">
                            <img src="/image/TransLoyd/logo1.png">
                            <div class="slogan">Trans Loyd</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-bug-messages">
        <div class="row">

            <div class="col-lg-12">
                <div class="title">Не удается войти?</div>

                <div class="text">
                    Оставьте данные и опишите проблему. <br>
                    Оператор свяжется с вами.
                </div>
            </div>

            <div class="col-lg-12">
                <?php $form = ActiveForm::begin([
                    'id' => 'bugMessages',
                    'fieldConfig' => ['enableLabel'=>false],
                    'options'=>['data-window-id' => "bugMessages", 'autocomplete' => "off"],
                    'validateOnBlur' => false,
                ]); ?>

                <div class="row">
                    <div class="col-12 col-md-6 mb-4">
                        <?= $form->field($model, 'name')->textInput(['placeholder' => $model->attributeLabels()['name']]);?>
                    </div>

                    <div class="col-12 col-md-6 mb-4">
                        <?= $form->field($model, 'phone')->textInput(['placeholder' => $model->attributeLabels()['phone'],'class' => 'form-control phone-mask']);?>
                    </div>

                    <div class="col-12 mb-4">
                        <?= $form->field($model, 'problem')->textInput(['placeholder' => $model->attributeLabels()['problem'],'maxlength' => 500]);?>
                    </div>

                    <div class="col-md-6 col-12 mb-4">
                        <?= $form->field($model, 'picture')->widget(
                            \trntv\filekit\widget\Upload::className(),
                            [
                                'url' => ['/file-storage/upload'],
                                'maxFileSize' => 5*1024*1024, // 5 MiB
                                'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
                                'showPreviewFilename' => true,
                            ]);
                        ?>

                        <?//= $form->field($model, 'screen')->fileInput(['placeholder' => $model->attributeLabels()['screen'], 'class' => 'form-control file-icon']);?>

                        <!--div class="form-group field-userprofile-firstname required validating file-icon">
                            <input type="text" id="userprofile-firstname" class="form-control" name="UserProfile[firstname]" placeholder="Скриншот" aria-required="true" aria-invalid="true">
                            <div class="invalid-feedback"></div>
                        </div-->
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'smb-btn']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</div>
