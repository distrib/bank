<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
$this->title = 'Авторизация';
?>
<div class="preload-wrapper">
    <div class="backgrond"></div>

    <div class="pl-topper light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="logo">
                        <a href="/" class="ls-wrapper">
                            <img src="/image/TransLoyd/logo1.png">
                            <div class="slogan">Trans Loyd</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-signin">
        <div class="row">

            <div class="col-lg-12">
                <div class="signin-wrapper">
                    <span>Вход</span>
                </div>
                <div class="signup-wrapper">
                    <a href="/signup" class="signup">Регистрация</a>
                </div>
            </div>

            <div class="col-lg-12">
                <?php $form = ActiveForm::begin([
                    'id' => 'sign-in',
                    'fieldConfig' => ['enableLabel'=>false],
                    'options'=>['data-window-id' => "signin"],
                    'validateOnBlur'=>false,
                ]); ?>

                    <?= $form->field($model, 'email')->textInput(['placeholder' => "E-mail"]);?>
                    <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Пароль"]);?>

                    <div class="form-group">
                        <?= Html::submitButton('Войти', ['class' => 'smb-btn', 'name' => 'login-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="col-lg-12">
                <div class="forgot-password">
                    <a href="/request-password-reset">Забыли пароль?</a>
                </div>
                <div class="cannot-enter">
                    <a href="/bug-messages">Не удается войти?</a>
                </div>
            </div>

        </div>
    </div>

</div>
