<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preload-wrapper">
    <div class="site-callback">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">Обратная связь</div>

                <div class="text">
                    Телефон: 8 800 600-03-03 <br>
                    E-mail: excellence-club@bmw.com
                </div>

                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                    <?= $form->field($model, 'text')->textarea(['maxlength'=>500])->label('Напишите нам');?>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'smb-btn'])?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>

</div>
