<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preload-wrapper">
    <div class="backgrond"></div>

    <div class="pl-topper light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="logo">
                        <a href="/" class="ls-wrapper">
                            <img src="/image/TransLoyd/logo1.png">
                            <div class="slogan">Trans Loyd</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-signup">
        <div class="row">

            <div class="col-lg-12">
                <div class="signin-wrapper">
                    <span>Регистрация</span>
                </div>
                <div class="signup-wrapper">
                    <a href="/signin" class="signup">Вход</a>
                </div>
            </div>

            <div class="col-12">
                <?php $form = ActiveForm::begin([
                    'id' => 'sign-up',
                    'fieldConfig' => ['enableLabel'=>false],
                    'options'=>['data-window-id' => "signup"],
                    'validateOnBlur' => false,
                ]);
                ?>
                    <div class="row">
                        <div class="col-12 mb-4">
                            <?= $form->field($model, 'gender',['options' => ['class' => 'big-radio']])
                                ->radioList([$profile::GENDER_MALE => "Господин", $profile::GENDER_FEMALE => "Госпожа"]);?>
                        </div>

                        <div class="col-4 col-xs-12 mb-3">
                            <?= $form->field($model, 'lastname')->textInput(['placeholder' => $model->attributeLabels()['lastname'].'*']);?>
                        </div>
                        <div class="col-4 mb-3">
                            <?= $form->field($model, 'firstname')->textInput(['placeholder' => $model->attributeLabels()['firstname'].'*']);?>
                        </div>
                        <div class="col-4 mb-3">
                            <?= $form->field($model, 'middlename')->textInput(['placeholder' => $model->attributeLabels()['middlename']]);?>
                        </div>

                        <div class="col-4 mb-3">
                            <div class="phone-title"><?= $model->attributeLabels()['phone'].'*'?></div>
                            <?= $form->field($model, 'phone')->textInput(['placeholder' => '+7 ___ ___-__-__','class' => 'form-control phone-mask']); ?>
                        </div>

                        <div class="col-4 mb-3">
                            <?
                            if( strlen($mError['email'])>0 ) {
                                echo $form->field($model, 'email')->begin();
                                echo Html::activeTextInput($model, 'email', ['class' => 'form-control is-invalid']);
                                echo Html::tag('div', $mError['email'], ['class' => 'invalid-feedback']);
                                echo $form->field($model, 'email')->end();
                            }else{
                                echo $form->field($model, 'email')->textInput(['placeholder' => 'E-mail*']);
                            }
                            ?>
                        </div>

                        <div class="col-12">
                            <div class="agreement-wrapper">
                                <input type="checkbox" name="agreement" checked>
                                <span>
                                    Я принимаю
                                    <a href="#" target="_blank">
                                        правила программы
                                    </a> <br>
                                    и даю
                                    <a href="#" target="_blank">
                                        согласие на обработку персональных данных
                                    </a>
                                </span>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group text-center">
                                <?= Html::submitButton('Отправить заявку', ['class' => 'smb-btn', 'name' => 'signup-button']) ?>
                            </div>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
