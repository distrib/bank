<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
$this->title = 'Благодарим за регистрацию!';
?>
<div class="preload-wrapper">
    <div class="backgrond"></div>

    <div class="pl-topper light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="logo">
                        <a href="/" class="ls-wrapper">
                            <img src="/image/TransLoyd/logo1.png">
                            <div class="slogan">Trans Loyd</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-request-password-reset-success <?= isset($class)? $class:'';?>">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <?= $title;?>
                </div>

                <div class="text">
                    <?= $text;?>
                </div>

                <div class="btn-wrapper">
                    <a class="btn-def" href="<?= $link;?>">ОK</a>
                </div>
            </div>
        </div>
    </div>

</div>