<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Сброс пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preload-wrapper">
    <div class="backgrond"></div>

    <div class="pl-topper light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="logo">
                        <a href="/" class="ls-wrapper">
                            <img src="/image/TransLoyd/logo1.png">
                            <div class="slogan">Trans Loyd</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-request-password-reset-success">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <?= $title;?>
                </div>

                <div class="text">
                    <?// Введите новый пароль?>
                </div>

                <?php $form = ActiveForm::begin([
                    'id' => 'reset-password-form',
                    'fieldConfig' => ['enableLabel'=>false]
                ]);
                ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Пароль"]) ?>

                <?= $form->field($model, 'password_confirm')->passwordInput(['placeholder' => "Подтвердите пароль"]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'smb-btn']) ?>
                </div>
                <br>
                <div class="agreement-wrapper">
                    <input type="checkbox" name="agreement" checked>
                    <span>
                        Я принимаю
                        <a href="/media/Правила BMW Excellence Club.pdf" target="_blank">
                            правила программы "BMW Excellence Club"
                        </a>
                        и даю
                        <a href="/media/Согласие на обработку данных.pdf" target="_blank">
                             согласие на обработку персональных данных
                        </a>


                    </span>
                </div>


                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>