<?php
/* @var $this yii\web\View */
$this->title = 'BMW EXCELLENCE CLUB';
?>
<div class="pl-topper">
    <div class="container">
        <div class="row">
            <div class="col-12 text-right">
                <div class="logo">
                    <a href="/" class="ls-wrapper">
                        <img src="/image/TransLoyd/logo1.png">
                        <div class="slogan">Trans Loyd</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pl-bottom">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title">Trans Loyd <span class="d-md-none"><br></span> bank</div>
                <div class="text">
                    Банк <span class="d-md-none"><br></span> исключительных привилегий
                </div>

                <a href="/signin" class="sign">Продолжить</a>
            </div>
        </div>
    </div>
</div>
