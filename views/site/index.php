<?php
/* @var $this yii\web\View */

use yii\bootstrap4\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'TRANS LOYD';
$url = $systemReference->picture['base_url'].$systemReference->picture['path'];
?>

<div class="ti-text-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                Описание: так как проект не продпалагает эксплуатации, то в результате разработки были допущены
                некоторые упрощения, а именно:
                <br>
                <ul>
                    <li>
                        В действительности номер карты и номер счет хранения средств это разные сущности, именно
                        это позволяет к одному счету привязывать несколько карт. Но так как такой
                        задачи не было - упрощаем и делаем № банковской карты = № номеру
                    </li>
                    <li>
                        Финансовые учереждения практикуют использовать для ведения учета принцип двойной записи,
                        перемещая средства между счетами корреспондентами. А так как мы заменили № счета на №
                        банковских карт, для внесения и вывода средств будет использоваться спец. карта,
                        предварительно созданная для этого.
                    </li>
                </ul>
            </div>

            <div class="col-12">
                <?
                $columns = [
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'{add} {minus} {history} {send}',
                        'buttons' => [
                            'add' => function ($url, $model, $key) {
                                $url = \yii\helpers\Url::to(['card/add', 'id' => $model['id']]);

                                $iconName = "plus";
                                $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);

                                return Html::a($icon, $url,['title'=>'Положить деньги на карту']);

                            },

                            'minus' => function ($url, $model, $key) {
                                $url = \yii\helpers\Url::current(['card/minus', 'id' => $model['id']]);

                                $iconName = "minus";
                                $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);

                                return Html::a($icon, $url,['title'=>'Снять деньги c карты']);
                            },

                            'history' => function ($url, $model, $key) {
                                $url = \yii\helpers\Url::to(['card/history', 'id' => $model['id']]);

                                $iconName = "search";
                                $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);

                                return Html::a($icon, $url,['title'=>'Движение средств по карте']);

                            },

                            'send' => function ($url, $model, $key) {
                                $url = \yii\helpers\Url::current(['card/send', 'id' => $model['id']]);

                                $iconName = "envelope";
                                $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);

                                return Html::a($icon, $url,['title'=>'Перевести средства']);

                            },
                        ],
                    ],
                    [
                        'attribute'=>'id',
                        'format'=>'text',
                    ],
                    [
                        'attribute'=>'card_number',
                        'format'=>'text',
                    ],
                    [
                        'attribute'=>'month',
                        'format'=>'text',
                    ],
                    [
                        'attribute'=>'year',
                        'format'=>'text',
                    ],
                    [
                        'attribute'=>'sum',
                        'format'=>'raw',
                        'value'=> function ($model, $key, $index, $grid){

                            return ($model['sum'])? $model['sum']:0;
                        },
                        'label' =>'Сумма'
                    ],
                    [
                        'attribute'=>'updated_at',
                        'format'=>['date', 'php:d.m.Y'],
                        'filter' => \yii\jui\DatePicker::widget([
                            'options' => ['class' => 'form-control'],
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'language' => 'ru',
                            'dateFormat' => 'dd.MM.yyyy',
                        ]),
                        'label' => 'Дата измененения данных по карте'
                    ],
                ];?>

                <p class="text-right">
                    <?= Html::a('Добавить карту', ['/create'], ['class' => 'btn btn-success']) ?>
                </p>

                <?echo \kartik\export\ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'exportConfig' => [
                        \kartik\export\ExportMenu::FORMAT_TEXT => false,
                        \kartik\export\ExportMenu::FORMAT_HTML => false,
                        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
                        \kartik\export\ExportMenu::FORMAT_PDF =>  false,
                        \kartik\export\ExportMenu::FORMAT_CSV =>  false
                    ],

                ]);

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => [
                        'class' => 'grid-view table-responsive'
                    ],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-admin-event-to-users',
                    ],
                    'rowOptions'=> function ($model, $key, $index, $grid){
                        //if($model->application_status == 0){ return ['class'=>'new']; }
                    },
                    'columns' => $columns,
                ]); ?>

            </div>
        </div>
    </div>
</div>
