<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preload-wrapper">
    <div class="backgrond"></div>

    <div class="pl-topper light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                    <div class="logo">
                        <a href="/" class="ls-wrapper">
                            <img src="/image/TransLoyd/logo1.png">
                            <div class="slogan">Trans Loyd</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-request-password-reset">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">Восстановление пароля</div>

                <div class="text">Введите e-mail, указанный при регистрации.</div>


                <?php $form = ActiveForm::begin([
                    'id' => 'request-password-reset-form'
                ]); ?>

                    <?= $form->field($model, 'email')->textInput(['placeholder' => "E-mail"])->label(false);?>

                    <div class="form-group">
                        <?= Html::submitButton('Восстановить', ['class' => 'smb-btn'])?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
