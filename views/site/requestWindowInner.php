<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
$this->title = $pageTitle;
?>

<div class="site-request-window-inner <?= isset($class)? $class:'';?>">
    <div class="row">
        <div class="col-lg-12">
            <div class="title">
                <?= $title;?>
            </div>

            <div class="text">
                <?= $text;?>
            </div>

            <a class="btn-def" href="<?= $link;?>"><?= $linkText;?></a>
        </div>
    </div>
</div>
