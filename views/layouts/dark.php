<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\AdditionalAsset;
use app\assets\SlickAsset;
use app\assets\FontAwesomeAsset;
use app\assets\MaskedinputAsset;
use app\assets\InputmaskAsset;
use app\widgets\Alert;
use yii\helpers\Url;

AdditionalAsset::register($this);
SlickAsset::register($this);
FontAwesomeAsset::register($this);
MaskedinputAsset::register($this);
InputmaskAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/favicon.png'])]);?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap dark">
    <div class="content">
        <section class="preloader main-screen">
            <div class="ms-content">
                <?= $content ?>
            </div>
        </section>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
