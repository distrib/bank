<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\modules\menu\clients\widgets\MenuRender;
use app\modules\menu\models\Menu;
use yii\helpers\Html;

use app\assets\AppAsset;
use app\assets\AdditionalAsset;
use app\assets\SlickAsset;
use app\assets\FontAwesomeAsset;
use app\widgets\Alert;
use app\assets\MaskedinputAsset;
use app\models\User;

AdditionalAsset::register($this);
SlickAsset::register($this);
FontAwesomeAsset::register($this);
MaskedinputAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <header>
        <div class="container">
            <div class="header-wrapper">
                <div class="row">
                    <div class="col-md-8 col-9">
                        <div class="line-1">
                            <a href="/" class="h-slogan">
                                Trans Loyd
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-3 text-right">
                        <div class="r-line">
                            <div class="rl-bottom">
                                <div class="icon-wrapper">
                                    <a href="#" class="avatar"></a>
                                    <?
                                    echo Html::beginForm(['/logout'], 'post');
                                    echo Html::submitButton('', ['class' => 'logout']);
                                    echo Html::endForm();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </header>

    <div class="content">
        <div class="container">
            <? /*= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) */ ?>
            <?= Alert::widget() ?>
        </div>

        <?= $content ?>
    </div>

    <footer class="footer">
        <div class="container">
            <div class="f-wrapper">
            </div>
        </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="allModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <!--button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button-->
                </div>
            </div>
        </div>
    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
