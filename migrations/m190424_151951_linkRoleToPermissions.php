<?php

use yii\db\Migration;

/**
 * Class m190424_151951_linkRoleToPermissions
 */
class m190424_151951_linkRoleToPermissions extends Migration
{
    public function up()
    {
        $role = Yii::$app->authManager->getRole('admin');
        $permit = Yii::$app->authManager->getPermission('canAdmin');
        Yii::$app->authManager->addChild($role, $permit);
    }

    public function down()
    {
        echo "m190424_151951_linkRoleToPermissions can be reverted only manual.\n";

        return true;
    }
}
