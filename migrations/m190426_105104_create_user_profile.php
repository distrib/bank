<?php

use yii\db\Migration;

/**
 * Class m190426_105104_create_user_profile
 */
class m190426_105104_create_user_profile extends Migration
{
    private $table = '{{%user_profile}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'firstname' => $this->string(255),
            'lastname' => $this->string(255),
            'middlename' => $this->string(255),
            'phone' => $this->string(20),
            'gender' => $this->integer(2)->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at'=> $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            'idx-user_profile-user_id',
            $this->table,
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_profile-user_id',
            $this->table,
            'user_id',
            'user',
            'id',
            'CASCADE'

        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-user_profile-user_id',
            $this->table
        );

        $this->dropColumn(
            $this->table,
            'user_id'
        );

        $this->dropTable($this->table);
    }
}
