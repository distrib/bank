<?php

use yii\db\Migration;
use app\models\User;
use app\models\Card;
use app\models\UserProfile;

/**
 * Class m190922_201052_card_data
 */
class m190922_201052_card_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user = new User();
        $user->username = 'excort13@mail.ru';
        $user->email = 'excort13@mail.ru';
        $user->status = User::STATUS_ACTIVE;
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->password_hash = Yii::$app->security->generatePasswordHash(123456789);
        $user->save();

        if($user){
            $card = new Card();
            $card->user_id = $user->id;
            $card->card_number = '1111111111111111';
            $card->month = 12;
            $card->year = 2019;
            $card->pin = Yii::$app->security->generatePasswordHash('1111');
            $card->save();

            $profile = new UserProfile();
            $profile->user_id = $user->id;
            $profile->firstname = 'Дефолтный счет';
            $profile->save();

        } else
            throwException('error creating user');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if ($user = User::findByEmailSignUp('excort13@mail.ru'))
        {
            $cards = Card::find()->where(['user_id'=>$user->id])->all();

            foreach( $cards as $card){
                $card->delete();
            }

            $user->delete();
        }
    }
}
