<?php

use yii\db\Migration;
use app\models\CashFlow;
use app\models\User;

/**
 * Class m190923_062716_cash_flow_data
 */
class m190923_062716_cash_flow_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($user = User::findByEmailSignUp('excort13@mail.ru'))
        {
            $cashFlow = new CashFlow();
            $cashFlow->sender = $user->id;
            $cashFlow->recipient = $user->id;
            $cashFlow->sum = $user->id;
            $cashFlow->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if ($user = User::findByEmailSignUp('excort13@mail.ru'))
        {
            if( $cashFlow = CashFlow::find()->where(['sender'=>$user->id, 'recipient'=>$user->id])->all() )
            {
                foreach( $cashFlow as $row){
                    $row->delete();
                }
            }
        }
    }
}
