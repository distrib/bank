<?php

use yii\db\Migration;

/**
 * Class m190424_142631_addRole
 */
class m190424_142631_addRole extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $role = Yii::$app->authManager->createRole('admin');
        $role->description = 'Администратор';
        Yii::$app->authManager->add($role);

        $role = Yii::$app->authManager->createRole('user');
        $role->description = 'Зарегистрированный пользователь';
        Yii::$app->authManager->add($role);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m190424_142631_addRole can be reverted only manual.\n";

        return true;
    }

}
