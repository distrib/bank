<?php

use yii\db\Migration;

/**
 * Class m190424_153033_linkUserToRole
 */
class m190424_153033_linkUserToRole extends Migration
{
    public function up()
    {
        $userRole = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->assign($userRole, 1); // for admin

        $permit = Yii::$app->authManager->getPermission('canAdmin');
        Yii::$app->authManager->assign($permit, 1);
    }

    public function down()
    {
        echo "m190424_153033_linkUserToRole can be reverted only manual.\n";

        return true;
    }
}
