<?php

use yii\db\Migration;

/**
 * Class m190922_190155_cash_flow
 */
class m190922_190155_cash_flow extends Migration
{
    private $table = '{{%cash_flow}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'sender' => $this->integer()->notNull(),
            'recipient' => $this->integer()->notNull(),
            'sum' => $this->double(2)->unsigned(),
            'created_at' => $this->integer(),
            'updated_at'=> $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            'idx-cash_flow-sender',
            $this->table,
            'sender'
        );

        $this->createIndex(
            'idx-cash_flow-recipient',
            $this->table,
            'recipient'
        );

        $this->addForeignKey(
            'fk-cash_flow-sender',
            $this->table,
            'sender',
            'card',
            'id',
            'CASCADE'

        );

        $this->addForeignKey(
            'fk-cash_flow-recipient',
            $this->table,
            'recipient',
            'card',
            'id',
            'CASCADE'

        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
