<?php

use yii\db\Migration;

/**
 * Class m190922_183339_cards
 */
class m190922_183339_cards extends Migration
{
    private $table = '{{%card}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'card_number' => $this->string(16)->notNull(),
            'month' => $this->integer(2)->notNull(),
            'year' => $this->integer(4)->notNull(),
            'pin' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at'=> $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            'idx-card-user_id',
            $this->table,
            'user_id'
        );

        $this->addForeignKey(
            'fk-card-user_id',
            $this->table,
            'user_id',
            'user',
            'id',
            'CASCADE'

        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
