<?php

use yii\db\Migration;

/**
 * Class m190424_145555_addPermissionForEnteringToAdminPanel
 */
class m190424_145555_addPermissionForEnteringToAdminPanel extends Migration
{
    public function up()
    {
        $permit = Yii::$app->authManager->createPermission('canAdmin');
        $permit->description = 'Право на вход в админку c правами админа';
        Yii::$app->authManager->add($permit);
    }

    public function down()
    {
        echo "m190424_145555_addPermissionForEnteringToAdminPanel can be reverted only manual.\n";

        return true;
    }
}
