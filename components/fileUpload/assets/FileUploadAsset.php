<?php
namespace app\components\fileUpload\assets;
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 24.06.2019
 * Time: 15:26
 */

class FileUploadAsset extends \yii\web\AssetBundle
{
    public $sourcePath ='@app/components/fileUpload/assets';
    public $css = [
        'css/document-upload.css',
    ];
    public $js = [
        'js/document-upload.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}