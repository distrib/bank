<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 24.06.2019
 * Time: 16:49
 */

namespace app\components\fileUpload;


use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class FileUploadBehavior extends Behavior
{
    public $attribute = 'file';
    public $FILE_PATH = '@app/web/uploads/menu';
    private $file;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT =>'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE=>'beforeUpdate'
        ];

    }

    /**
     * @return bool
     */
    public function beforeInsert(){

        $this->file = UploadedFile::getInstance($this->owner,$this->attribute);
        if(empty($this->file)){
            return false;
        };
        $this->createFile();
        return true;
    }

    /**
     * @return bool
     */
    public function beforeUpdate(){

        $this->file = UploadedFile::getInstance($this->owner,$this->attribute);
        if(empty($this->file)){
            return false;
        };
        $this->createFile();
        $this->removeOldFile();
        return true;
    }

    /**
     * удаление старого файла
     */
    public function removeOldFile(){
        $oldImage = $this->owner->oldAttributes[$this->attribute];
        if(isset($oldImage)&&is_file(Yii::getAlias($this->FILE_PATH.'/'.basename($oldImage)))){
            unlink(Yii::getAlias($this->FILE_PATH.'/'.basename($oldImage)));
        }
    }


    /**
     * @return string
     * @throws \yii\base\Exception
     * Генерация название
     */
    public function generateName(){
        return Yii::$app->security->generateRandomString(16);
    }

    /**
     * @throws \yii\base\Exception
     * Создание файла
     */
    public function createFile()
    {
        $name = $this->generateName(). '.' . $this->file->extension;
        if(!is_dir(Yii::getAlias($this->FILE_PATH))){
            FileHelper::createDirectory(Yii::getAlias($this->FILE_PATH));
        }
        $path = Yii::getAlias($this->FILE_PATH .'/'. $name);
        if ($this->file->saveAs($path)) {
            $this->owner->{$this->attribute} = $name;
        }
    }


}