<?php

namespace app\components\fileUpload;

use app\components\fileUpload\assets\FileUploadAsset;
use SplFileInfo;
use Yii;
use yii\helpers\Html;
use yii\widgets\InputWidget;


class FileUploadWidget extends InputWidget
{
    protected $html = '';
    public $attributeName = 'file';

    /**
     * @return string
     */
    public function run()
    {
        $this->registerPlugin();
        $this->open();
        if ($this->hasModel()) {
            $this->html .= Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            $this->html .= Html::textInput($this->name, $this->value, $this->options);
        }
        $this->close();
        return $this->html;
    }

    /**
     *
     */
    public function open()
    {
        $this->html .= '
                <div class="row it">
                    <div class="col-sm-11" id="one">
                        <br>
                        <div id="uploader">
                            <div class="row uploadDoc">
                                <div class="col-sm-3">
                                    <div class="docErr">' . Yii::t("app", "Загрузите pdf файл") . '</div><!--error-->
                                    <div class="fileUpload btn btn-orange">
                                        <img src="' . $this->getIcon() . '" class="icon">
                                        <span class="upl" id="upload">' . $this->getDocument() . '</span>
';
    }

    public function close()
    {
        $this->html .= '
                               </div><!-- btn-orange -->
                            </div><!-- col-3 -->
                        </div><!--row-->
                    </div><!--uploader-->
                </div><!--one-->
            </div><!-- row -->
';

    }

    /**
     * @return mixed|string
     */
    public function getDocument()
    {
        if ($this->hasModel() && !empty($this->model->{$this->attributeName})) {
            return $this->model->{$this->attributeName};
        } else {
            return Yii::t("app", "Загрузить файл");
        }
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        if ($this->hasModel() && !empty($this->model->{$this->attributeName})) {
            $withoutexp = new SplFileInfo($this->model->{$this->attributeName});
            $withoutexp = $withoutexp->getExtension();
            if ($withoutexp == 'pdf') {
                return 'https://image.flaticon.com/icons/svg/179/179483.svg';
            }
        }
        return 'https://image.flaticon.com/icons/svg/136/136549.svg';
    }

    /**
     *
     */
    public function registerPlugin()
    {
        $this->options['class'] = 'upload up';
        $this->options['type'] = 'file';
        $this->options['id'] = 'up';
        $this->options['onchange'] = new \yii\web\JsExpression("readURL(this);");
        FileUploadAsset::register($this->view);
    }

}