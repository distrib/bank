<?php
/**
 * Yii2 Shortcuts
 * @author Eugene Terentev <eugene@terentev.net>
 * -----
 * This file is just an example and a place where you can add your own shortcuts,
 * it doesn't pretend to be a full list of available possibilities
 * -----
 */

/**
 * @return int|string
 */
function getMyId()
{
    return Yii::$app->user->getId();
}

/**
 * @param string $view
 * @param array $params
 * @return string
 */
function render($view, $params = [])
{
    return Yii::$app->controller->render($view, $params);
}

/**
 * @param $url
 * @param int $statusCode
 * @return \yii\web\Response
 */
function redirect($url, $statusCode = 302)
{
    return Yii::$app->controller->redirect($url, $statusCode);
}

/**
 * @param $form \yii\widgets\ActiveForm
 * @param $model
 * @param $attribute
 * @param array $inputOptions
 * @param array $fieldOptions
 * @return string
 */
function activeTextinput($form, $model, $attribute, $inputOptions = [], $fieldOptions = [])
{
    return $form->field($model, $attribute, $fieldOptions)->textInput($inputOptions);
}

/**
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function env($key, $default = false) {

    $value = getenv($key);

    if ($value === false) {
        return $default;
    }

    switch (strtolower($value)) {
        case 'true':
        case '(true)':
            return true;

        case 'false':
        case '(false)':
            return false;
    }

    return $value;
}

/**
 * @param integer $pass_len
 * @param array $pass_chars
 * @return string
 */
function randString($pass_len=10, $pass_chars=false)
{
    static $allchars = "abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ0123456789";
    $string = "";
    if(is_array($pass_chars))
    {
        while(strlen($string) < $pass_len)
        {
            if(function_exists('shuffle'))
                shuffle($pass_chars);
            foreach($pass_chars as $chars)
            {
                $n = strlen($chars) - 1;
                $string .= $chars[mt_rand(0, $n)];
            }
        }
        if(strlen($string) > count($pass_chars))
            $string = substr($string, 0, $pass_len);
    }
    else
    {
        if($pass_chars !== false)
        {
            $chars = $pass_chars;
            $n = strlen($pass_chars) - 1;
        }
        else
        {
            $chars = $allchars;
            $n = 61; //strlen($allchars)-1;
        }
        for ($i = 0; $i < $pass_len; $i++)
            $string .= $chars[mt_rand(0, $n)];
    }
    return $string;
}