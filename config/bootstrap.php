<?php
Yii::setAlias('@storage', dirname(__DIR__).'web/source' );
Yii::setAlias('@storageUrl', env('STORAGE_URL'));

define('COOKIE_DOMAIN', parse_url('http://'.$_SERVER['HTTP_HOST'], PHP_URL_HOST), true);
define('BASE_URL', env('BASE_FILESTORAGE_URL'), true);