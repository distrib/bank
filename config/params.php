<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'TinyMce'=>[
        //TinyMCE options
        'pluginOptions' => [
            'language' => 'ru',
        ],
    ],

    //домен для кук
    'cookieDomain' => env('COOKIE_DOMAIN'),

    //ключ для кук общий (frontend + backend)
    'cookieValidationKey' => env('COOKIE_VALIDATION_KEY'),

    // ограничение времени после сброса пароля пользователя
    'user.passwordResetTokenExpire' => '172800‬',
];
