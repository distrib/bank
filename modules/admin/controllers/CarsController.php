<?php

namespace app\modules\admin\controllers;

use app\components\expertsender\ExpertSender;
use app\models\EventToUsers;
use app\models\search\EventToUsersSearch;
use app\models\User;
use app\models\UserCars;
use Yii;
use app\models\Events;
use app\models\EventsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class CarsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->isAjax) {

            $model = $this->findModel($id);

            if( $model->load(Yii::$app->request->post()) && $model->checkUniqueVin() ){

                $purchaseDate = $model->purchase_date;
                if( strlen($purchaseDate) ){
                    $date = \DateTime::createFromFormat('d.m.Y', $purchaseDate);
                    $model->purchase_date = date_format($date, 'Y-m-d');
                }

                if( $model->save() ){

                    if( $model->user->bmw_status == User::STATUS_CRM_VIN_DUPLICATE ) {

                        $user = User::findOne($model->user->id);
                        $user->status = User::STATUS_CRM_NOT_PASSED;
                        $user->save();
                    }

                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => true];
                }
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserCars the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserCars::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
