<?php

namespace app\modules\admin\controllers;

use app\models\OfferingToUsers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\models\search\OfferingAppSearch;
use app\models\Notifications;


/**
 * OfferingToUsersController implements the CRUD actions for OfferingToUsers model.
 */
class OfferingToUsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OfferingToUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OfferingAppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OfferingToUsers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $currApplicationStatus = $model->application_status;
        if( $model->load(Yii::$app->request->post()) ){

            $data = Yii::$app->request->post('OfferingToUsers',[]);

            if( $data['application_status'] !== 0 ){
                $model->status_change_time = time();
            }else{
                if($currApplicationStatus>0){
                    $model->application_status = $currApplicationStatus;
                }
            }

            if( $model->save() ){
                if( intval($data['application_status']) === 1){
                    Notifications::setNotification(
                        Notifications::TYPE_CONFIRMATION_OFFERINGS,
                        'Для Вас подтверждено специальное предложение <a href="/special_offers/'.$model->offering->slug.'" >'.$model->offering->title.'</a>',
                        $model->user
                    );
                }

                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the OfferingToUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Offerings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OfferingToUsers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
