<?php
namespace app\modules\admin\controllers;

use app\components\grid\EnumColumn;
use app\models\search\BugMessagesSearch;
use app\models\search\CallbackSearch;
use app\models\User;
use app\modules\admin\models\search\ActivityReportSearch;
use app\modules\admin\models\search\EventsAppSearch;
use app\modules\admin\models\search\OfferingAppSearch;
use app\modules\admin\models\search\UserCarsSearch;
use app\modules\admin\models\search\UserReportSearch;
use app\modules\admin\models\search\UserSearch;
use arturoliveira\ExcelView;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;

/**
 * Report controller
 */
class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect('admin/report/user');
    }

    /**
     * Report by user action.
     *
     * @return string
     */
    public function actionUser()
    {
        return $this->render('index',[
            'data' => ' ',
        ]);
    }

    public function actionUsers()
    {

        $searchModel = new UserReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCars()
    {

        $searchModel = new UserCarsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('cars', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOfferings()
    {

        $searchModel = new OfferingAppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('offerings', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionEvents()
    {

        $searchModel = new EventsAppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('events', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInterests()
    {

        $searchModel = new UserReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('interests', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActivity()
    {

        $searchModel = new ActivityReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('activity', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionBug()
    {

        $searchModel = new BugMessagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bug', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCallback()
    {

        $searchModel = new CallbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('callback', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
