<?php

namespace app\modules\admin\controllers;

use app\models\Notifications;
use app\models\OfferingToUsers;
use app\models\search\OfferingToUsersSearch;
use Yii;
use app\models\SpecialOfferings;
use app\models\search\SpecialOfferingsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SpecialOfferingsController implements the CRUD actions for SpecialOfferings model.
 */
class SpecialOfferingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SpecialOfferings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SpecialOfferingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SpecialOfferings model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SpecialOfferings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SpecialOfferings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }else {

            // default values
            $model->active=1;


        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SpecialOfferings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $searchModel = new OfferingToUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'searchFeedbackModel' => $searchModel,
            'dataProviderFeedback' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing SpecialOfferings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionReject () {

        $post = Yii::$app->request->post();
        $feedback = OfferingToUsers::findOne($post['id']);
        $feedback->application_status = 2;
        $feedback->status_comment = $post['reason'];
        $feedback->status_change_time = time();
        $feedback->save();
        return $this->redirect(['update',
            'id'=>$feedback->offering_id
        ]);


    }

    public function actionAccept ($id) {

        $feedback = OfferingToUsers::findOne($id);
        $feedback->application_status = 1;
        $feedback->status_change_time = time();
        $feedback->save();
        Notifications::setNotification(Notifications::TYPE_CONFIRMATION_OFFERINGS,'Для Вас подтверждено специальное предложение <a href="/special_offers/' . $feedback->offering->slug . '" >' .  $feedback->offering->title . '</a>',$feedback->user);
        return $this->redirect(['update',
            'id'=>$feedback->offering_id
        ]);


    }

    public function actionProcess ($id) {

        $feedback = OfferingToUsers::findOne($id);
        $feedback->application_status = 3;
        $feedback->status_change_time = time();
        $feedback->save();
        return $this->redirect(['update',
            'id'=>$feedback->offering_id
        ]);


    }

    /**
     * Finds the SpecialOfferings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SpecialOfferings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SpecialOfferings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the SpecialOfferingsSlider model based on its primary key value.
     * If the model is not found, a new model will be returned.
     * @param integer $event_id
     * @return mixed
     */
    protected function findSlider($special_offerings_id)
    {
        if (($model = SpecialOfferingsSlider::findOne(['special_offerings_id'=>$special_offerings_id])) !== null) {
            return $model;
        }else{
            return new SpecialOfferingsSlider();
        }
    }
}
