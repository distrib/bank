<?php

namespace app\modules\admin\controllers;

use app\components\expertsender\ExpertSender;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\search\EventToUsersSearch;
use app\models\EventToUsers;
use app\models\Notifications;
use app\models\Events;

/**
 * EventToUsersController implements the CRUD actions for EventToUsers model.
 */
class EventToUsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single OfferingToUsers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $event = $this->findEventModel($model->event_id);

        $currApplicationStatus = $model->application_status;
        if( $model->load(Yii::$app->request->post()) ){

            $data = Yii::$app->request->post('EventToUsers',[]);

            if( intval($data['application_status']) !== 0 ){
                $model->status_change_time = time();
            }else{
                if($currApplicationStatus>0){
                    $model->application_status = $currApplicationStatus;
                }
            }

            if( $model->save() ){
                if( intval($data['application_status']) === 1){
                    Notifications::setNotification(
                        Notifications::TYPE_CONFIRMATION_EVENT,
                        'Вы зарегистрированы на мероприятие <a href="/events/'.$model->event->slug.'" >'.$model->event->title.'</a>',
                        $model->user
                    );
                }

                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'event' => $event,
        ]);
    }

    /**
     * Lists all event_to_users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventToUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the EventToUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventToUsers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEventModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
