<?php

namespace app\modules\admin\controllers;

use app\components\expertsender\ExpertSender;
use app\models\EventToUsers;
use app\models\Notifications;
use app\models\search\EventToUsersSearch;
use Yii;
use app\models\Events;
use app\models\EventsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\EventsSlider;
use app\components\alfabank\alfabank;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Events();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {

            // default values
            $model->active=1;
            $model->pay=0;
            $model->limit = 100;


        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $searchModel = new EventToUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
        }
        $etuStatuses = EventToUsers::getApplicationStatuses();

        return $this->render('update', [
            'model' => $model,
            'searchFeedbackModel' => $searchModel,
            'dataProviderFeedback' => $dataProvider,
            'etuStatuses' => $etuStatuses,
        ]);
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionReject () {

        $post = Yii::$app->request->post();
        $feedback = EventToUsers::findOne($post['id']);
        $feedback->application_status = 2;
        $feedback->status_comment = $post['reason'];
        $feedback->status_change_time = time();
        $feedback->save();
        return $this->redirect(['update',
            'id'=>$feedback->event_id
        ]);


    }

    public function actionAccept ($id) {
//        $feedback = EventToUsers::findOne($id);
//        $feedback->application_status = 1;
//        $feedback->status_change_time = time();
//        $feedback->save();
//        Notifications::setNotification(Notifications::TYPE_CONFIRMATION_EVENT,'Вы зарегистрированы на мероприятие <a href="/events/' . $feedback->event->slug . '" >' .  $feedback->event->title . '</a>',$feedback->user);
//
//        return $this->redirect(['update',
//            'id'=>$feedback->event_id
//        ]);
    }

    public function actionSendLink ($id) {

        $EventToUsers = EventToUsers::findOne($id);

        //$mail = Yii::$app->expertSender->createTransactionalMessage('Ссылка на оплату','pay-link');
        //\Yii::trace($mail, 'ppayment');

        $response = Yii::$app->alfabank->registerEventAsOrder($EventToUsers);
        if( $response->status == 'success'){

            $EventToUsers->pay_link = $response->message['formUrl'];
            $EventToUsers->pay_order_id = $response->message['orderId'];
            $EventToUsers->application_status = EventToUsers::APPLICATION_STATUS_PAY_LINK_SEND;
            $EventToUsers->status_change_time = time();
            $EventToUsers->save();

            // Отправка на почту
            Yii::$app->expertSender->sendPayLink($EventToUsers->user,$response->message['formUrl']);

            // Оповещение в кабинете
            $description = 'Ссылка для оплаты участия в мероприятии ';
            $description .= '<a href="/events/'.$EventToUsers->event->slug.'">'.$EventToUsers->event->title.'</a> ';
            $description .= 'отправлена Вам на почту';
            Notifications::setNotification(Notifications::TYPE_CONFIRMATION_EVENT,$description,$EventToUsers->user);

        }

        return $this->redirect([
            'update',
            'id'=>$EventToUsers->event_id
        ]);
    }

    public function actionProcess ($id) {

        $feedback = EventToUsers::findOne($id);
        $feedback->application_status = 3;
        $feedback->status_change_time = time();
        $feedback->save();

        return $this->redirect(['update',
            'id'=>$feedback->event_id
        ]);

    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the EventsSlider model based on its primary key value.
     * If the model is not found, a new model will be returned.
     * @param integer $event_id
     * @return mixed
     */
    protected function findSlider($event_id)
    {
        if (($model = EventsSlider::findOne(['event_id'=>$event_id])) !== null) {
            return $model;
        }else{
            return new EventsSlider();
        }
    }

}
