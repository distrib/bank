<?php

namespace app\modules\admin\controllers;

use app\models\ImportUsersMismatchLog;
use app\models\User;
use app\models\UserCars;
use app\models\UserProfile;
use moonland\phpexcel\Excel;
use Yii;
use app\models\ImportUsersLog;
use app\models\search\ImportUsersLogSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ImportUsersController implements the CRUD actions for ImportUsersLog model.
 */
class ImportUsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all ImportUsersLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImportUsersLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ImportUsersLog model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionSetXls()
    {

        if ($file = UploadedFile::getInstanceByName('xls')) {


            $data = Excel::widget([
                'mode' => 'import',
                'fileName' => $file->tempName,
                'setFirstRecordAsKeys' => false, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
                'setIndexSheetByName' => false, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric.
                'getOnlySheet' => 'sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
            ]);

            unset($data[1]);


            $log = new ImportUsersLog();

            $log->importUsers($data);




        }

        $this->redirect('/admin/import-users');

    }


    /**
     * Finds the ImportUsersLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImportUsersLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImportUsersLog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * @param User $user
     * @param array $item
     * @param string $what
     * @return ImportUsersMismatchLog|null
     */

    protected function checkMismatch($user, $item,$what)
    {


        $status = 0;
        $firstname = '';
        $lastname = '';
        $middlename = '';
        $email = '';
        $phone = '';


        if (trim(strtolower($user->profile->firstname)) != trim(strtolower($item['C']))) {
            $status = 1;
            $firstname = 'Сайт: ' . $user->profile->firstname . ' CRM: ' . $item['C'];

        }

        if (trim(strtolower($user->profile->lastname)) != trim(strtolower($item['E']))) {
            $status = 1;
            $lastname = 'Сайт: ' . $user->profile->lastname . ' CRM: ' . $item['E'];

        }

        if (trim(strtolower($user->profile->middlename)) != trim(strtolower($item['D']))) {
            $status = 1;
            $middlename = 'Сайт: ' . $user->profile->middlename . ' CRM: ' . $item['D'];

        }

        if (trim(strtolower($user->email)) != trim(strtolower($item['H']))) {
            $status = 1;
            $email = 'Сайт: ' . $user->email . ' CRM: ' . $item['H'];

        }

        if (($user->status == User::STATUS_ACTIVE || $user->status == User::STATUS_RESTORED) && ($user->profile->bmw_id != $item['A'])) {
            $status = 1;
            $bmw = 'Сайт: ' . $user->profile->bmw_id . ' CRM: ' . $item['A'];

        }

        if (preg_replace('/\D/', '', $user->profile->phone) != preg_replace('/\D/', '', $item['G'])) {
            $status = 1;
            $phone = 'Сайт: ' . $user->profile->phone . ' CRM: ' . $item['G'];

        }

        $ms = null;

        if ($status) {
            $ms = new ImportUsersMismatchLog();


            $ms->user_id = $user->id;
            $ms->bmw_id = isset($bmw) ? $bmw:'';
            $ms->phone = isset($phone) ? $phone:'';
            $ms->email = isset($email) ? $email:'';
            $ms->firstname = isset($firstname) ? $firstname:'';
            $ms->lastname = isset($lastname) ? $lastname:'';
            $ms->middlename = isset($middlename) ? $middlename:'';

            switch ($user->status) {
                case User::STATUS_INACTIVE:
                case User::STATUS_INACTIVE_CRM_PASSED:
                case User::STATUS_INACTIVE_CRM_MISMATCH:
                case User::STATUS_ACTIVATION_PROCESS:
                    $user->status = User::STATUS_INACTIVE_CRM_MISMATCH;
                    break;
                case User::STATUS_ACTIVE;
                case User::STATUS_ACTIVE;
                    $user->status = User::STATUS_ACTIVE_CRM_MISMATCH;
                    break;

            }



        } else {
            switch ($user->status) {
                case User::STATUS_INACTIVE:
                case User::STATUS_INACTIVE_CRM_PASSED:
                case User::STATUS_INACTIVE_CRM_MISMATCH:
                    $user->status = User::STATUS_INACTIVE_CRM_PASSED;
                    break;
            }


        }

       $user->save();

        return $ms;
    }
}
