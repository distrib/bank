<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index','expert-send'],
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect('admin/user');

        /*$user = User::find()->count();
        $offerings = OfferingToUsers::find()->count();
        $eventsAll = EventToUsers::find()->count();
        $callbackAll = Callback::find()->count();
        $bugMessagesAll = BugMessages::find()->count();

        if(\Yii::$app->user->can('canAdmin'))
        {
            return $this->render('index',[
                'user' => $user,
                'offerings' => $offerings,
                'eventsAll' => $eventsAll,
                'callback' => $callbackAll,
                'bugMessagesAll' => $bugMessagesAll,
            ]);
        }
        else throw new ForbiddenHttpException('У вас недостаточно прав для выполнения указанного действия');*/

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            $this->layout = 'main-login';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

//    public function actionExpertSend() {
//
//        $ex = new ExpertSender();
//
//
//        var_dump($ex->updateTransactionalMessage(657,'Восстановление пароля','bmw-restore'));
//       // $ex->sendRegistrationMessage(User::findOne(133),'asd');
//
//
//
//    }
}
