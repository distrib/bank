<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\User;
use app\modules\admin\models\UserForm;
use app\modules\admin\models\search\UserSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UserProfile;
use app\models\ImportUsersMismatchLog;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $warning ='';

        $warning .= User::getPassedCount() ? '<br>'.User::getPassedCount().' пользователь(ей) ждут приглашения':'';
        $warning .= User::getMismatchCount() ? '<br><a href="\admin\user?UserSearch[bmw_status]=2">'.User::getMismatchCount().' имеют несоответствия</a>':'';
        $warning .= User::getDuplicateCount() ? '<br><a href="\admin\user?UserSearch[bmw_status]=3">'.User::getDuplicateCount().' Дубль VIN</a>':'';

       if (!empty($warning))
        Yii::$app->session->setFlash('error', $warning);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
            'profile' => $this->loadProfile($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserForm();
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveProfile($model->id);
            return $this->redirect(['index']);
        }

        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        asort($roles);

        $profile= new UserProfile();
        $profile->gender= $profile::GENDER_MALE;
        $profile->connection= $profile::CONNECTION_PHONE;

        return $this->render('create', [
            'model' => $model,
            'roles' => $roles,
            'profile' => $profile,
        ]);
    }

    /**
     * Updates an existing User model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new UserForm();
        $model->setModel($this->findModel($id));
        $profile = $this->loadProfile($model->id);
        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {

                $data = Yii::$app->request->post('UserProfile',[]);
                $profile->setAttributes($data);

                if( is_array($data['picture']) ){
                    $profile->picture = $data['picture'];
                }

                $birthday = $profile->birthday;
                if( strlen($birthday) ){
                    $date = \DateTime::createFromFormat('d.m.Y', $birthday);
                    $profile->birthday = date_format($date, 'Y-m-d');
                }

                if( $profile->checkUniquePhone() ){
                    if( $profile->save() ){
                        return $this->redirect(['update','id'=>$id]);
                    }
                }
            }
        }

        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        asort($roles);


        $carsProvider = new ActiveDataProvider([
            'query' => $this->findModel($id)->getCars(),
        ]);

        $importUserMismatch = ImportUsersMismatchLog::find()->where(['user_id' => $id])
            ->orderBy(['user_log_id'=>SORT_DESC])->one();

        return $this->render('update', [
            'model' => $model,
            'roles' => $roles,
            'carsProvider' => $carsProvider,
            'profile' => $profile,
            'importUserMismatch' => $importUserMismatch,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->authManager->revokeAll($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function loadProfile($id) {
        if( ($UserProfileModel = UserProfile::findOne(['user_id'=>$id])) !== null ){
            return $UserProfileModel;
        }else{
            return null;
        }
    }

    public  function actionSendInvitation($id) {

        $user = $this->findModel($id);
        $user->status = User::STATUS_ACTIVATION_PROCESS;
        $user->generateEmailVerificationToken();
        $user->generatePasswordResetToken();
        $user->save();

        if( $user->sendVerificationTokenToUser() ){
            Yii::$app->session->setFlash('success', 'Приглашение отправлено');
        }else{
            Yii::$app->session->setFlash('alerts', 'Сбой отправки приглашения');
        }

        $this->redirect('/admin/user/update?id='.$id);

    }

    public function actionArchive($id)
    {
        $model = $this->findModel($id);

        $model->status = User::STATUS_DELETED;
        $model->save();
        Yii::$app->session->setFlash('success', 'Пользователь успешно перенесен в архив');
        return $this->redirect(['update','id'=>$id]);
    }

    public function actionRenew($id)
    {
        $model = $this->findModel($id);

        $model->status =($model->status == User::STATUS_DELETED || $model->status == User::STATUS_REPEATED_REGISTRATION)  ? User::STATUS_RESTORED:User::STATUS_ACTIVE;
        $model->save();
        Yii::$app->session->setFlash('success', 'Пользователь активирован');
        return $this->redirect(['update','id'=>$id]);
    }

//    public function actionChangeMessage()
//    {
//        var_dump(Yii::$app->expertSender->updateTransactionalMessage(656,'Добро пожаловать в BMW Excellence Club','bmw-registration'));
//    }
}
