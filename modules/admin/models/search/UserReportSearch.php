<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserReportSearch extends User
{

    public $firstname;
    public $lastname;
    public $middlename;
    public $gender;


    public $phone;
    public $bmw_id;
    public $car_vin;
    public $car_model;
    public $first_car_purchase_date;
    public $last_car_purchase_date;
    public $dealer;
    public $dealer_town;
    public $town;
    public $connection;
    public $membership_days;
    public $count_cars;
    public $birthday;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'bmw_status', 'gender', 'manual_register', 'created_at', 'updated_at', 'count_cars'], 'integer'],
            [[ 'auth_key', 'crf_verification', 'birthday', 'middlename', 'car_vin', 'town', 'car_model', 'dealer', 'membership_days', 'connection', 'dealer_town', 'last_car_purchase_date', 'first_car_purchase_date', 'password_hash', 'email','firstname','lastname','phone', 'bmw_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->joinWith(['profile','lastCar lastCar', 'firstCar firstCar','lastCar.dealerModel dealer'])
            ->where(['<>','user.status',User::STATUS_INACTIVE])
            ->where(['user.status'=>[User::STATUS_RESTORED,User::STATUS_DELETED,User::STATUS_ACTIVE]])
            ->groupBy('user.id')->orderBy('user.created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.status' => $this->status,
            'bmw_status' => $this->bmw_status,
            'manual_register' => $this->manual_register,
            'user.created_at' => $this->created_at,
            'user.updated_at' => $this->updated_at,
             'user_profile.gender' => $this->gender,
            'user_profile.birthday' => $this->birthday,
        ]);
        $query->andFilterWhere([
            'datediff(\''.date('Y-m-d',time()).'\',lastCar.purchase_date )' => $this->membership_days,
        ]);
        if (!empty($this->count_cars) ||  $this->count_cars=='0') {
            $query->having(['count(lastCar.id)' => pow($this->count_cars, 2)]);
        }
        $query->andFilterWhere(['like', 'user.auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'user.password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'user.email', $this->email])



            ->andFilterWhere(['like', 'user_profile.lastname', $this->lastname])
            ->andFilterWhere(['like', 'user_profile.middlename', $this->middlename])
            ->andFilterWhere(['like', 'user_profile.bmw_id', $this->bmw_id])
            ->andFilterWhere(['like', 'user_profile.firstname', $this->firstname])
            ->andFilterWhere(['like', 'user_profile.phone', $this->phone])
            ->andFilterWhere(['like', 'lastCar.vin', $this->car_vin])
            ->andFilterWhere(['like', 'lastCar.model', $this->car_model])
            ->andFilterWhere(['like', 'dealer.name', $this->dealer])
            ->andFilterWhere(['like', 'lastCar.dealer_town', $this->dealer_town])
            ->andFilterWhere(['like', 'user.crf_verification', $this->crm_verification])
            ->andFilterWhere(['like', 'lastCar.purchase_date', $this->last_car_purchase_date]);



        return $dataProvider;
    }


}
