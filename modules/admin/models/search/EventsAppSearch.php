<?php

namespace app\modules\admin\models\search;

use app\models\EventToUsers;
use app\models\UserCars;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class EventsAppSearch extends EventToUsers
{


    public $event_name;
    public $type;

    public $bmw_id;
    public $firstname;
    public $lastname;
    public $middlename;
    public $town;
    public $email;
    public $phone;
    public $connection;






    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'application_status', 'connection','type'], 'integer'],
            [['bmw_id', 'middlename', 'event_name', 'status_comment', 'firstname', 'lastname', 'town',  'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventToUsers::find()->joinWith(['user','event','profile'])

           ->orderBy('event_to_users.created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'event_to_users.application_status' => $this->application_status,
            'user_profile.connection' => $this->connection,
            'events.type' => $this->type,

        ]);




        $query
            ->andFilterWhere(['like', 'events.title', $this->event_name])
            ->andFilterWhere(['like', 'event_to_users.status_comment', $this->status_comment])
            ->andFilterWhere(['like', 'user_profile.lastname', $this->lastname])
            ->andFilterWhere(['like', 'user_profile.middlename', $this->middlename])
            ->andFilterWhere(['like', 'user_profile.bmw_id', $this->bmw_id])
            ->andFilterWhere(['like', 'user_profile.firstname', $this->firstname])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'user_profile.phone', $this->phone])
            ->andFilterWhere(['like', 'user_profile.town', $this->town]);

        return $dataProvider;
    }


}
