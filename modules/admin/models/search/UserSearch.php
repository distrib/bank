<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{

    public $firstname;
    public $lastname;
    public $phone;
    public $bmw_id;
    public $car_vin;
    public $car_model;
    public $first_car_purchase_date;
    public $last_car_purchase_date;
    public $dealer;
    public $dealer_town;
    public $town;
    public $connection;
    public $membership_days;
    public $count_cars;
    public $manual_register;
    public $crm_verification;
    public $statusSecond;
    public $middlename;
    public $birthday;
    public $gender;
    public $card_number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'bmw_status', 'updated_at', 'count_cars'], 'integer'],
            [
                [
                    'auth_key', 'car_vin', 'town', 'car_model', 'dealer', 'membership_days', 'connection',
                    'dealer_town', 'last_car_purchase_date', 'first_car_purchase_date', 'password_hash',
                    'email','firstname','lastname','phone', 'bmw_id', 'created_at', 'manual_register',
                    'crm_verification', 'statusSecond', 'middlename', 'birthday', 'gender', 'card_number',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->joinWith(['profile','lastCar lastCar', 'firstCar firstCar','lastCar.dealerModel dealer'])
            ->groupBy('user.id')
            ->orderBy('user.id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'bmw_status' => $this->bmw_status,
            'user.updated_at' => $this->updated_at,
            'manual_register' => $this->manual_register,
            "DATE_FORMAT(crm_verification, '%d.%m.%Y')" => $this->crm_verification,
            "DATE_FORMAT(birthday, '%d.%m.%Y')" => $this->birthday,
            'gender' => $this->gender,
            "DATE_FORMAT(lastCar.purchase_date, '%d.%m.%Y')" => $this->last_car_purchase_date,
            "DATE_FORMAT(firstCar.purchase_date, '%d.%m.%Y')" => $this->first_car_purchase_date,
            'card_number' => $this->card_number,
        ]);

        if( $this->statusSecond ){
            $query->andFilterWhere([
                'user.status' => $this->statusSecond,
                'bmw_status' => User::STATUS_CRM_PASSED,
            ]);
        }else{
            $query->andFilterWhere([
                'user.status' => $this->status,
            ]);
        }

        if($this->created_at){
            $query->andFilterWhere([
                "DATE_FORMAT(FROM_UNIXTIME(user.created_at), '%d.%m.%Y')" => $this->created_at,
                'manual_register' => 0,
            ]);
        }

        $query->andFilterWhere([
            'datediff(\''.date('Y-m-d',time()).'\',lastCar.purchase_date )' => $this->membership_days,
        ]);
        if (!empty($this->count_cars) ||  $this->count_cars=='0') {
            $query->having(['count(lastCar.id)' => pow($this->count_cars, 2)]);
        }
        $query->andFilterWhere(['like', 'user.auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'user.password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'user.email', $this->email])

            ->andFilterWhere(['like', 'user_profile.lastname', $this->lastname])
            ->andFilterWhere(['like', 'user_profile.bmw_id', $this->bmw_id])
            ->andFilterWhere(['like', 'user_profile.firstname', $this->firstname])
            ->andFilterWhere(['like', 'user_profile.middlename', $this->middlename])
            ->andFilterWhere(['like', 'user_profile.phone', $this->phone])
            ->andFilterWhere(['like', 'lastCar.vin', $this->car_vin])
            ->andFilterWhere(['like', 'lastCar.model', $this->car_model])
            ->andFilterWhere(['like', 'dealer.name', $this->dealer])
            ->andFilterWhere(['like', 'lastCar.dealer_town', $this->dealer_town]);

        return $dataProvider;
    }


}
