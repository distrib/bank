<?php

namespace app\modules\admin\models\search;

use app\models\UserCars;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserCarsSearch extends UserCars
{

    public $manual_register;
    public $crm_verification;
    public $bmw_status;
    public $bmw_id;

    public $firstname;
    public $lastname;
    public $middlename;

    public $town;

    public $email;
    public $phone;






    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'manual_register', 'bmw_status', 'manual_register', 'created_at', 'updated_at'], 'integer'],
            [[ 'vin', 'model', 'bmw_id','dealer_town', 'dealer', 'crm_verification', 'middlename', 'firstname', 'lastname', 'town',  'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserCars::find()->joinWith(['user','profile'])

           ->orderBy('user_cars.created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_cars.id' => $this->id,
            'user.manual_register' => $this->manual_register,
            'user_cars.purchase_date' => $this->purchase_date,
            'user.crm_verification' => $this->crm_verification,

        ]);

        if ($this->bmw_status == 1)
            $query->andFilterWhere(['user.bmw_status' => [1, 2]]); else $query->andFilterWhere(['user.bmw_status' => $this->bmw_status]);



        $query->andFilterWhere(['like', 'user_cars.vin', $this->vin])
            ->andFilterWhere(['like', 'user_cars.model', $this->model])
            ->andFilterWhere(['like', '.dealer', $this->dealer])
            ->andFilterWhere(['like', 'user_profile.lastname', $this->lastname])
            ->andFilterWhere(['like', 'user_profile.middlename', $this->middlename])
            ->andFilterWhere(['like', 'user_cars.dealer_town', $this->dealer_town])
            ->andFilterWhere(['like', 'user_profile.bmw_id', $this->bmw_id])
            ->andFilterWhere(['like', 'user_profile.firstname', $this->firstname])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'user_profile.phone', $this->phone])
            ->andFilterWhere(['like', 'user_profile.town', $this->town]);

        return $dataProvider;
    }


}
