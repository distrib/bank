<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class ActivityReportSearch extends User
{
    public $bmw_id;
    public $firstname;
    public $lastname;
    public $middlename;




    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'login_count'], 'integer'],
            [[ 'middlename', 'firstname','lastname', 'bmw_id','activation_link_date','last_login_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->joinWith(['profile'])
            ->where(['user.status'=>[User::STATUS_RESTORED,User::STATUS_DELETED,User::STATUS_ACTIVE]])
            ->groupBy('user.id')->orderBy('user.created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.activation_link_date' => $this->activation_link_date,
            'user.last_login_date' => $this->last_login_date,
            'user.login_count' => $this->login_count,
        ]);


        $query->andFilterWhere(['like', 'user.auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'user_profile.lastname', $this->lastname])
            ->andFilterWhere(['like', 'user_profile.middlename', $this->middlename])
            ->andFilterWhere(['like', 'user_profile.bmw_id', $this->bmw_id])
            ->andFilterWhere(['like', 'user_profile.firstname', $this->firstname]);



        return $dataProvider;
    }


}
