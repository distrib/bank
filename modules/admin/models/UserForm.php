<?php
namespace app\modules\admin\models;

use app\models\User;
use yii\base\Exception;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Create user form
 */
class UserForm extends Model
{
    public $id;
    public $email;
    public $password;
    public $manual_club_date;
    public $status;
    public $bmw_status;
    public $roles;

    private $model;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [


            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass'=> User::className(), 'filter' => function ($query) {
                if (!$this->getModel()->isNewRecord) {
                    $query->andWhere(['not', ['id'=>$this->getModel()->id]]);
                }
            }],

            ['password', 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6],
            ['manual_club_date', 'string'],
            [['status'], 'integer'],
            [['bmw_status'], 'integer'],
            [['roles'], 'each',
                'rule' => ['in', 'range' => ArrayHelper::getColumn(
                    Yii::$app->authManager->getRoles(),
                    'name'
                )]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'email' => 'E-mail',
            'status' => 'Статус',
            'bmw_status' => 'Статус выгрузки',
            'password' => 'Пароль',
            'roles' => 'Roles',
            'manual_club_date' => 'Дата начала участия в клубе (ручной ввод)',
        ];
    }

    /**
     * @param User $model
     * @return mixed
     */
    public function setModel($model)
    {
        $this->id = $model->id;
        $this->email = $model->email;
        $this->status = $model->status;
        $this->bmw_status = $model->bmw_status;
        $this->manual_club_date = $model->manual_club_date;
        $this->model = $model;
        $this->roles = ArrayHelper::getColumn(
            Yii::$app->authManager->getRolesByUser($model->getId()),
            'name'
        );
        return $this->model;
    }

    /**
     * @return User
     */
    public function getModel()
    {
        if (!$this->model) {
            $this->model = new User();
        }
        return $this->model;
    }

    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     * @throws Exception
     */
    public function save()
    {
        if ($this->validate()) {
            $model = $this->getModel();
            $isNewRecord = $model->getIsNewRecord();

            $model->email = $this->email;
            $model->status = $this->status;

            if( strlen($this->manual_club_date) ){
                $manual_date = \DateTime::createFromFormat('d.m.Y', $this->manual_club_date);
                $model->manual_club_date = date_format($manual_date, 'Y-m-d');
            } else {

                $model->manual_club_date = null;

            }

            if ($this->password) {
                $model->setPassword($this->password);
            }
            if (!$model->save()) {
                throw new Exception('Model not saved');
            }else $this->id=$model->id;
            if ($isNewRecord) {
                $model->afterSignup();
            }
            $auth = Yii::$app->authManager;
            $auth->revokeAll($model->getId());

            if ($this->roles && is_array($this->roles)) {
                foreach ($this->roles as $role) {
                    $auth->assign($auth->getRole($role), $model->getId());
                }
            }

            return !$model->hasErrors();
        }
        return null;
    }
}
