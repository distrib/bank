<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ImportUsersLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Import Users Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-users-log-index">

    <h3>Импортировать пользователей</h3>
    <?php $form = ActiveForm::begin(['action' => ['/admin/import-users/set-xls'], 'options' => ['enctype' => 'multipart/form-data', 'class'=>'form-control' ]]) ?>

    <?=Html::fileInput('xls')?><br>
    <?=Html::submitButton('Отправить',['class' => 'btn btn-success'])?>
    <?php ActiveForm::end()?>


    <br>
    <br>
    <br>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

       //     'id',
            'new_users',
            'updated_users',
            'mismatch_users',
            'created_at:datetime',
            //'updated_at',

          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
