<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialOfferings */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Системный справочник', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="special-offerings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'cod',
            'value',
            [
                'attribute'=>'created_at',
                'format'=>'datetime',
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $model,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
                ]),
            ],
            [
                'attribute'=>'updated_at',
                'format'=>'datetime',
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $model,
                    'attribute' => 'updated_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
                ]),
            ],
        ],
    ]) ?>

</div>
