<?
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $searchFeedbackModel app\models\search\EventToUsersSearch */
/* @var $dataProviderFeedback yii\data\ActiveDataProvider */

$this->title = 'Редактировать запись: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<div class="system-reference-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

