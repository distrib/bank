<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialOfferings */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="system-reference-form">
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'cod')->textInput() ?>

        <?= $form->field($model, 'name')->textInput() ?>

        <?= $form->field($model, 'value')->textInput() ?>

        <?= $form->field($model, 'picture')->widget(
            \trntv\filekit\widget\Upload::className(),
            [
                'url' => ['/admin/file-storage/upload'],
                'maxFileSize' => 5*1024*1024, // 5 MiB
                'maxNumberOfFiles' => 1,
                'multiple' => false,
            ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
