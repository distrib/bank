<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Системный справочник';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">
    <!--p>
        <?= Html::a('Создать запись', ['create'], ['class' => 'btn btn-success']) ?>
    </p-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['class' => 'freeze'];
                },
            ],
            ['class' => 'yii\grid\SerialColumn'],
            'cod',
            'name',
            'value',
            [
                'attribute' => 'imageHelper',
                'label' => 'Изображение',
                'format' => 'raw',
                'filter' => [
                    0 => 'Нет',
                    1 => 'Есть',
                ],
                'value' => function ($model) {

                    if( strlen($model->image) )
                        return "<span style='color:green'>Есть</span>";
                    else
                        return "<span style='color:red'>Нет</span>";

                },
            ],
            [
                'attribute'=>'created_at',
                'format'=>'datetime',
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
                ]),
            ],
            [
                'attribute'=>'updated_at',
                'format'=>'datetime',
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
                ]),
            ],
        ],
    ]);?>

</div>
