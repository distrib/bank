<?php

/* @var $this yii\web\View */

$this->title = 'Оповещения';
?>
<div class="admin-panel">
    <div class="row">
        <div class="col-lg-8 col-lg-push-2">
            <div class="row">

                <div class="col-lg-4">
                    <div class="card">
                        <div class="title">
                            Заявок на регистрацию
                        </div>

                        <div class="text">
                            <a href="/admin/user"><?= $user?></a>
                            /
                            <a href="#">0</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="title">
                            Заявок на спец. предложения
                        </div>

                        <div class="text">
                            <a href="#"><?= $offerings?></a>
                            /
                            <a href="#">0</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="title">
                            Заявок на события
                        </div>

                        <div class="text">
                            <a href="/admin/event-to-users"><?= $eventsAll?></a>
                            /
                            <a href="#">0</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="title">
                            Заявок на обратную связь
                        </div>

                        <div class="text">
                            <a href="/admin/callback"><?= $callback?></a>
                            /
                            <a href="#">0</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="title">
                            Заявок «не удается войти»
                        </div>

                        <div class="text">
                            <a href="/admin/bug-message"><?= $bugMessagesAll?></a>
                            /
                            <a href="#">0</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="title">
                            Оплаченных заявок
                        </div>

                        <div class="text">
                            <a href="#">0</a>
                            /
                            <a href="#">0</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
