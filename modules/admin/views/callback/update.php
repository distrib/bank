<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $searchFeedbackModel app\models\search\EventToUsersSearch */
/* @var $dataProviderFeedback yii\data\ActiveDataProvider */

$this->title = 'Редактировать обратную связь:';
$this->params['breadcrumbs'][] = ['label' => 'Обратная связь', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>


<div class="callback-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
