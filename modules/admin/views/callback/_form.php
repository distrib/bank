<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="callback-form">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id',
                'label' => 'Номер заявки'
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата заявки',
                'format'=>['date', 'php:d.m.Y'],
            ],
            [
                'attribute'=>'created_at',
                'label' => 'Время заявки ',
                'format'=>['time', 'php:H:i:s'],
            ],
            [
                'label'=>'ID участника',
                'attribute'=>'user_id',
            ],
            [
                'label'=>'ФИО',
                'attribute'=>'user_id',
                'format' => 'raw',
                'value'=>function($data){
                    //return $data->user->profile->fullName;
                    return Html::a(
                        $data->user->profile->fullName, // text
                        '/admin/user/update?id='.$data->user->id,
                        [
                            'title' => $data->user->email,
                            'target' => '_blank'
                        ]
                    );
                },
            ],
            [
                'label'=>'Город пользователя',
                'attribute'=>'user_id',
                'format' => 'raw',
                'value'=>function($data){
                    return $data->user->profile->town;
                },
            ],
            [
                'attribute'=>'user_id',
                'format'=>'text',
                'label'=>'Почта',
                'value'=>function($data){
                    return $data->user->email;
                },
            ],
            [
                'label'=>'Телефон',
                'attribute'=>'user_id',
                'format' => 'raw',
                'value'=>function($data){
                    return $data->user->profile->phone;
                },
            ],
            [
                'label'=>'Способ связи ',
                'attribute'=>'user_id',
                'format' => 'raw',
                'value'=>function($data){
                    if( $data->user->profile->connection == 1 ){
                        return 'по телефону';
                    }else{
                        return 'по почте';
                    }
                },
            ],
            [
                'label'=>'Текст сообщения',
                'attribute'=>'text',
            ],
        ],
    ]) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'callback-form-editor',
    ]); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'processed')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
