<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обратная связь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callback-index">
    <?php Pjax::begin(); ?>

        <?
        $columns = [
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            '/admin/callback/update?id='.$model['id']);
                    },
                ],
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['class' => 'freeze'];
                },
            ],
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'id',
                'format'=>'text',
                'label'=>'Номер заявки',
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата заявки',
                'format'=>['date', 'php:d.m.Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
            ],
            [
                'attribute'=>'created_at',
                'label' => 'Время заявки ',
                'filter' => false,
                'format'=>['time', 'php:H:i:s'],
            ],
            [
                'label' => 'Статус обработки заявки',
                'attribute'=>'processed',
                'filter' => [
                    0 => 'Не обработана',
                    1 => 'Обработана',
                ],
                'content'=>function($data){
                    if ($data['processed'] == 1)
                        return "<span style='color:green'>Обработана</span>";
                    else
                        return "<span style='color:red'>Не обработана</span>";
                },
            ],
            [
                'attribute'=>'processed_time',
                'label'=>'Дата обработки заявки',
                'format'=>['date', 'php:d.m.Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $searchModel,
                    'attribute' => 'processed_time',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
            ],
            [
                'attribute'=>'processed_time',
                'label' => 'Время обработки заявки',
                'filter' => false,
                'format'=>['time', 'php:H:i:s'],
            ],
            [
                'label'=>'Текст сообщения',
                'attribute'=>'text',
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['class' => 'text'];
                },
            ],
            [
                'label'=>'ID участника',
                'attribute'=>'user_id',
            ],
            [
                'attribute'=>'fullname',
                'format'=>'text',
                'filter'=> false,
                'label'=>'ФИО',
            ],
            [
                'attribute'=>'town',
                'format'=>'text',
                'label'=>'Город пользователя',
            ],
            [
                'attribute'=>'email',
                'format'=>'text',
                'label'=>'Почта',
                'content'=>function($data){
                    //return $data->user->profile->fullName;
                    return Html::a(
                        $data["email"],
                        '/admin/user/update?id='.$data['user_id'],
                        [
                            //'title' => $data->user->email,
                            'target' => '_blank'
                        ]
                    );
                },
            ],
            [
                'attribute'=>'phone',
                'format'=>'text',
                'label'=>'Телефон',
            ],
            [
                'label' => 'Способ связи',
                'attribute'=>'connection',
                'filter' => [
                    1 => 'по телефону',
                    2 => 'по почте',
                ],
                'content'=>function($data){
                    if ($data['connection'] == 1)
                        return "<span style='color:green'>по телефону</span>";
                    else
                        return "<span style='color:red'>по почте</span>";
                },
            ],
        ];

        echo \kartik\export\ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'exportConfig' => [
                \kartik\export\ExportMenu::FORMAT_TEXT => false,
                \kartik\export\ExportMenu::FORMAT_HTML => false,
                \kartik\export\ExportMenu::FORMAT_EXCEL => false,
                \kartik\export\ExportMenu::FORMAT_PDF =>  false,
                \kartik\export\ExportMenu::FORMAT_CSV =>  false
            ],

        ]);

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [
                'class' => 'grid-view table-responsive'
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-admin-callback',
            ],
            'rowOptions'=> function ($model, $key, $index, $grid){
                if($model["processed"] == 0){
                    return ['class'=>'new'];
                }
            },
            'columns' => $columns,
        ]); ?>

    <?php Pjax::end(); ?>
</div>
