<?php

use app\modules\menu\models\Menu;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/image/logo.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $profile->firstname." ".$profile->lastname;?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'BMW MENU', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Пользователи',
                        'icon' => 'address-card',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Список пользователей', 'icon' => 'list','url' => ['/admin/user']],
                            ['label' => 'Импорт пользователей', 'icon' => 'list', 'url' => ['/admin/import-users']],
                        ],
                    ],
                    [
                        'label' => 'Модерация',
                        'icon' => 'pencil-square-o',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Заявки на «специальные предложения»',
                                'icon' => 'list',
                                'url' => ['/admin/offering-to-users'],
                                'template'=>'<a href="{url}" class="all">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-blue">'.$offeringsAll.'</small></span></a><a href="#" class="new">&nbsp;<span class="pull-right-container"><small class="label pull-right bg-red">'.$offeringsNew.'</small></span></a>',
                            ],
                            [
                                'label' => 'Заявки на «события»',
                                'icon' => 'list',
                                'url' => ['/admin/event-to-users'],
                                'template'=>'<a href="{url}" class="all">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-blue">'.$eventsAll.'</small></span></a><a href="#" class="new">&nbsp;<span class="pull-right-container"><small class="label pull-right bg-red">'.$eventsNew.'</small></span></a>',
                            ],
                            [
                                'label' => 'Обращения «обратная связь»',
                                'icon' => 'list',
                                'url' => ['/admin/callback'],
                                'template'=>'<a href="{url}" class="all">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-blue">'.$callbackAll.'</small></span></a><a href="{url}" class="new">&nbsp;<span class="pull-right-container"><small class="label pull-right bg-red">'.$callbackNew.'</small></span></a>',
                            ],
                            [
                                'label' => 'Обращения «не удается войти»',
                                'icon' => 'list',
                                'url' => ['/admin/bug-message'],
                                'template'=>'<a href="{url}" class="all">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-blue">'.$bugMessagesAll.'</small></span></a><a href="#" class="new">&nbsp;<span class="pull-right-container"><small class="label pull-right bg-red">'.$bugMessagesNew.'</small></span></a>',
                            ],
                            [
                                'label' => 'Заявки на оплату',
                                'icon' => 'list',
                                'url' => ['/admin/payments'],
                                'template'=>'<a href="{url}" class="all">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-blue">0</small></span></a><a href="#" class="new">&nbsp;<span class="pull-right-container"><small class="label pull-right bg-red">0</small></span></a>',
                            ],
                            ['label' => 'Справочник дилеров', 'icon' => 'list', 'url' => ['/admin/dealers']],
                        ],
                    ],
                    [
                        'label' => 'Контент',
                        'icon' => 'gift',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Календарь событий', 'icon' => 'list', 'url' => ['/admin/events']],
                            ['label' => 'Специальные предложения', 'icon' => 'list', 'url' => ['/admin/special-offerings']],
                            ['label' => 'Меню', 'icon' => 'list', 'url' => \yii\helpers\Url::to(['/admin/menu/default/place','id'=> Menu::TYPE_HEADER])],
                            ['label' => 'Баннер на главной', 'icon' => 'list', 'url' => ['/admin/system-reference']],
                            ['label' => 'Интересы', 'icon' => 'list', 'url' => ['/admin/interests']],
                        ],
                    ],
                    [
                        'label' => 'Отчеты',
                        'icon' => 'sticky-note-o ',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Отчет по пользователям', 'icon' => 'list', 'url' => ['/admin/report/users']],
                            ['label' => 'Отчет по автомобилям', 'icon' => 'list', 'url' => ['/admin/report/cars']],
                            ['label' => 'Отчет по спец. предложениям', 'icon' => 'list', 'url' => ['/admin/report/offerings']],
                            ['label' => 'Отчет по событиям', 'icon' => 'list', 'url' => ['/admin/report/events']],
                            ['label' => 'Отчет по оплатам', 'icon' => 'list', 'url' => ['/admin/report/user']],
                            ['label' => 'Отчет по интересам', 'icon' => 'list', 'url' => ['/admin/report/interests']],
                            ['label' => 'Отчет по обращениям «не удается войти»', 'icon' => 'list', 'url' => ['/admin/report/bug']],
                            ['label' => 'Отчет по обращениям «обратная связь»', 'icon' => 'list', 'url' => ['/admin/report/callback']],
                            ['label' => 'Отчет по активности', 'icon' => 'list', 'url' => ['/admin/report/activity']],
                            ['label' => 'Отчет по подаркам', 'icon' => 'list', 'url' => ['/admin/report/user']],
                        ],
                    ],

//                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                ],
            ]
        ) ?>

    </section>

</aside>
