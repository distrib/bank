<?php

use app\modules\admin\assets\AppAsset;
use yii\helpers\Html;
use app\models\UserProfile;
use app\models\User;
use app\models\Callback;
use app\models\BugMessages;
use app\models\OfferingToUsers;
use app\models\EventToUsers;

/* @var $this \yii\web\View */
/* @var $content string */

if (class_exists('app\modules\admin\assets\AppAsset')) {
    app\modules\admin\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <?

    $profile = Yii::$app->user->identity->profile;
    $warning = [];
    if( User::getPassedCount() ){
        $warning[] = User::getPassedCount().' пользователь(ей) ждут приглашения';
    }
    if( User::getMismatchCount() ){
        $warning[] = User::getMismatchCount().' пользователь(ей) имеют несоответствия';
    }
    if( User::getDuplicateCount() ){
        $warning[] = User::getDuplicateCount().' пользователь(ей) имеют Дубль VIN';
    }
    ?>
    <?= $this->render(
        'header.php',
        [
            'directoryAsset' => $directoryAsset,
            'profile' => $profile,
            'warning' => $warning,
        ]
    ) ?>

    <?
        $offeringsAll = OfferingToUsers::find()->count();
        $eventsAll = EventToUsers::find()->count();
        $callbackAll = Callback::find()->count();
        $bugMessagesAll = BugMessages::find()->count();

        $offeringsNew = OfferingToUsers::find()->where('application_status=0')->count();
        $eventsNew = EventToUsers::find()->where('application_status=0')->count();
        $callbackNew = Callback::find()->where('processed<>1')->count();
        $bugMessagesNew = BugMessages::find()->where('processed<>1')->count();
    ?>
    <?= $this->render(
        'left.php',
        [
            'directoryAsset' => $directoryAsset,
            'profile' => $profile,
            'offeringsAll' => $offeringsAll,
            'eventsAll' => $eventsAll,
            'callbackAll' => $callbackAll,
            'bugMessagesAll' => $bugMessagesAll,
            'offeringsNew' => $offeringsNew,
            'eventsNew' => $eventsNew,
            'callbackNew' => $callbackNew,
            'bugMessagesNew' => $bugMessagesNew,
        ]
    )
    ?>

    <?= $this->render(
        'content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

