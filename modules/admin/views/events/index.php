<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мероприятия';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">


    <p>
        <?= Html::a('Создать мероприятие', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?
    $columns = [
        [
            'class' => 'yii\grid\ActionColumn',
            'template'=>'{update} {delete}',
            'contentOptions' =>function ($model, $key, $index, $column){
                return ['class' => 'freeze'];
            },
        ],
        ['class' => 'yii\grid\SerialColumn'],
        'title',
        // 'description:ntext',
        [
            'attribute' => 'published',
            'filter' => [
                0 => 'Нет',
                1 => 'Да',
            ],
            'content'=>function($data){
                if ($data['published'] == 1)
                    return "<span style='color:green'>Да</span>";
                else
                    return "<span style='color:red'>Нет</span>";
            },
        ],
        [
            'attribute' => 'active',
            'filter' => [
                0 => 'Нет',
                1 => 'Да',
            ],
            'content'=>function($data){
                if ($data['active'] == 1)
                    return "<span style='color:green'>Да</span>";
                else
                    return "<span style='color:red'>Нет</span>";
            },
        ],
        [
            'attribute' => 'main',
            'filter' => [
                0 => 'Нет',
                1 => 'Да',
            ],
            'content'=>function($data){
                if ($data['main'] == 1)
                    return "<span style='color:green'>Да</span>";
                else
                    return "<span style='color:red'>Нет</span>";
            },
        ],
        [
            'attribute' => 'type',
            'format' => 'raw',
            'filter' => [
                0 => 'Локальное',
                1 => 'Глобальное',
            ],
            'value' => function ($model) {

                if ($model['type'] == 1)
                    return "<span style='color:green'>Глобальное</span>";
                elseif ($model->type == 0)
                    return "<span style='color:blue'>Локальное</span>";

            },
        ],
        [
            'attribute' => 'pay',
            'label' => 'Доступ',
            'format' => 'raw',
            'filter' => [
                0 => 'Бесплатное',
                1 => 'Платное',
            ],
            'value'=>function($model){
                if ($model->pay == 0)
                    return "<span style='color:green'>Бесплатное</span>";
                else
                    return "<span style='color:red'>Платное</span>";
            },
        ],
        [
            'attribute'=>'pay_price',
            'label' => 'Стоимость',
        ],


        [
            'attribute' => 'limit',
            'label' => 'Лимит',
        ],
        [
            'attribute'=> 'countOrderByEvent',
            'format'=>'text',
            'filter'=>false,
        ],
        [
            'attribute' => 'date_start',
            'format'=>'date',
            'filter' => \yii\jui\DatePicker::widget([
                'options' => ['class' => 'form-control'],
                'model' => $searchModel,
                'attribute' => 'date_start',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
            ]),
        ],
        [
            'attribute'=>'date_end',
            'format'=>'date',
            'filter' => \yii\jui\DatePicker::widget([
                'options' => ['class' => 'form-control'],
                'model' => $searchModel,
                'attribute' => 'date_end',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
            ]),
        ],
    ];

    echo \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'exportConfig' => [
            \kartik\export\ExportMenu::FORMAT_TEXT => false,
            \kartik\export\ExportMenu::FORMAT_HTML => false,
            \kartik\export\ExportMenu::FORMAT_EXCEL => false,
            \kartik\export\ExportMenu::FORMAT_PDF =>  false,
            \kartik\export\ExportMenu::FORMAT_CSV =>  false
        ],
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => $columns,
    ]); ?>


</div>
