<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(['action'=>'/admin/events/reject','method'=>'POST']);
?>
<?= Html::hiddenInput('id',$id)?>
<?= \yii\helpers\Html::textarea('reason',null,['class="form-control']);?>
 <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>