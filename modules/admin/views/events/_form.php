<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="events-form">

    <?php $form = ActiveForm::begin([
        'id' => 'event-form-editor',
    ]); ?>

    <?= $form->errorSummary($model); ?>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Основное</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Содержимое</a></li>
            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Оплата</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <?= $form->field($model, 'title')->textInput(['maxlength' => false]) ?>

                <?= $form->field($model, 'slug')->textInput(['readonly'=> true]) ?>

                <?= $form->field($model, 'published')->checkbox() ?>

                <?= $form->field($model, 'active')->checkbox() ?>

                <?= $form->field($model, 'type')->dropDownList([0=>'Локальное', 1=>'Глобальное']) ?>

                <?= $form->field($model, 'limit')->textInput(['type' => 'number', 'min' => 1, 'max' => 5000]) ?>

                <?= $form->field($model, 'main')->checkbox() ?>

                <?php if (empty($model->main_order)) $model->main_order = 10;?>
                <?= $form->field($model, 'main_order')->textInput(['type' => 'number', 'min' => 1, 'max' => 5000]) ?>

                <?= $form->field($model, 'date_start')->widget(\yii\jui\DatePicker::className(), [
                    'options' => ['class' => 'form-control'],
                    'dateFormat' => 'dd.MM.yyyy',
                ]) ?>

                <?= $form->field($model, 'date_end')->widget(\yii\jui\DatePicker::className(), [
                    'options' => ['class' => 'form-control'],
                    'dateFormat' => 'dd.MM.yyyy',
                ]) ?>
            </div>

            <div class="tab-pane" id="tab_2">

                <?= $form->field($model, 'picture_preview')->widget(
                    \trntv\filekit\widget\Upload::className(),
                    [
                        'url' => ['/admin/file-storage/upload'],
                        'maxFileSize' => 5*1024*1024, // 5 MiB
                    ]);
                ?>

                <?= $form->field($model, 'picture')->widget(
                    \trntv\filekit\widget\Upload::className(),
                    [
                        'url' => ['/admin/file-storage/upload'],
                        'maxFileSize' => 5*1024*1024, // 5 MiB
                    ]);
                ?>

                <?= $form->field($model, 'slider')->widget(
                    \trntv\filekit\widget\Upload::className(),
                    [
                        'url' => ['/admin/file-storage/upload'],
                        'maxFileSize' => 5*1024*1024, // 5 MiB
                        'maxNumberOfFiles' => 5,
                        'multiple' => true,
                    ]);
                ?>

                <?= $form->field($model, 'description')->widget(\xvs32x\tinymce\Tinymce::className())?>
            </div>

            <div class="tab-pane" id="tab_3">


                <?= $form->field($model, 'pay')->checkbox() ?>

                <?= $form->field($model, 'pay_description')->widget(\xvs32x\tinymce\Tinymce::className())?>

                <?= $form->field($model, 'pay_price')->textInput() ?>

                <?= $form->field($model, 'pay_account')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'pay_agreement')->textInput(['maxlength' => true]) ?>
            </div>

        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
