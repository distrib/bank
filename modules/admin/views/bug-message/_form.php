<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="bug-message-form">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id',
                'label' => 'Номер заявки'
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата заявки',
                'format'=>['date', 'php:d.m.Y'],
            ],
            [
                'attribute'=>'created_at',
                'label' => 'Время заявки ',
                'format'=>['time', 'php:H:i:s'],
            ],
            [
                'attribute'=>'name',
                'label' => 'Имя отправителя'
            ],
            [
                'attribute'=>'phone',
                'label' => 'Телефон'
            ],
            [
                'attribute'=>'problem',
                'label' => 'Текст сообщения'
            ],
            [
                'attribute'=>'screen',
                'label' => 'Скриншот',
                'value'=> '/uploads'.$model->screen,
                'format' => ['image', ['width'=>'100%','height'=>'auto']],
            ],
        ],
    ]) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'bug-message-form-editor',
    ]); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'processed')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
