<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обращения об ошибках';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bug-message-index">
    <?php Pjax::begin(); ?>
        <?
        $columns = [
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['class' => 'freeze'];
                },
            ],
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'id',
                'format'=>'text',
                'label'=>'Номер обращения',
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата обращения',
                'format'=>['date', 'php:d.m.Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
            ],
            [
                'attribute'=>'created_at',
                'label' => 'Время обращения',
                'format'=>['time', 'php:H:i:s'],
                'filter' => false,
            ],
            [
                'label' => 'Статус обработки обращения',
                'attribute'=>'processed',
                'filter' => [
                    0 => 'Нет',
                    1 => 'Да',
                ],
                'content'=>function($model){
                    if ($model->processed == 1)
                        return "<span style='color:green'>Да</span>";
                    else
                        return "<span style='color:red'>Нет</span>";
                },
            ],
            [
                'attribute'=>'processed_time',
                'label'=>'Дата обработки обращения',
                'format'=>['date', 'php:d.m.Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'model' => $searchModel,
                    'attribute' => 'processed_time',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
            ],
            [
                'attribute'=>'processed_time',
                'label' => 'Время обработки обращения',
                'format'=>['time', 'php:H:i:s'],
                'filter' => false,
            ],
            [
                'attribute'=>'problem',
                'label' => 'Текст обращения',
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['class' => 'text'];
                },
            ],
            [
                'attribute' => 'screenHelper',
                'label' => 'Скриншот',
                'format' => 'raw',
                'filter' => [
                    0 => 'Нет',
                    1 => 'Есть',
                ],
                'value' => function ($model) {

                    if( strlen($model->screen) )
                        return "<span style='color:green'>Есть</span>";
                    else
                        return "<span style='color:red'>Нет</span>";

                },
            ],
            [
                'attribute'=>'name',
                'format'=>'text',
                'label'=>'Имя отправителя',
            ],
            [
                'attribute'=>'phone',
                'format'=>'text',
                'label'=>'Телефон',
            ],
        ];

        echo \kartik\export\ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'exportConfig' => [
                \kartik\export\ExportMenu::FORMAT_TEXT => false,
                \kartik\export\ExportMenu::FORMAT_HTML => false,
                \kartik\export\ExportMenu::FORMAT_EXCEL => false,
                \kartik\export\ExportMenu::FORMAT_PDF =>  false,
                \kartik\export\ExportMenu::FORMAT_CSV =>  false
            ],

        ]);

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [
                'class' => 'grid-view table-responsive table-admin-bug-message'
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered',
            ],
            'rowOptions'=> function ($model, $key, $index, $grid){
                if($model->processed == 0){ return ['class'=>'new']; }
            },
            'columns' => $columns,
        ]); ?>
    <?php Pjax::end(); ?>
</div>
