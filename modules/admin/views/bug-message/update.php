<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $searchFeedbackModel app\models\search\EventToUsersSearch */
/* @var $dataProviderFeedback yii\data\ActiveDataProvider */

$this->title = 'Редактировать обращение "Не удается войти":';
$this->params['breadcrumbs'][] = ['label' => 'Обращение', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>


<div class="bug-message-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
