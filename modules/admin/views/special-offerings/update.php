<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialOfferings */
/* @var $searchFeedbackModel app\models\search\EventToUsersSearch */
/* @var $dataProviderFeedback yii\data\ActiveDataProvider */

$this->title = 'Редактировать предложение: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Предложения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>


<div class="events-update">

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class=""><a href="#main_tab_1" data-toggle="tab" aria-expanded="true">Мероприятие</a></li>
            <li class="active"><a href="#main_tab_2" data-toggle="tab" aria-expanded="false">Заявки</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane " id="main_tab_1">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>

            <div class="tab-pane active" id="main_tab_2">
                <?php \yii\widgets\Pjax::begin() ?>
                <?
                $columns = [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    //'event_id',
                    ['attribute' => 'user_id',
                        'value' => 'user.email'
                    ],
                    ['attribute' => 'application_status',
                        'format' => 'raw',
                        'filter' => [

                            0 => 'Новая',
                            1 => 'Одобрена',
                            2 => 'Отклонена',
                            3 => 'В процессе',

                        ],
                        'value' => function ($model) {


                            if ($model->application_status == 1)
                                return "<span style='color:green'>Одобрена</span>";
                            elseif ($model->application_status == 0)
                                return "<span style='color:gray'>Новая</span>";
                            elseif ($model->application_status == 2)
                                return "<span style='color:red'>Отклонена</span>";
                            elseif ($model->application_status == 3)
                                return "<span style='color:#97a0b3'>В процессе</span>";

                        },
                    ],
                    // 'status_comment:ntext',
                    //'pay_status',
                    //'pay_time:datetime',
                    //'status_change_time:datetime',
                    [
                        'attribute' => 'created_at',
                        'format' => 'date',
                        'filter' => \yii\jui\DatePicker::widget([
                            'options' => ['class' => 'form-control'],
                            'model' => $searchFeedbackModel,
                            'attribute' => 'created_at',
                            'language' => 'ru',
                            'dateFormat' => 'dd.MM.yyyy',
                        ]),
                    ],
                    'status_comment',

                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{reject} {accept} {process}',
                        'buttons' => [
                            'accept' => function ($url, $model, $key) {

                                if ($model->application_status != 1) {
                                    $iconName = "ok";

                                    $title = 'Одобрить';
                                    $url = \yii\helpers\Url::current(['accept', 'id' => $model->id]);

                                    //Для стилизации используем библиотеку иконок
                                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);

                                    return Html::a($icon, $url,['title'=>$title]);
                                } else {
                                    return '';
                                }
                            },

                            'reject' => function ($url, $model, $key) {
                                if ($model->application_status != 2) {
                                    $iconName = "remove";

                                    //Текст в title ссылки, что виден при наведении
                                    $title = 'Отклонить';

                                    $id = 'reject-' . $key;
                                    $options = [
                                        'title' => $title,
                                        'aria-label' => $title,
                                        'data-pjax' => 'true',
                                        'id' => $id
                                    ];

                                    $url = \yii\helpers\Url::current(['reject', 'id' => $key]);

                                    //Для стилизации используем библиотеку иконок
                                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);

                                    $crsf = \Yii::$app->getRequest()->getCsrfToken();
                                    //Обработка клика на кнопку
                                    $js = <<<JS
            $("#{$id}").on("click",function(event){  
                    event.preventDefault();
                    var myModal = $("#myModal");
                    var modalBody = myModal.find('.modal-body');
                    var modalTitle = myModal.find('.modal-header');
                    
                    modalBody.html('<form id="w4" action="/admin/special-offerings/reject" method="POST"><input type="hidden" name="_csrf" value="{$crsf}"><input type="hidden" name="id" value="{$model->id}"><textarea name="reason" class="form-control" required></textarea> <div class="form-group">   <br>     <button type="submit" class="btn btn-success">Отправить</button>    </div></form>');

                    myModal.modal("show");
                }
            );
JS;


                                    //Регистрируем скрипты
                                    $this->registerJs($js, \yii\web\View::POS_READY, $id);

                                    return Html::a($icon, $url, $options);
                                } else {
                                    return '';
                                }
                            },
                            'process' => function ($url, $model, $key) {

                                if ($model->application_status == 0) {
                                    $iconName = "time";
                                    $title = 'В процессе';
                                    $url = \yii\helpers\Url::current(['process', 'id' => $model->id]);

                                    //Для стилизации используем библиотеку иконок
                                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);

                                    return Html::a($icon, $url,['title'=>$title]);
                                } else {
                                    return '';
                                }
                            },
                        ],
                    ],
                ];

                echo \kartik\export\ExportMenu::widget([
                    'dataProvider' => $dataProviderFeedback,
                    'columns' => $columns,
                    'exportConfig' => [
                        \kartik\export\ExportMenu::FORMAT_TEXT => false,
                        \kartik\export\ExportMenu::FORMAT_HTML => false,
                        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
                        \kartik\export\ExportMenu::FORMAT_PDF =>  false,
                        \kartik\export\ExportMenu::FORMAT_CSV =>  false
                    ],
                ]);

                echo GridView::widget([
                    'dataProvider' => $dataProviderFeedback,
                    'filterModel' => $searchFeedbackModel,
                    'columns' => $columns,
                ]); ?>
            </div>
        </div>
    </div>

    <?php \yii\bootstrap\Modal::begin([

        //Этот текст заменяется в javascripte
        'header' => '<h3>Укажите причину отказа</h3>',
        'options' => ['id' => 'myModal'],
        'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
    ]);
    //Этот текст заменяется в javascripte
    echo \Yii::t('yii', 'Text...');

    \yii\bootstrap\Modal::end(); ?>

    <?php \yii\widgets\Pjax::end() ?>
</div>
