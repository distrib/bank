<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Предложения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">
    <p>
        <?= Html::a('Создать предложение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?
    $columns = [
        [
            'class' => 'yii\grid\ActionColumn',
            'template'=>'{update} {delete}',
            'contentOptions' =>function ($model, $key, $index, $column){
                return ['class' => 'freeze'];
            },
        ],
        ['class' => 'yii\grid\SerialColumn'],
        // 'id',
        'title',
        // 'description:ntext',
        ['attribute' => 'active',
            'format' => 'raw',
            'filter' => [
                0 => 'Нет',
                1 => 'Да',
            ],
            'value' => function ($model) {
                if ($model->active == 1)
                    return "<span style='color:green'>Да</span>";
                elseif ($model->active == 0)
                    return "<span style='color:red'>Нет</span>";
            },
        ],
        ['attribute' => 'published',
            'format' => 'raw',
            'filter' => [
                0 => 'Нет',
                1 => 'Да',
            ],
            'value' => function ($model) {
                if ($model->published == 1)
                    return "<span style='color:green'>Да</span>";
                elseif ($model->published == 0)
                    return "<span style='color:red'>Нет</span>";
            },
        ],
        ['attribute' => 'need_reg',
            'format' => 'raw',
            'filter' => [
                0 => 'Нет',
                1 => 'Да',
            ],
            'value' => function ($model) {
                if ($model->need_reg == 1)
                    return "<span style='color:green'>Да</span>";
                elseif ($model->need_reg == 0)
                    return "<span style='color:red'>Нет</span>";
            },
        ],
        [
            'attribute'=>'created_at',
            'format'=>'date',
            'filter' => \yii\jui\DatePicker::widget([
                'options' => ['class' => 'form-control'],
                'model' => $searchModel,
                'attribute' => 'created_at',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
            ]),
        ],
        [
            'attribute'=>'updated_at',
            'format'=>'date',
            'filter' => \yii\jui\DatePicker::widget([
                'options' => ['class' => 'form-control'],
                'model' => $searchModel,
                'attribute' => 'updated_at',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy HH:mm:ss',
            ]),
        ],
    ];

    echo \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'exportConfig' => [
            \kartik\export\ExportMenu::FORMAT_TEXT => false,
            \kartik\export\ExportMenu::FORMAT_HTML => false,
            \kartik\export\ExportMenu::FORMAT_EXCEL => false,
            \kartik\export\ExportMenu::FORMAT_PDF =>  false,
            \kartik\export\ExportMenu::FORMAT_CSV =>  false
        ],
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => $columns,
    ]);?>

</div>
