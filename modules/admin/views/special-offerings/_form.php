<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialOfferings */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Основное</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Краткое описание</a></li>
            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Подробное описание</a></li>

        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <?= $form->field($model, 'title')->textInput(['maxlength' => false]) ?>

                <?= $form->field($model, 'slug')->textInput(['readonly'=> true]) ?>

                <?= $form->field($model, 'published')->checkbox() ?>

                <?= $form->field($model, 'active')->checkbox() ?>

                <?= $form->field($model, 'need_reg')->checkbox() ?>

            </div>

            <div class="tab-pane" id="tab_2">

                <?= $form->field($model, 'picture_preview')->widget(
                    \trntv\filekit\widget\Upload::className(),
                    [
                        'url' => ['/admin/file-storage/upload'],
                        'maxFileSize' => 5 * 1024 * 1024, // 5 MiB
                    ]);
                ?>

                <?= $form->field($model, 'short_description')->widget(\xvs32x\tinymce\Tinymce::className()) ?>

            </div>

            <div class="tab-pane" id="tab_3">

                <?= $form->field($model, 'picture')->widget(
                    \trntv\filekit\widget\Upload::className(),
                    [
                        'url' => ['/admin/file-storage/upload'],
                        'maxFileSize' => 5 * 1024 * 1024, // 5 MiB
                    ]);
                ?>

                <?= $form->field($model, 'slider')->widget(
                    \trntv\filekit\widget\Upload::className(),
                    [
                        'url' => ['/admin/file-storage/upload'],
                        'maxFileSize' => 5*1024*1024, // 5 MiB
                        'maxNumberOfFiles' => 5,
                        'multiple' => true,
                    ]);
                ?>

                <?= $form->field($model, 'description')->widget(\xvs32x\tinymce\Tinymce::className()) ?>

            </div>


        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
