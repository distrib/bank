<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $roles yii\rbac\Role[] */

$this->title = 'Редактировать: '.$model->email;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label'=>'Редактировать'];
?>
<div class="user-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'roles' => $roles,
        'profile' => $profile,
        'carsProvider' => $carsProvider,
        'importUserMismatch' => $importUserMismatch,
    ])?>

</div>
