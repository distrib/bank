<?php

use app\components\grid\EnumColumn;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $profile app\models\UserProfile */
/* @var $model app\modules\admin\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="nav-tabs-custom">
            <?
            if( Yii::$app->request->get('tab_2')== 'active'){
                $tab1= '';
                $tab2= 'active';
            }else{
                $tab1= 'active';
                $tab2= '';
            }?>
            <ul class="nav nav-tabs">
                <li class="<?= $tab1?>"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Личные данные</a></li>
                <li class="<?= $tab2?>"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Автомобили</a></li>
                <li ><a href="#tab_3" data-toggle="tab" aria-expanded="false">Сервисная информация</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane <?= $tab1?>" id="tab_1">
                    <div class="row">
                        <div class="col-lg-4">
                            <?php echo $form->field($profile, 'bmw_id') ?>
                            <?php echo $form->field($profile, 'firstname') ?>
                            <?php echo $form->field($profile, 'lastname') ?>
                            <?php echo $form->field($profile, 'middlename') ?>
                            <?php echo $form->field($profile, 'gender')->radioList([
                                $profile::GENDER_MALE => "Мужской",
                                $profile::GENDER_FEMALE => "Женский"
                            ]);?>
                            <?php echo $form->field($profile, 'town')->label('Город пользователя') ?>

                            <?
                            echo $form->field($profile, 'birthday')->begin();
                                echo Html::activeLabel($profile,'birthday');
                                echo DatePicker::widget([
                                    'name'  => 'UserProfile[birthday]',
                                    'value'  => $profile->birthday,
                                    'language' => 'ru',
                                    'id' => 'date-picker-profile',
                                    'dateFormat' => 'dd.MM.yyyy',
                                    'attribute' => 'birthday',
                                    'model' => $profile,
                                    'options' => ['autocomplete' => 'off', 'class' => 'form-control'],
                                ]);
                                echo Html::error($profile,'birthday', ['class' => 'help-block']);
                            echo $form->field($profile, 'birthday')->end();
                            ?>

                            <?php echo $form->field($model, 'email') ?>

                            <?if( Yii::$app->user->identity->id == 1){
                                echo $form->field($model, 'password')->passwordInput();
                                echo $form->field($model, 'status')->dropDownList(User::statuses());
                                echo $form->field($model, 'roles')->checkboxList($roles)->label('Роль');
                            }?>

                            <?php echo $form->field($profile, 'phone') ?>
                            <?php echo $form->field($profile, 'card_number') ?>
                            <?php echo $form->field($profile, 'connection')->radioList([
                                $profile::CONNECTION_PHONE => "Телефон",
                                $profile::CONNECTION_EMAIL => "E-mail"
                            ]);?>

                            <?echo $form->field($model, 'manual_club_date')->begin();
                                echo Html::activeLabel($model,'manual_club_date');
                                echo DatePicker::widget([
                                    'name'  => 'manual_club_date',
                                    'value'  => $model->manual_club_date,
                                    'language' => 'ru',
                                    'id' => 'date-picker-profile',
                                    'dateFormat' => 'dd.MM.yyyy',
                                    'attribute' => 'manual_club_date',
                                    'model' => $model,
                                    'options' => ['autocomplete' => 'off', 'class' => 'form-control'],
                                ]);
                                echo Html::error($model,'manual_club_date', ['class' => 'help-block']);
                            echo $form->field($model, 'manual_club_date')->end();?>

                            <?/*= $form->field($profile, 'picture')->widget(
                                \trntv\filekit\widget\Upload::className(),
                                [
                                    'url' => ['/admin/file-storage/upload'],
                                    'maxFileSize' => 5 * 1024 * 1024, // 5 MiB
                                ]);
                            */?>


                        </div>
                        <div class="col-lg-8">
                            <?php
                            if ((!$model->getModel()->isNewRecord) && (
                                    ($model->status == User::STATUS_INACTIVE) ||
                                    // ($model->status==User::STATUS_INACTIVE_CRM_MISMATCH) ||
                                    ($model->status == User::STATUS_REPEATED_REGISTRATION) ||
                                    ($model->status == User::STATUS_ACTIVATION_PROCESS)

                                ) && ($model->bmw_status == User::STATUS_CRM_PASSED)
                            )

                                echo Html::a(
                                $model->status == User::STATUS_ACTIVATION_PROCESS ? 'Повторная отправка приглашения' : 'Отправка приглашения',
                                ['send-invitation', 'id' => $model->id],
                                ['class' => 'btn btn-success']);
                            elseif ((!$model->getModel()->isNewRecord) && ($model->status == User::STATUS_ACTIVE || $model->status == User::STATUS_RESTORED)  )

                                echo Html::a(
                                'Перенести в архив',
                                ['archive', 'id' => $model->id],
                                ['class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Вы уверены?',
                                    ]]);
                            elseif ((!$model->getModel()->isNewRecord) && ($model->status == User::STATUS_DELETED))

                                echo Html::a(
                                    'Активировать',
                                    ['renew', 'id' => $model->id],
                                    ['class' => 'btn btn-success',
                                        'data' => [
                                            'confirm' => 'Вы уверены?',
                                        ]]);
                            elseif ((!$model->getModel()->isNewRecord) && (
                                ($model->status == User::STATUS_INACTIVE)

                            ) && ($model->bmw_status != User::STATUS_CRM_PASSED))

                                echo Html::a(
                                    'Удалить',
                                    ['delete', 'id' => $model->id],
                                    ['class' => 'btn btn-danger',
                                        'data' => [
                                            'confirm' => 'Вы уверены?',
                                        ]]);
                            ?>



                            <h2>Список несоотвествий</h2><br>
                            <?
                            $controlList = ["firstname","lastname","middlename","email","phone","status","birthday"];
                            if( !empty($importUserMismatch) && $model->bmw_status==User::STATUS_CRM_MISMATCH ){
                                foreach( $importUserMismatch as $key => $iUM){
                                    if( in_array($key,$controlList) && $iUM ){
                                        if($key == 'status') $iumTitle = 'Статус'; else $iumTitle = $profile->attributeLabels()[$key];
                                        ?>
                                        <div class="ium-row">
                                            <span style="text-transform: uppercase; color: #3c8dbc; font-weight: 600; padding-right: 30px;width:170px;display: inline-block;">
                                                <?= $iumTitle;?>
                                            </span>
                                            <span><?= $iUM;?></span>
                                        </div>
                                    <?}
                                }
                            }else{?>
                                <div class="ium-row">
                                    Список пуст
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane <?= $tab2?>" id="tab_2">
                    <?php echo GridView::widget([
                        'dataProvider' => $carsProvider,
                        'options' => [
                            'class' => 'grid-view table-responsive',
                            'id' => 'adm-cars'
                        ],
                        'columns' => [
                            'vin',
                            'model',
                            [
                                'attribute' => 'purchase_date',
                                'format' => ['date', 'php:d.m.Y']
                            ],
                            'dealer',
                            [
                                'label' => 'Наименование дилера',
                                'filter' => false,
                                'content' => function ($model) {
                                    return $model->getDialerNameAdmin();
                                },
                            ],
                            'dealer_town',
                            ['class' => 'yii\grid\ActionColumn',
                                'controller' => 'cars',
                                'template' => '{update}',
                            ],
                        ],
                    ]); ?>
                </div>

                <div class="tab-pane" id="tab_3">

                    <h3>Общая информация</h3>
                    <b>Статус регистрации: </b><?=User::statuses()[$model->getModel()->status]?><br>
                    <b>Способ регистрации: </b><?= $model->getModel()->manual_register ? 'Ручная':'Через CRM'?><br>
                    <? if( !$model->getModel()->manual_register ):?>
                        <b>Дата регистрации: </b><?=date('d.m.Y',$model->getModel()->created_at)?><br>
                    <?endif;?>

                    <?php
                    if ($model->getModel()->getValidCars()) {
                        $carList = $model->getModel()->getValidCars();;
                        if (count($carList) > 1) {
                            $myFirstCar = array_pop($carList);
                        } else
                            $myFirstCar = array_shift($carList);
                        $date = DateTime::createFromFormat('Y-m-j', $myFirstCar->purchase_date);
                        if ($date instanceof \DateTime) {
                            $days = floor((time() - $date->format('U')) / (60 * 60 * 24));
                        } else
                            $days = 0;
                        ?>

                        <?php
                        if (strlen($model->getModel()->manual_club_date)) {
                            $endDate = DateTime::createFromFormat('Y-m-j', $model->getModel()->manual_club_date);
                        } else {

                            $endDate = DateTime::createFromFormat('Y-m-j', array_shift($model->getModel()->getValidCars())->purchase_date);
                        }
                        $eDate = $endDate->format('d.m.Y');
                        $endDate->modify('+3 years');
                        $sDate = $endDate->format('d.m.Y');

                        $crm_verification = $model->getModel()->crm_verification;
                        ?>

                        <b>Дата подтверждения данными из CRM: </b><?= ($crm_verification)? $crm_verification:'-';?><br>
                        <b>Количество дней в программе: </b><?= $days ?><br>
                        <b>Дата начала членства: </b><?= $eDate;?><br>
                        <b>Дата окончания членства: </b><?= $sDate?><br>

                        <h3>Интересы</h3>
                        <?php echo GridView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query' => $model->getModel()->getInterests(),
                                'sort' => false,
                            ]),
                            'columns' => [
                                'title',
//                            [
//                                'attribute' => 'image',
//
//                                'filter' => false,
//                                'content' => function ($model) {
//                                    return Html::img('/uploads/' . $model->image, ['width' => '200px']);
//                                },
//                            ],
                            ],
                        ]); ?>

                        <h3>Заявки на мероприятия</h3>
                        <?php echo GridView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query' => $model->getModel()->getEventApplications(),
                                'sort' => false,
                            ]),
                            'columns' => [
                                'event.title',
                                ['attribute' => 'application_status',
                                    'format' => 'raw',
                                    'filter' => [

                                        0 => 'На рассмотрении',
                                        1 => 'Одобрена',
                                        2 => 'Отклонена',

                                    ],
                                    'value' => function ($model) {


                                        if ($model->application_status == 1)
                                            return "<span style='color:green'>Одобрена</span>";
                                        elseif ($model->application_status == 0)
                                            return "<span style='color:gray'>На рассмотрении</span>";
                                        elseif ($model->application_status == 2)
                                            return "<span style='color:red'>Отклонена</span>";

                                    },
                                ],

//                            [
//                                'attribute' => 'image',
//
//                                'filter' => false,
//                                'content' => function ($model) {
//                                    return Html::img('/uploads/' . $model->image, ['width' => '200px']);
//                                },
//                            ],
                            ],
                        ]); ?>

                        <h3>Посещеные мероприятия</h3>
                        <?php echo GridView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query' => $model->getModel()->getEvents()->andFilterWhere(['<', 'events.date_end', time()]),
                                'sort' => false,
                            ]),
                            'columns' => [
                                'title',

//                            [
//                                'attribute' => 'image',
//
//                                'filter' => false,
//                                'content' => function ($model) {
//                                    return Html::img('/uploads/' . $model->image, ['width' => '200px']);
//                                },
//                            ],
                            ],
                        ]); ?>

                        <h3>спец. предложения</h3>
                        <?php echo GridView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query' => $model->getModel()->getSpecialOfferings(),
                                'sort' => false,
                            ]),
                            'columns' => [
                                'title',

//                            [
//                                'attribute' => 'image',
//
//                                'filter' => false,
//                                'content' => function ($model) {
//                                    return Html::img('/uploads/' . $model->image, ['width' => '200px']);
//                                },
//                            ],
                            ],
                        ]);

                    }?>

                    <h3>Лог смены E-mail</h3>
                    <?php echo GridView::widget([
                        'dataProvider' => new ActiveDataProvider([
                            'query' => $model->getModel()->getEmailLogs(),
                            'sort' => false,
                        ]),
                        'columns' => [
                            'was',
                            'become',
                            ['attribute' => 'operator',
                                'value' => 'correctorModel.email'
                            ],
                            'created_at',
//                            [
//                                'attribute' => 'image',
//
//                                'filter' => false,
//                                'content' => function ($model) {
//                                    return Html::img('/uploads/' . $model->image, ['width' => '200px']);
//                                },
//                            ],
                        ],
                    ]);

                    ?>
                </div>

            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton("Сохранить", ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
