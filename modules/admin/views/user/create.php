<?php
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\UserForm */
/* @var $roles yii\rbac\Role[] */
$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <?php echo $this->render('_form', [
        'model' => $model,
        'roles' => $roles,
        'profile' => $profile
    ]) ?>
</div>
