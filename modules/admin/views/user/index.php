<?php

use app\components\grid\EnumColumn;
use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<!---->
<!--    <p>-->
<!--        --><?php //echo Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?php
        $columns = [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['class' => 'freeze'];
                },
            ],
            //'id',
            [
                'attribute' => 'bmw_id',
                'value' => 'profile.bmw_id'
            ],
            [
                'class' => EnumColumn::className(),
                'attribute' => 'manual_register',
                'enum' => User::RegisterType(),
                'label' => 'Способ регистрации',
            ],
            [
                'label' => 'Дата регистрации',
                'attribute' => 'created_at',
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'changeMonth' => true,
                        'yearRange' => '1980:2099',
                        'changeYear' => true,
                    ],
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
                'format' => 'raw',
                'value' => function ($model) {
                    if( $model->manual_register ){
                        return "<span style='color:red'>-</span>";
                    }else{
                        return date('d.m.Y',$model->created_at);
                    }
                },
            ],
            [
                'attribute'=>'created_at',
                'label' => 'Время обработки обращения',
                'filter' => false,
                'format' => 'raw',
                'value' => function ($model) {
                    if( $model->manual_register ){
                        return "<span style='color:red'>-</span>";
                    }else{
                        return date('H:i:s',$model->created_at);
                    }
                },
            ],
            [
                'attribute' => 'crm_verification',
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'changeMonth' => true,
                        'yearRange' => '1980:2099',
                        'changeYear' => true,
                    ],
                    'model' => $searchModel,
                    'attribute' => 'crm_verification',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
                'format'=>['date', 'php:d.m.Y'],
                'label'=>'Дата загрузки из CRM',
            ],
            [
                'class' => EnumColumn::className(),
                'attribute' => 'status',
                'enum' => User::statusesList(),
                'filter' => User::statusesList(),
                'label' => 'Статус регистрации',
            ],
            [
                'class' => EnumColumn::className(),
                'attribute' => 'bmw_status',
                'enum' => User::statusesListBmw(),
                'filter' => User::statusesListBmw(),
                'label' => 'Статус сверки с CRM',
            ],
            [
                'class' => EnumColumn::className(),
                'filter' => [
                    User::STATUS_ACTIVE => 'Активный',
                    User::STATUS_RESTORED => 'Повторно зарегистрированный',
                    User::STATUS_DELETED => 'Удаленный',
                ],
                'label' => 'Статус участника',
                'attribute'=>'statusSecond',
                'format' => 'raw',
                'value' => function ($model) {
                    if( $model->bmw_status == User::STATUS_CRM_PASSED){
                        if( $model->status == User::STATUS_ACTIVE){
                            return 'Активный';
                        }elseif($model->status == User::STATUS_RESTORED){
                            return 'Повторно зарегистрированный';
                        }elseif($model->status == User::STATUS_DELETED){
                            return 'Удаленный';
                        }
                    }else{
                        return "<span style='color:red'>-</span>";
                    }
                },
            ],
            [
                'attribute' => 'lastname',
                'value' => 'profile.lastname'
            ],
            [
                'attribute' => 'firstname',
                'value' => 'profile.firstname'
            ],
            [
                'attribute' => 'middlename',
                'value' => 'profile.middlename'
            ],
            [
                'attribute' => 'gender',
                'class' => EnumColumn::className(),
                'enum' => [1=>'Муж', 2=>'жен'],
                'filter' =>[1=>'Муж', 2=>'жен'],
                'value' => 'profile.gender'
            ],
            [
                'label' => 'Дата рождения',
                'attribute' => 'birthday',
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'changeMonth' => true,
                        'yearRange' => '1940:2099',
                        'changeYear' => true,
                    ],
                    'model' => $searchModel,
                    'attribute' => 'birthday',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
                'value' => 'profile.birthday',
                'format'=>['date', 'php:d.m.Y'],
            ],
            //   'username',
            'email:email',
            [
                'attribute' => 'phone',
                'value' => 'profile.phone'
            ],
            [
                'label' => 'VIN',
                'attribute' => 'car_vin',
                'value' => 'lastCar.vin'
            ],
            [
                'label' => 'Модель',
                'attribute' => 'car_model',
                'value' => 'lastCar.model',
            ],
            [
                'attribute' => 'dealer',
                'label' => 'Дилер',
                'value' => 'lastCar.dealerModel.name',
            ],
            [
                'label' => 'Город дилера',
                'attribute' => 'dealer_town',
                'value' => 'lastCar.dealer_town',
            ],
            [
                'attribute' => 'town',
                'label' => 'Город участника',
                'value' => 'profile.town',
            ],
            [
                'label' => 'Дата покупки автомобиля',
                'attribute' => 'last_car_purchase_date',
                'value' => 'lastCar.purchase_date',
                'format'=>['date', 'php:d.m.Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'changeMonth' => true,
                        'yearRange' => '1940:2099',
                        'changeYear' => true,
                    ],
                    'model' => $searchModel,
                    'attribute' => 'last_car_purchase_date',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
            ],
            [
                'label' => 'Дата окончания срока членства',
                'content'=>function($model){
                    $date = DateTime::createFromFormat('Y-m-d', $model->getMembershipEndDate());
                    if ($date instanceof \DateTime) {
                        return $date->format('d.m.Y');
                    }

                },
            ],
            [
                'attribute' => 'membership_days',
                'label' => 'Дней в программе',
                'content'=>function($model){
                    return $model-> getProgramDays();
                },
            ],
            [
                'label' => 'Дата покупки первого автомобиля',
                'attribute' => 'first_car_purchase_date',
                'value' => 'firstCar.purchase_date',
                'format'=>['date', 'php:d.m.Y'],
                'filter' => \yii\jui\DatePicker::widget([
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'changeMonth' => true,
                        'yearRange' => '1940:2099',
                        'changeYear' => true,
                    ],
                    'model' => $searchModel,
                    'attribute' => 'first_car_purchase_date',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
            ],
            [
                'attribute' => 'count_cars',
                'label' => 'Количество Автомобилей',
                'content'=>function($model){
                    return $model->getCars()->count();
                },
            ],
            [
                'attribute' => 'connection',
                'label' => 'Способ связи',
                'format' => 'raw',
                'filter' => [
                    1 => 'E-mail',
                    2 => 'Телефон',
                ],
                'value' => function ($model) {

                    if ($model->profile->connection == 1)
                        return "<span style='color:green'>E-mail</span>";
                    elseif ($model->profile->connection == 2)
                        return "<span style='color:blue'>Телефон</span>";

                },
            ],
            [
                'attribute' => 'card_number',
                'label' => 'Карта лояльности',
                'value' => 'profile.card_number',
            ],
            //  'created_at:datetime',
            //  'updated_at',
        ];

    echo \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'exportConfig' => [
            \kartik\export\ExportMenu::FORMAT_TEXT => false,
            \kartik\export\ExportMenu::FORMAT_HTML => false,
            \kartik\export\ExportMenu::FORMAT_EXCEL => false,
            \kartik\export\ExportMenu::FORMAT_PDF =>  false,
            \kartik\export\ExportMenu::FORMAT_CSV =>  false
        ],

    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => $columns,
    ]); ?>
</div>
