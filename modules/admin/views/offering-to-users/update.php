<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OfferingToUsers */

$this->title = 'Редактировать обращение "Заявка на спец. предложение":';
$this->params['breadcrumbs'][] = ['label' => 'Обращение', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<div class="offering-to-users-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
