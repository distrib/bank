<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на спец. предложения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offering-to-users-index">
<?php
$columns = [
    [
        'class' => 'yii\grid\ActionColumn',
        'template'=>'{update}',
        'contentOptions' =>function ($model, $key, $index, $column){
            return ['class' => 'freeze'];
        },
    ],
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute'=>'id',
        'format'=>'text',
        'label'=>'Номер заявки',
    ],
    [
        'attribute'=>'created_at',
        'label'=>'Дата заявки',
        'format'=>['date', 'php:d.m.Y'],
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'model' => $searchModel,
            'attribute' => 'created_at',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
        ]),
    ],
    [
        'attribute'=>'created_at',
        'label' => 'Время заявки',
        'format'=>['time', 'php:H:i:s'],
        'filter' => false,
    ],
    [
        'attribute' => 'offering_name',
        'value'=>'offering.title',
        'label'=>'Наименование спец. предложения',
    ],
    [
        'attribute' => 'application_status',
        'label' => 'Статус обработки заявки',
        'format' => 'raw',
        'filter' => [

            0 => 'Новая',
            1 => 'Одобрена',
            2 => 'Отклонена',
            3 => 'В процессе',

        ],
        'value' => function ($model) {


            if ($model->application_status == 1)
                return "<span style='color:green'>Одобрена</span>";
            elseif ($model->application_status == 0)
                return "<span style='color:gray'>Новая</span>";
            elseif ($model->application_status == 2)
                return "<span style='color:red'>Отклонена</span>";
            elseif ($model->application_status == 3)
                return "<span style='color:#97a0b3'>В процессе</span>";

        },
    ],
    'status_comment',
    [
        'attribute'=>'status_change_time',
        'label'=>'Дата обработки заявки',
        'format'=>['date', 'php:d.m.Y'],
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'model' => $searchModel,
            'attribute' => 'status_change_time',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
        ]),
    ],
    [
        'attribute' => 'status_change_time',
        'label' =>'Время обработки заявки',
        'filter' => false,
        'value' => function ($model) {
            if ($model->status_change_time)
                return date('H:i:s', $model->status_change_time);
        }
    ],
    [
        'attribute' => 'bmw_id',
        'value' => 'profile.bmw_id'
    ],
    [
        'attribute'=>'firstname',
        'label' => 'ФИО',
        'format'=> 'raw',
        'filter'=> false,
        'value'=> function($model){
            return $model->profile->fullName;
        }
    ],
    [
        'attribute' => 'town',
        'label' => 'Город участника',
        'value' => 'profile.town'
    ],
    [
        'attribute' => 'email',
        'value' => 'user.email'
    ],
    [
        'attribute' => 'phone',
        'value' => 'profile.phone'
    ],
    [
        'attribute' => 'connection',
        'label' => 'Способ связи',
        'format' => 'raw',
        'filter' => [
            1 => 'E-mail',
            2 => 'Телефон',
        ],
        'value' => function ($model) {
            if ($model->profile->connection == 1)
                return "<span style='color:green'>E-mail</span>";
            elseif ($model->profile->connection == 2)
                return "<span style='color:blue'>Телефон</span>";
        },
    ],
];

echo \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'exportConfig' => [
        \kartik\export\ExportMenu::FORMAT_TEXT => false,
        \kartik\export\ExportMenu::FORMAT_HTML => false,
        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
        \kartik\export\ExportMenu::FORMAT_PDF =>  false,
        \kartik\export\ExportMenu::FORMAT_CSV =>  false
    ],
]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'options' => [
        'class' => 'grid-view table-responsive'
    ],
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-admin-offering-to-users',
    ],
    'rowOptions'=> function ($model, $key, $index, $grid){
        if($model->application_status == 0){ return ['class'=>'new']; }
    },
    'columns' => $columns,
]);
?>


<?php //echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'options' => [
//        'class' => 'grid-view table-responsive'
//    ],
//    'columns' => [
//
//]); ?>