<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на события';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-to-users-index">
    <?
    $columns = [
        [
            'class' => 'yii\grid\ActionColumn',
            'template'=>'{update}',
            'contentOptions' =>function ($model, $key, $index, $column){
                return ['class' => 'freeze'];
            },
        ],
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'id',
            'format'=>'text',
            'label'=>'Номер заявки',
        ],
        [
            'attribute'=>'created_at',
            'label'=>'Дата заявки',
            'format'=>['date', 'php:d.m.Y'],
            'filter' => \yii\jui\DatePicker::widget([
                'options' => ['class' => 'form-control'],
                'model' => $searchModel,
                'attribute' => 'created_at',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
            ]),
        ],
        [
            'attribute'=>'created_at',
            'label' => 'Время заявки',
            'format'=>['time', 'php:H:i:s'],
            'filter' => false,
        ],
        [
            'attribute'=>'eventName',
            'label' => 'Наименование события',
            'format' => 'raw',
            'value'=> 'event.title',
        ],
        [
            'attribute'=>'application_status',
            'label' => 'Статус обработки заявки',
            'filter' => [
                0 => 'Новая',
                1 => 'Одобрена',
                2 => 'Отклонена',
                3 => 'В процессе',
            ],
            'format' => 'raw',
            'value'=>function($model){

                if ($model->application_status == 1)
                    return "<span style='color:green'>Одобрена</span>";
                elseif ($model->application_status == 0)
                    return "<span style='color:gray'>Новая</span>";
                elseif ($model->application_status == 2)
                    return "<span style='color:red'>Отклонена</span>";
                elseif ($model->application_status == 3)
                    return "<span style='color:#97a0b3'>В процессе</span>";
            },
        ],
        [
            'attribute'=>'status_comment',
            'label' => 'Комментарий',
            'format' => 'raw',
        ],
        [
            'attribute'=>'status_change_time',
            'label'=>'Дата обработки заявки',
            'format'=>['date', 'php:d.m.Y'],
//                'value'=>function($model){
//                    return $model->status_change_time;
//                },
            'filter' => \yii\jui\DatePicker::widget([
                'options' => ['class' => 'form-control'],
                'model' => $searchModel,
                'attribute' => 'status_change_time',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
            ]),
        ],
        [
            'attribute'=>'status_change_time',
            'label' => 'Время обработки заявки',
            'format'=>['time', 'php:H:i:s'],
            'filter' => false,
        ],
        [
            'attribute'=>'eventGlobal',
            'label' => 'Глобальное/локальное событие',
            'format' => 'raw',
            'filter' => [
                0 => 'Локальное',
                1 => 'Глобальное',
            ],
            'value'=>function($model){
                if ($model->event->type == 0)
                    return "<span style='color:green'>Локальное</span>";
                else
                    return "<span style='color:red'>Глобальное</span>";
            },
        ],
        [
            'attribute'=>'event',
            'label' => 'Количество мест',
            'format'=> 'raw',
            'filter' => false,
            'value'=>function($model){
                if($model->event->limit){
                    return $model->event->limit - $model->event->countApprovedOrderByEvent;
                }
            },
        ],
        [
            'attribute'=>'eventPay',
            'label' => 'Платное/бесплатное',
            'format'=> 'raw',
            'filter' => [
                0 => 'Бесплатное',
                1 => 'Платное',
            ],
            'value'=>function($model){
                if ($model->event->pay == 0)
                    return "<span style='color:green'>Бесплатное</span>";
                else
                    return "<span style='color:red'>Платное</span>";
            },
        ],
        [
            'attribute'=>'eventPrice',
            'label' => 'Стоимость',
            'format'=> 'raw',
            'value'=> 'event.pay_price',
        ],
        [
            'attribute'=>'payStatus',
            'label' => 'Статус оплаты',
            'format'=> 'raw',
            'filter' => [
                0 => 'Не оплачено',
                1 => 'Оплачено',
                '-' => '-',
            ],
            'value'=> function($model){
                if ($model->event->pay == 0) {
                    return "<span style='color:grey'>-</span>";
                }else{
                    if ($model->pay_status == 1)
                        return "<span style='color:green'>Оплачено</span>";
                    else
                        return "<span style='color:red'>Не оплачено</span>";
                }
            },
        ],
        [
            'attribute'=>'user_id',
            'label' => 'ID участника',
            'format'=> 'raw',
        ],
        [
            'attribute'=>'user_profile',
            'label' => 'ФИО',
            'format'=> 'raw',
            'value'=> function($model){
                return $model->profile->fullName;
            }
        ],
        [
            'attribute'=>'userTown',
            'label' => 'Город пользователя',
            'format'=> 'raw',
            'value'=> 'profile.town',
        ],
        [
            'attribute'=>'userEmail',
            'label' => 'Почта',
            'format'=> 'raw',
            'value'=> 'user.email'
        ],
        [
            'attribute'=> 'userPhone',
            'label' => 'Телефон',
            'format'=> 'raw',
            'value'=> 'profile.phone',
        ],
        [
            'attribute'=>'userConnection',
            'label' => 'Способ связи',
            'format'=> 'raw',
            'filter' => [
                1 => 'Телефон',
                2 => 'Почта',
            ],
            'value'=> function($model){
                if ($model->profile->connection == 1)
                    return "<span style='color:green'>Телефон</span>";
                else
                    return "<span style='color:red'>Почта</span>";
            }
        ],
    ];

    echo \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'exportConfig' => [
            \kartik\export\ExportMenu::FORMAT_TEXT => false,
            \kartik\export\ExportMenu::FORMAT_HTML => false,
            \kartik\export\ExportMenu::FORMAT_EXCEL => false,
            \kartik\export\ExportMenu::FORMAT_PDF =>  false,
            \kartik\export\ExportMenu::FORMAT_CSV =>  false
        ],

    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-admin-event-to-users',
        ],
        'rowOptions'=> function ($model, $key, $index, $grid){
            if($model->application_status == 0){ return ['class'=>'new']; }
        },
        'columns' => $columns,
    ]); ?>


</div>
