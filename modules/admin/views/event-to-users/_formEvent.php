<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="event-to-users-form">
    <?
    $attributes = [
        [
            'attribute'=>'id',
            'label' => 'Номер заявки'
        ],
        [
            'attribute'=>'title',
            'label' => 'Наименование события',
            'format' => 'raw',
        ],
        [
            'attribute'=>'type',
            'label' => 'Глобальное/локальное событие',
            'format' => 'raw',
            'value'=>function($model){
                if ($model->type == 0)
                    return "<span style='color:green'>Локальное</span>";
                else
                    return "<span style='color:red'>Глобальное</span>";
            },
        ],
        [
            'attribute'=>'limit',
            'label' => 'Количество мест',
            'format'=> 'raw',
            'value'=>function($model){
                if($model->limit){
                    return $model->limit - $model->countApprovedOrderByEvent;
                }
            },
        ],
        [
            'attribute'=>'pay',
            'label' => 'Платное/бесплатное',
            'format'=> 'raw',
            'value'=>function($model){
                if ($model->pay == 0)
                    return "<span style='color:green'>Бесплатное</span>";
                else
                    return "<span style='color:red'>Платное</span>";
            },
        ],
    ];

    if($model->pay !== 0){
        array_push($attributes,[
            'attribute'=>'pay_price',
            'label' => 'Стоимость посещения',
            'format'=> 'raw',
        ]);
    }

    echo DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
