<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventToUsers */

$this->title = 'Редактировать заявку на событие:';
$this->params['breadcrumbs'][] = ['label' => 'Заявки на события', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<div class="bug-message-update">
    <?
    if( Yii::$app->request->get('tab_2')== 'active'){
        $tab1= '';
        $tab2= 'active';
    }else{
        $tab1= 'active';
        $tab2= '';
    }?>
    <ul class="nav nav-tabs">
        <li class="<?= $tab1?>"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Описание заявки</a></li>
        <li class="<?= $tab2?>"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Описание события</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane <?= $tab1?>" id="tab_1">
            <?= $this->render('_form', [
                'model' => $model,
                'pay' => $event->pay,
            ]) ?>
        </div>

        <div class="tab-pane <?= $tab2?>" id="tab_2">
            <?= $this->render('_formEvent', [
                'model' => $event,
            ]) ?>
        </div>
    </div>

</div>
