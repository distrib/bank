<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="event-to-users-form">
    <?
    $attributes = [
        [
            'attribute'=>'id',
            'label' => 'Номер заявки'
        ],
        [
            'attribute'=>'created_at',
            'label'=>'Дата заявки',
            'format'=>['date', 'php:d.m.Y'],
        ],
        [
            'attribute'=>'created_at',
            'label' => 'Время заявки ',
            'format'=>['time', 'php:H:i:s'],
        ],
        [
            'attribute'=>'user_id',
            'label' => 'ID участника'
        ],
        [
            'label'=>'ФИО',
            'attribute'=>'user_id',
            'format' => 'raw',
            'value'=>function($data){
                return Html::a(
                    $data->user->profile->fullName, // text
                    '/admin/user/update?id='.$data->user->id,
                    [
                        'title' => $data->user->email,
                        'target' => '_blank'
                    ]
                );
            },
        ],
        [
            'label'=>'Город пользователя',
            'attribute'=>'user_id',
            'format' => 'raw',
            'value'=>function($data){
                return $data->profile->town;
            },
        ],
        [
            'attribute'=>'user_id',
            'format'=>'text',
            'label'=>'Почта',
            'value'=>function($data){
                return $data->user->email;
            },
        ],
        [
            'label'=>'Телефон',
            'attribute'=>'user_id',
            'format' => 'raw',
            'value'=>function($data){
                return $data->profile->phone;
            },
        ],
        [
            'label'=>'Способ связи ',
            'attribute'=>'user_id',
            'format' => 'raw',
            'value'=>function($data){
                if( $data->profile->connection == 1 ){
                    return 'по телефону';
                }else{
                    return 'по почте';
                }
            },
        ],
    ];

    if($pay !== 0){
        array_push($attributes,[
            'label'=>'Статус оплаты',
            'attribute'=>'pay_status',
            'format' => 'raw',
            'value'=>function($data){
                if( $data->pay_status == 1 ){
                    return 'Оплачено';
                }else{
                    return 'Не оплачено';
                }
            },
        ]);
    }

    echo DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'bug-message-form-editor',
    ]); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'status_comment')->textarea(); ?>
        <?= $form->field($model, 'application_status')->dropDownList([
            0 => 'Новая',
            1 => 'Одобрена',
            2 => 'Отклонена',
            3 => 'В процессе',
        ]);?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
