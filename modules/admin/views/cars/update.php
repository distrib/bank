<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $roles yii\rbac\Role[] */

$this->title = 'Update: '.$model->model.' ('.$model->vin.")";
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vin, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label'=>'Update'];
?>
<div class="user-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'viewType' => $viewType,
    ])?>

</div>
