<?php

use app\components\grid\EnumColumn;
use app\models\User;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $profile app\models\UserProfile */
/* @var $model app\modules\admin\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="cars-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'adm-cars-edit',
            'data-window-id' => 'userCarsSubmit',
        ],
    ]); ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo $form->field($model, 'purchase_date')->textInput([
                    'value' => ($model->purchase_date)? Yii::$app->formatter->asDatetime($model->purchase_date, 'php:d.m.Y'):'',
                ]);?>
                <?php echo $form->field($model, 'vin') ?>
                <?php echo $form->field($model, 'model') ?>
                <?php echo $form->field($model, 'dealer') ?>
                <?php echo $form->field($model, 'dealer_town') ?>
                <?php echo $form->field($model, 'main')->radioList([
                    1 => "Да",
                    0 => "Нет"
                ]);?>
            </div>
        </div>

        <div class="form-group" style="text-align:center">
            <?= Html::submitButton("Сохранить", ['class' => 'btn btn-primary','name' => 'signup-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
