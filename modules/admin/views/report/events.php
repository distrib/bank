<?php

/* @var $this yii\web\View */

use app\components\grid\EnumColumn;
use app\models\User;
use kartik\grid\GridView;

$this->title = 'Отчет по событиям';
    $this->params['breadcrumbs'][] = $this->title;
    ?>
    <div class="user-index">

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!---->
    <!--    <p>-->
    <!--        --><?php //echo Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->



<?php
$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'created_at',
        'label' =>'Дата заявки',
        'value' => function ($model) {
            if ($model->created_at)
                return date('Y-m-d', $model->created_at);

        }
    ],
    [
        'attribute' => 'created_at',
        'label' =>'Время заявки',
        'value' => function ($model) {
            if ($model->created_at)
                return date('H:i:s', $model->created_at);

        }
    ],

    [
        'attribute' => 'event_name',
        'value'=>'event.title',
    ],

    ['attribute' => 'application_status',
        'format' => 'raw',
        'filter' => [

            0 => 'Новая',
            1 => 'Одобрена',
            2 => 'Отклонена',
            3 => 'В процессе',

        ],
        'value' => function ($model) {


            if ($model->application_status == 1)
                return "<span style='color:green'>Одобрена</span>";
            elseif ($model->application_status == 0)
                return "<span style='color:gray'>Новая</span>";
            elseif ($model->application_status == 2)
                return "<span style='color:red'>Отклонена</span>";
            elseif ($model->application_status == 3)
                return "<span style='color:#97a0b3'>В процессе</span>";

        },
    ],
    'status_comment',

    [
        'attribute' => 'status_change_time',
        'label' =>'Дата обработки заявки',
        'value' => function ($model) {
            if ($model->status_change_time)
            return date('Y-m-d', $model->status_change_time);

        }
    ],
    [
        'attribute' => 'status_change_time',
        'label' =>'Время обработки заявки',
        'value' => function ($model) {
if ($model->status_change_time)
            return date('H:i:s', $model->status_change_time);

        }
    ],

    [
        'attribute' => 'type',
        'format' => 'raw',
        'filter' => [
            0 => 'Локальное',
            1 => 'Глобальное',
        ],
        'value' => function ($model) {

            if ($model->event->type == 1)
                return "<span style='color:green'>Глобальное</span>";
            elseif ($model->event->type == 0)
                return "<span style='color:blue'>Локальное</span>";

        },
    ],

    [
        'label' => 'Количество мест',
        'value' => function ($model) {

    if ($model->event->limit) {

        return ($model->event->limit - $model->event->CountApprovedOrderByEvent);

    }

        }
    ],


    [
        'attribute' => 'bmw_id',
        'value' => 'profile.bmw_id'
    ],

    [
        'attribute' => 'firstname',
        'value' => 'profile.firstname'
    ],
    [
        'attribute' => 'lastname',
        'value' => 'profile.lastname'
    ],

    [
        'attribute' => 'middlename',
        'value' => 'profile.middlename'
    ],
    [
        'attribute' => 'town',
        'label' => 'Город участника',
        'value' => 'profile.town'
    ],
    [
        'attribute' => 'email',
        'value' => 'user.email'
    ],
    [
        'attribute' => 'phone',
        'value' => 'profile.phone'
    ],

    [
        'attribute' => 'connection',
        'label' => 'Способ связи',
        'format' => 'raw',
        'filter' => [
            1 => 'E-mail',
            2 => 'Телефон',
        ],
        'value' => function ($model) {

            if ($model->profile->connection == 1)
                return "<span style='color:green'>E-mail</span>";
            elseif ($model->profile->connection == 2)
                return "<span style='color:blue'>Телефон</span>";

        },
    ],










    //  'created_at:datetime',
    //  'updated_at',


];
echo \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'exportConfig' => [
        \kartik\export\ExportMenu::FORMAT_TEXT => false,
        \kartik\export\ExportMenu::FORMAT_HTML => false,
        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
        \kartik\export\ExportMenu::FORMAT_PDF =>  false,
        \kartik\export\ExportMenu::FORMAT_CSV =>  false
    ],

]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,


    'columns' => $columns,
]);
?>


<?php //echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'options' => [
//        'class' => 'grid-view table-responsive'
//    ],
//    'columns' => [
//
//]); ?>