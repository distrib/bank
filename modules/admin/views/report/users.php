<?php

/* @var $this yii\web\View */

use app\components\grid\EnumColumn;
use app\models\User;
use kartik\grid\GridView;

$this->title = 'Отчет по пользователям';
    $this->params['breadcrumbs'][] = $this->title;
    ?>
    <div class="user-index">

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!---->
    <!--    <p>-->
    <!--        --><?php //echo Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->



<?php
$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'bmw_id',
        'value' => 'profile.bmw_id'
    ],
    [
        'class' => EnumColumn::className(),
        'attribute' => 'manual_register',
        'enum' => User::RegisterType(),

    ],
    [
        'attribute' => 'crm_verification',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'crm_verification',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
    ],
    [
        'class' => EnumColumn::className(),
        'attribute' => 'status',
        'enum' => User::statuses(),
        'filter' =>User::statusesReport(),
    ],

    //   'username',

    [
        'attribute' => 'firstname',
        'value' => 'profile.firstname'
    ],
    [
        'attribute' => 'lastname',
        'value' => 'profile.lastname'
    ],

    [
        'attribute' => 'middlename',
        'value' => 'profile.middlename'
    ],

    [
        'attribute' => 'gender',
        'class' => EnumColumn::className(),
        'enum' => [1=>'Муж', 2=>'жен'],
        'filter' =>[1=>'Муж', 2=>'жен'],
        'value' => 'profile.gender'
    ],

    [
        'label' => 'Дата рождения',
        'attribute' => 'birthday',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'birthday',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
        'value' => 'profile.birthday'
    ],
    'email:email',

    [
        'attribute' => 'phone',
        'value' => 'profile.phone'
    ],

    [
        'attribute' => 'town',
        'label' => 'Город участника',
        'value' => 'profile.town'
    ],

    [
        'label' => 'VIN',
        'attribute' => 'car_vin',
        'value' => 'lastCar.vin'
    ],
    [
        'label' => 'Модель',
        'attribute' => 'car_model',
        'value' => 'lastCar.model'
    ],

    [
        'attribute' => 'dealer',
        'label' => 'Дилер',
        'value' => 'lastCar.dealerModel.name'
    ],
    [
        'label' => 'Город дилера',
        'attribute' => 'dealer_town',
        'value' => 'lastCar.dealer_town'
    ],

    [
        'label' => 'Дата покупки автомобиля',
        'attribute' => 'last_car_purchase_date',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'last_car_purchase_date',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
        'value' => 'lastCar.purchase_date'
    ],

    [
        'label' => 'Дата окончания срока членства',

        'content'=>function($model){
            return $model->getMembershipEndDate();
        },
    ],
    [
        'attribute' => 'membership_days',
        'label' => 'Дней в программе',

        'content'=>function($model){
            return $model-> getProgramDays();
        },
    ],

    [
        'label' => 'Дата покупки первого автомобиля',
        'attribute' => 'first_car_purchase_date',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'first_car_purchase_date',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
        'value' => 'firstCar.purchase_date'
    ],

    [
        'attribute' => 'count_cars',
        'label' => 'Количество Автомобилей',

        'content'=>function($model){
            return $model->getCars()->count();
        },
    ],

    [
        'attribute' => 'connection',
        'label' => 'Способ связи',
        'format' => 'raw',
        'filter' => [
            1 => 'E-mail',
            2 => 'Телефон',
        ],
        'value' => function ($model) {

            if ($model->profile->connection == 1)
                return "<span style='color:green'>E-mail</span>";
            elseif ($model->profile->connection == 2)
                return "<span style='color:blue'>Телефон</span>";

        },
    ],




    //  'created_at:datetime',
    //  'updated_at',


];
echo \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
     'exportConfig' => [
            \kartik\export\ExportMenu::FORMAT_TEXT => false,
            \kartik\export\ExportMenu::FORMAT_HTML => false,
            \kartik\export\ExportMenu::FORMAT_EXCEL => false,
            \kartik\export\ExportMenu::FORMAT_PDF =>  false,
         \kartik\export\ExportMenu::FORMAT_CSV =>  false
            ],


]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,


    'columns' => $columns,
]);
?>


<?php //echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'options' => [
//        'class' => 'grid-view table-responsive'
//    ],
//    'columns' => [
//
//]); ?>