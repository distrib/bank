<?php

/* @var $this yii\web\View */

use app\components\grid\EnumColumn;
use app\models\User;
use kartik\grid\GridView;

$this->title = 'Отчет по пользователям';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="user-index">

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!---->
    <!--    <p>-->
    <!--        --><?php //echo Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->


<?php
$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'bmw_id',
        'value' => 'profile.bmw_id'
    ],

    [
        'attribute' => 'firstname',
        'value' => 'profile.firstname'
    ],
    [
        'attribute' => 'lastname',
        'value' => 'profile.lastname'
    ],

    [
        'attribute' => 'middlename',
        'value' => 'profile.middlename'
    ],

    [
        'attribute' => 'gender',
        'class' => EnumColumn::className(),
        'enum' => [1 => 'Муж', 2 => 'жен'],
        'filter' => [1 => 'Муж', 2 => 'жен'],
        'value' => 'profile.gender'
    ],

    [
        'label' => 'Дата рождения',
        'attribute' => 'birthday',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'birthday',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
        'value' => 'profile.birthday'
    ],

    [
        'label' => 'Раздел Интересы заполен?',
        'value' => function ($model) {

            return $model->getInterests()->count() > 0 ? 'Да' : 'Нет';

        }

    ],

    'email:email',

    [
        'attribute' => 'phone',
        'value' => 'profile.phone'
    ],

    [
        'attribute' => 'town',
        'label' => 'Город участника',
        'value' => 'profile.town'
    ],

    [
        'label' => 'VIN',
        'attribute' => 'car_vin',
        'value' => 'lastCar.vin'
    ],
    [
        'label' => 'Модель',
        'attribute' => 'car_model',
        'value' => 'lastCar.model'
    ],

    [
        'attribute' => 'count_cars',
        'label' => 'Количество Автомобилей',

        'content' => function ($model) {
            return $model->getCars()->count();
        },
    ],
    [
        'label' => 'Дата покупки автомобиля',
        'attribute' => 'last_car_purchase_date',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'last_car_purchase_date',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
        'value' => 'lastCar.purchase_date'
    ],
    [
        'label' => 'Дата окончания срока членства',

        'content' => function ($model) {
            return $model->getMembershipEndDate();
        },
    ],
    [
        'attribute' => 'membership_days',
        'label' => 'Дней в программе',

        'content' => function ($model) {
            return $model->getProgramDays();
        },
    ],

    [
        'attribute' => 'connection',
        'label' => 'Способ связи',
        'format' => 'raw',
        'filter' => [
            1 => 'E-mail',
            2 => 'Телефон',
        ],
        'value' => function ($model) {

            if ($model->profile->connection == 1)
                return "<span style='color:green'>E-mail</span>";
            elseif ($model->profile->connection == 2)
                return "<span style='color:blue'>Телефон</span>";

        },
    ],



    //  'created_at:datetime',
    //  'updated_at',


];

$export_columns = $columns;
foreach (\app\models\Interests::find()->all() as $interest) {
    $idInterest = $interest->id;
    $export_columns[] =  [
        'label' => 'Интересы',

        'content' => function ($model) use ($idInterest) {
           $interest =  $model->getInterests()->where(['interests.id'=>$idInterest])->one();
            return  $interest ? strip_tags($interest->title):'';
        },
    ];

}

$columns[] = [
    'label' => 'Интересы',

    'content' => function ($model) {
        $interestAr=[];
    foreach ( $model->interests as $interest) {
        $interestAr[] = $interest->title;
    }
        return  implode(', ',$interestAr);
    },
];

echo \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $export_columns,
    'exportConfig' => [
        \kartik\export\ExportMenu::FORMAT_TEXT => false,
        \kartik\export\ExportMenu::FORMAT_HTML => false,
        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
        \kartik\export\ExportMenu::FORMAT_PDF => false,
        \kartik\export\ExportMenu::FORMAT_CSV => false
    ],


]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,


    'columns' => $columns,
]);
?>


<?php //echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'options' => [
//        'class' => 'grid-view table-responsive'
//    ],
//    'columns' => [
//
//]); ?>