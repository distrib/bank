<?php

/* @var $this yii\web\View */

use app\components\grid\EnumColumn;
use app\models\User;
use kartik\grid\GridView;

$this->title = 'Отчет по обратной связи';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="user-index">

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!---->
    <!--    <p>-->
    <!--        --><?php //echo Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->


<?php
$columns = [
    ['class' => 'yii\grid\SerialColumn'],

    [
        'attribute'=>'id',
        'format'=>'text',
        'label'=>'Номер обращения',
    ],
    [
        'attribute'=>'created_at',
        'label'=>'Дата обращения',
        'format'=>['date', 'php:d.m.Y'],
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'model' => $searchModel,
            'attribute' => 'created_at',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
        ]),
    ],

    [
        'label' => 'Статус обработки обращения',
        'attribute'=>'processed',
        'filter' => [
            0 => 'Нет',
            1 => 'Да',
        ],
        'content'=>function($data){
            if ($data['processed'] == 1)
                return "<span style='color:green'>Да</span>";
            else
                return "<span style='color:red'>Нет</span>";
        },
    ],
    [
        'attribute'=>'processed_time',
        'label'=>'Дата обработки обращения',
        'format'=>['date', 'php:d.m.Y'],
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'model' => $searchModel,
            'attribute' => 'processed_time',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
        ]),
    ],

    [
        'attribute'=>'problem',
        'label' => 'Текст обращения',
        'contentOptions' =>function ($model, $key, $index, $column){
            return ['class' => 'text'];
        },
    ],

    [
        'attribute'=>'name',
        'format'=>'text',
        'label'=>'Имя отправителя',
    ],
    [
        'attribute'=>'phone',
        'format'=>'text',
        'label'=>'Телефон',
    ],
];





echo \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'exportConfig' => [
        \kartik\export\ExportMenu::FORMAT_TEXT => false,
        \kartik\export\ExportMenu::FORMAT_HTML => false,
        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
        \kartik\export\ExportMenu::FORMAT_PDF => false,
        \kartik\export\ExportMenu::FORMAT_CSV => false
    ],


]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,


    'columns' => $columns,
]);
?>


<?php //echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'options' => [
//        'class' => 'grid-view table-responsive'
//    ],
//    'columns' => [
//
//]); ?>