<?php

/* @var $this yii\web\View */

use app\components\grid\EnumColumn;
use app\models\User;
use kartik\grid\GridView;

$this->title = 'Отчет по Автомобилям';
    $this->params['breadcrumbs'][] = $this->title;
    ?>
    <div class="user-index">

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!---->
    <!--    <p>-->
    <!--        --><?php //echo Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->



<?php
$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'vin',
    ],
    [
        'attribute' => 'model',
    ],
    [
        'attribute' => 'dealer',
    ],
    [
        'attribute' => 'dealer_town',
    ],
    [
        'label' => 'Дата покупки автомобиля',
        'attribute' => 'purchase_date',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'purchase_date',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),

    ],

    [
        'class' => EnumColumn::className(),
        'attribute' => 'manual_register',
        'enum' => User::RegisterType(),
        'value' => 'user.manual_register'
    ],

    [
        'attribute' => 'crm_verification',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'crm_verification',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),

        'value' => 'user.crm_verification'
    ],

    [
        'attribute' => 'bmw_status',
        'filter' =>[
                0=>'Не Проверено CRM',
                1=>'Ок',
                3=> 'Дубль VIN'
                ],
        'value' => function ($model) {
            if (in_array($model->user->bmw_status,[1,2])) return 'Ок'; else return User::BmwStatuses()[$model->user->bmw_status];

        }
    ],

    [
        'attribute' => 'bmw_id',
        'value' => 'profile.bmw_id'
    ],

    [
        'attribute' => 'firstname',
        'value' => 'profile.firstname'
    ],
    [
        'attribute' => 'lastname',
        'value' => 'profile.lastname'
    ],

    [
        'attribute' => 'middlename',
        'value' => 'profile.middlename'
    ],
    [
        'attribute' => 'town',
        'label' => 'Город участника',
        'value' => 'profile.town'
    ],
    [
        'attribute' => 'email',
        'value' => 'user.email'
    ],
    [
        'attribute' => 'phone',
        'value' => 'profile.phone'
    ],










    //  'created_at:datetime',
    //  'updated_at',


];
echo \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'exportConfig' => [
        \kartik\export\ExportMenu::FORMAT_TEXT => false,
        \kartik\export\ExportMenu::FORMAT_HTML => false,
        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
        \kartik\export\ExportMenu::FORMAT_PDF =>  false,
        \kartik\export\ExportMenu::FORMAT_CSV =>  false
    ],


]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,


    'columns' => $columns,
]);
?>


<?php //echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'options' => [
//        'class' => 'grid-view table-responsive'
//    ],
//    'columns' => [
//
//]); ?>