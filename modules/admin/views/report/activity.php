<?php

/* @var $this yii\web\View */

use app\components\grid\EnumColumn;
use app\models\User;
use kartik\grid\GridView;

$this->title = 'Отчет по пользователям';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="user-index">

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <!---->
    <!--    <p>-->
    <!--        --><?php //echo Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->


<?php
$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'bmw_id',
        'value' => 'profile.bmw_id'
    ],

    [
        'attribute' => 'firstname',
        'value' => 'profile.firstname'
    ],
    [
        'attribute' => 'lastname',
        'value' => 'profile.lastname'
    ],

    [
        'attribute' => 'middlename',
        'value' => 'profile.middlename'
    ],


    [
        'label' => 'Дата регистрации',
        'attribute' => 'activation_link_date',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'activation_link_date',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
        'value' => 'activation_link_date'
    ],

    [
        'label' => 'Количество авторизаций',
        'attribute' => 'login_count',
    ],


    [
        'label' => 'Дата последней авторизации',
        'attribute' => 'last_login_date',
        'filter' => \yii\jui\DatePicker::widget([
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'changeMonth' => true,
                'yearRange' => '1980:2099',
                'changeYear' => true,
            ],
            'model' => $searchModel,
            'attribute' => 'last_login_date',
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]),
    ],

    [
        'label' => 'Количество отправленных обращений',
        'value' => function ($model) {

            return $model->getCallBacks()->count();

        }


    ],

    [
        'label' => 'Количество регистраций на события и спец. предложения',
        'value' => function ($model) {

            return $model->getApprovedEventApplications()->count() + $model->getApprovedOfferingApplications()->count();

        }


    ],




    //  'created_at:datetime',
    //  'updated_at',


];
echo \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'exportConfig' => [
        \kartik\export\ExportMenu::FORMAT_TEXT => false,
        \kartik\export\ExportMenu::FORMAT_HTML => false,
        \kartik\export\ExportMenu::FORMAT_EXCEL => false,
        \kartik\export\ExportMenu::FORMAT_PDF => false,
        \kartik\export\ExportMenu::FORMAT_CSV => false
    ],


]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,


    'columns' => $columns,
]);
?>


<?php //echo GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'options' => [
//        'class' => 'grid-view table-responsive'
//    ],
//    'columns' => [
//
//]); ?>