<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DealersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дилеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealers-index">
    <p>
        <?= Html::a('Создать Дилера', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?
    $columns = [
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
            'contentOptions' =>function ($model, $key, $index, $column){
                return ['class' => 'freeze'];
            },
        ],
        ['class' => 'yii\grid\SerialColumn'],
        //'id',
        'number',
        'name',
    ];

    echo \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'exportConfig' => [
            \kartik\export\ExportMenu::FORMAT_TEXT => false,
            \kartik\export\ExportMenu::FORMAT_HTML => false,
            \kartik\export\ExportMenu::FORMAT_EXCEL => false,
            \kartik\export\ExportMenu::FORMAT_PDF =>  false,
            \kartik\export\ExportMenu::FORMAT_CSV =>  false
        ],

    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ], //{delete}
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'number',
            'name',
        ],
    ]); ?>


</div>
