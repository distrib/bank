<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dealers */

$this->title = 'Создать Дилера';
$this->params['breadcrumbs'][] = ['label' => 'Дилеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
