<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Interests */

$this->title = 'Изменить интерес: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Интересы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="interests-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
