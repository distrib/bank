<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\InterestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Интересы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="interests-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--    <p>-->
    <!--        --><? //= Html::a('Create Interests', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['class' => 'freeze'];
                },
            ],
            ['class' => 'yii\grid\SerialColumn'],
            //   'id',
            'title',
            [
                'attribute' => 'image',
                'filter' => false,
                'content' => function ($model) {
                    return Html::img('/uploads/' . $model->image, ['width' => '200px']);
                },
            ],
//            [
//                'attribute' => 'active',
//                'filter' => [
//                    0 => 'Нет',
//                    1 => 'Да',
//                ],
//                'content'=>function($data){
//                    if ($data['active'] == 1)
//                        return "<span style='color:green'>Да</span>";
//                    else
//                        return "<span style='color:red'>Нет</span>";
//                },
//            ],
            // 'order',
            //'created_at',
            //'updated_at',
        ],
    ]); ?>


</div>
