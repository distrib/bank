<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Interests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="interests-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture')->widget(
        \trntv\filekit\widget\Upload::className(),
        [
            'url' => ['/admin/file-storage/upload'],
            'maxFileSize' => 5*1024*1024, // 5 MiB
        ]);

    //TODO Тут вписать справочную инфу по картинке
    ?>
    <p> <b>Комментарий: </b><?=$model->comment?></p>

<!--    --><?//= $form->field($model, 'order')->textInput(['type' => 'number', 'min' => 1, 'max' => 5000]) ?>
<!---->
<!--    --><?//= $form->field($model, 'active')->checkbox() ?>



    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
