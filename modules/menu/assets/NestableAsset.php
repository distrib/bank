<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 14.06.2019
 * Time: 11:53
 */

namespace app\modules\menu\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class NestableAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/menu/assets/js';

    public $js = [
        'jquery.nestable.js'
    ];
    public $depends = [
        JqueryAsset::class
    ];

}