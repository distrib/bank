<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 19.06.2019
 * Time: 14:41
 */

namespace app\modules\menu\assets;


use app\modules\admin\assets\AppAsset;
use yii\web\AssetBundle;

class NestableMyAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/menu/assets/css';
    public $css = [
        'jquery.nestable.css'
    ];
    public $depends = [
        AppAsset::class
    ];

}