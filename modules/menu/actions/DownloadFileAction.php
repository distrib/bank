<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21.07.2019
 * Time: 9:19
 */

namespace app\modules\menu\actions;


use app\modules\menu\models\Menu;
use Yii;
use yii\base\Action;

class DownloadFileAction extends Action
{
    protected $model;

    public function run($id){
//        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->download($id);
    }

    public function download($id)
    {

        if(!$this->hasModel($id)){
            return $this->referrer();
        }
        $path = Yii::getAlias(Menu::FILE_PATH.'/'.$this->model->file);
        if (is_file($path)) {
            return Yii::$app->response->sendFile($path);
        }
        return $this->referrer();
    }

    protected function hasModel($id){
        $this->model = Menu::find()->active()->self($id)->one();
        if (!empty($this->model)) {
            return true;
        }else{
            return false;
        }
    }

    public function referrer()
    {

        if(isset(Yii::$app->request->referrer) && !empty(Yii::$app->request->referrer)){

            return Yii::$app->controller->redirect(Yii::$app->request->referrer);

        } else {

            return  Yii::$app->controller->goHome();

        }
    }

}