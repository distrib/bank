<ul>
    <?php foreach ($menus as $menu): ?>
        <?php if (Yii::$app->request->pathInfo == $menu->url) {
            $class = 'active';
        } else {
            $class = '';
        }
        ?>
        <li class="<?= $class ?>">
            <a href="<?= $menu->generateUrls ?>"><?= $menu->title ?></a>
        </li>
    <?php endforeach; ?>
</ul>
