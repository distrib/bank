<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 14.06.2019
 * Time: 12:37
 */

namespace app\modules\menu\admin\widgets;


use app\modules\menu\assets\NestableAsset;
use app\modules\menu\assets\NestableMyAsset;
use app\modules\menu\components\ParentMenuWidget;
use app\modules\menu\models\Menu;
use app\modules\menu\models\MenuCore;
use Yii;
use yii\helpers\Url;

class NestableMenuWidget extends ParentMenuWidget
{
    public $type = Menu::TYPE_HEADER;

    public function run()
    {
        $this->registerAssets();
        $this->registerJs();
        $menu_query = $this->getQuery();
        if(empty($menu_query)){return false;}
        $menu_object = $this->treeRecursive($menu_query, 0);
        $menu_view_nestable = $this->getViewNestable($menu_object);
        return $menu_view_nestable;
    }

    public function registerAssets(){
        NestableAsset::register($this->view);
        NestableMyAsset::register($this->view);
    }

    public function getQuery()
    {
        $menus = MenuCore::find()
            ->orderBy(['sort' => SORT_ASC])
            ->type($this->type)
            ->all();
        return $menus;
    }

    public function getViewNestable($menu_object)
    {
        $menu_view_nestable = '';
        if (!empty($menu_object)) {
            foreach ($menu_object as $menu) {
                $menu_view_nestable .= $this->renderMenu($menu);
            }
        }
        return $menu_view_nestable;
    }

    public function renderMenu($item)
    {
        $out = '';
        $out .= '<li class="dd-item dd3-item" data-id="' . $item->menu->id . '">
				<div class="dd-handle dd3-handle"></div>
				<div class="dd3-content">' . $item->menu->title . '
					<div class="dd-link">' .  $item->menu->link.' '.self::getStatus($item->menu).'</div>
					<div class="dd-action-btns">
						<a data-pjax="0" href="' . Url::to(['/admin/menu/default/update', 'id' =>  $item->menu->id]) . '" class="btn btn-complete dd-edit-btn">
							<i class="fa fa-pencil"></i>
						</a>
						<a id="menu' .  $item->menu->id . '" data-post="' .  $item->menu->id . '" class="btn btn-danger dd-delete-btn menu-item-delete">
							<i class="fa fa-trash"></i>
						</a>
					</div>
				</div>';

        if (!empty($item->children)) {
            $out .= '<ol class="dd-list">';
            foreach ($item->children as $child_item) {
                $out .= $this->renderMenu($child_item);
            }
            $out .= '</ol>';
        }
        $out .= '</li>';
        return $out;
    }


    /**
     * @param $item
     * @return string
     */
    public static function getStatus($item){
        if($item->status == Menu::STATUS_NO_ACTIVE){
           return '<span class="label label-danger">'. Yii::t('app','Не активный').'</span>';
        }else{
          return '<span class="label label-success">'. Yii::t('app','Активный').'</span>';
        }
    }



    public function registerJs()
    {
        $js = <<<JS
	$('#menu-nestable').nestable({
        maxDepth: 4
    }).on('change', function () 
    {
        var items = $('#menu-nestable').nestable("serialize");
        var data = {};
        data.data = JSON.stringify(items);
        $.post('/admin/menu/default/index', data);
    });

	$('.menu-item-delete').on('click', function() 
	{
		var isDelete = confirm("Do you Really wont to remove this?");

		if( isDelete == true ) 
		{
            var id = $(this).data('post');

            if( id > 0) 
            {
                $.post('/admin/menu/default/delete?id=' + id);
                
                $(this).closest('.dd-item').remove();
            }
		}
		
	});
	
	$('.js-create-menu').on('click', function() 
	{
    	$('#create-menu-modal').modal('show');
	});
	

JS;
        $this->view->registerJs($js);
    }

}