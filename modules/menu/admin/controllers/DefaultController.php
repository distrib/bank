<?php

namespace app\modules\menu\admin\controllers;

use app\modules\menu\actions\DownloadFileAction;
use app\modules\menu\models\Menu;
use app\modules\menu\models\MenuCore;
use Yii;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'roles' => ['canAdmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'download'=>DownloadFileAction::class
        ];
    }

    public function actionIndex()
    {
        if(Yii::$app->request->post())
        {
            if ($data = @json_decode(Yii::$app->request->post('data'), true))
            {
                return MenuCore::sortTree($data);
            }
        }
        return $this->render('index');
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post()) && $model->save() )
        {

            return $this->redirect(['update', 'id' => $model->id]);

        } else {

            return $this->render('create', [
                'model' => $model,
            ]);

        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['update', 'id' => $model->id]);

        } else {

            return $this->render('update', [
                'model' => $model,
            ]);

        }
    }

    public function actionPlace()
    {
        $id = Yii::$app->request->get('id');
        $form = new Menu();

        if(Yii::$app->request->isAjax)
        {
            if ($form->load(Yii::$app->request->post()) )
            {
                $form->type = $id;
                $form->save();
            }
        }
        return $this->render('place', [
            'FormModel' => $form,
            'type_id' => $id
        ]);
    }


    public function actionCreateModal()
    {
        $model = new Menu();
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->setFlash('success',Yii::t('app','Меню удачно создано'));
            return $this->referrer();
        }
        Yii::$app->session->setFlash('error',Yii::t('app',$model->getErrors()));
        return $this->referrer();
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return true;
    }

    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null)
        {
            return $model;

        } else {

            throw new NotFoundHttpException('Ошибка 404');

        }
    }


    public function referrer()
    {

        if(isset(Yii::$app->request->referrer) && !empty(Yii::$app->request->referrer)){

            return Yii::$app->controller->redirect(Yii::$app->request->referrer);

        } else {

            return  Yii::$app->controller->goHome();

        }
    }
}
