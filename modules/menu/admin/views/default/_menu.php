<?php


use app\modules\menu\models\MenuCore;
use yii\helpers\Url;

$active = isset($active) ? $active : 1;
$types = MenuCore::getTypes();
?>

<?php foreach( $types as $index => $type ): ?>

	<li class="<?= $active == $index ? 'active' : '' ?>">

		<a data-pjax="0" href="<?= Url::to(['/admin/menu/default/place', 'id' => $index]) ?>"><?= $type ?></a>

	</li>

<?php endforeach; ?>
