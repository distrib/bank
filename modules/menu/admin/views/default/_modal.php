<?php

use app\components\fileUpload\FileUploadWidget;
use app\modules\menu\components\MenuTypeWidget;
use app\modules\menu\models\MenuCore;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="modal fade stick-up" id="create-menu-modal">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="pg-close fs-14"></i>
                </button>
                <h5><span class="semi-bold"><?= Yii::t('app', 'Создать меню') ?></span></h5>
            </div>

            <div class="modal-body">
                <?= MenuTypeWidget::widget(['classFile' => 'field-menu-file','classUrl' => 'field-menu-url'])?>
                <?php $form = ActiveForm::begin([
                    'id' => 'create-menu-form',
                    'action' => '/admin/menu/default/create-modal'
                ]); ?>

                <?= $form->field($FormModel, 'title')->textInput() ?>


                <?= $form->field($FormModel, 'url')->textInput() ?>


                <?= $form->field($FormModel, 'file',['options'=>['class'=>'hidden']])->fileInput() ?>


                <?= $form->field($FormModel, 'type')->dropDownList(
                    MenuCore::getTypes()
                ) ?>

                <?= $form->field($FormModel, 'status')->dropDownList(
                    MenuCore::getStatusList()
                ) ?>



                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary', 'id' => 'menu_submit']) ?>

                <?php ActiveForm::end(); ?>

            </div>

        </div>

    </div>

</div>

