<?php


use app\modules\menu\admin\widgets\NestableMenuWidget;
use app\modules\menu\assets\NestableMyAsset;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', Yii::t('app', 'Меню'));
$this->params['breadcrumbs'][] = [
	'label' => $this->title,
	'url'   => Url::to(['index']),
	'class' => 'breadcrumbs__link'
];
?>
	<div class="container-fluid container-fixed-lg m-t-20">

	    <div class="panel panel-transparent">

	        <div class="panel-body">

	            <div class="panel panel-default">
	                <div class="panel-body page-header-block">

		                <h2><?= Html::encode($this->title) ?></h2>

		                <a class="btn btn-success js-create-menu" href="#"><?= Yii::t('app','Создать меню') ?></a>

	                </div>

	            </div>

	        </div>

		    <div class="panel-body  row-default">

			    <div class="row">

				    <div class="col-md-12">

					    <div class="panel panel-default">

						    <ul class="nav nav-tabs nav-tabs-simple">

								<?= $this->render('_menu', [
									'active' => $type_id
								])?>

						    </ul>

					    </div>

				    </div>

			    </div>

		    </div>

		    <div class="row">

			    <div class="col-md-6 col-md-offset-3">

				    <div class="panel-body">

					    <div class="panel panel-default">

						    <div class="panel-body page-header-block">

							    <div class="dd" id="menu-nestable">

								    <ol class="dd-list">
									    <?= NestableMenuWidget::widget(['type'=>$type_id]);?>
								    </ol>

							    </div>

						    </div>

					    </div>

				    </div>

			    </div>

		    </div>

	    </div>

	</div>


<?= $this->render('_modal', [
	'FormModel' => $FormModel,
	'type' => $type_id
])?>

