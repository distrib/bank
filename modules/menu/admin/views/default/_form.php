<?php

use app\modules\menu\components\MenuTypeWidget;
use app\modules\menu\models\MenuCore;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="col-md-8">

    <div class="panel panel-default">

        <div class="panel-body">
            <?= MenuTypeWidget::widget(['classFile' => 'field-menu-file','classUrl' => 'field-menu-url','classParent' => 'panel-body'])?>
        </div>

    </div>

</div>

<?php $form = ActiveForm::begin([
    'id' => 'create-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'errorCssClass' => '',
    'options' => ['enctype' => 'multipart/form-data']
]); ?>

    <div class="col-md-8">

        <div class="panel panel-default">

            <div class="panel-body">
                <?= $form->field($model, 'title')->textInput() ?>

				<?= $form->field($model, 'url' )->textInput() ?>

                <?= $form->field($model, 'file',['options'=>['class'=>'hidden']])->fileInput() ?>

            </div>

        </div>

    </div>

	<div class="col-md-4">

		<div class="panel panel-default">

			<div class="panel-body">

				<?= $form->field($model, 'type')->dropDownList(
                   MenuCore::getTypes()
				) ?>

                <?= $form->field($model, 'status')->dropDownList(
                   MenuCore::getStatusList()
                ) ?>


			</div>

		</div>

	</div>

    <div class="col-md-12">

        <?=  Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') :  Yii::t('app', 'Обновить'), ['class' => 'btn btn-primary']) ?>

    </div>

<?php ActiveForm::end(); ?>
