<?php

namespace app\modules\menu\models;

use app\components\fileUpload\FileUploadBehavior;
use Yii;
use yii\db\ActiveRecord;

class Menu extends ActiveRecord
{

    const STATUS_ACTIVE = 1;

    const STATUS_NO_ACTIVE = 0;

    const TYPE_HEADER = 1;

    const TYPE_FOOTER = 2;

    const TYPE_FOOTER_SMALL = 3;

    const TYPE_REGISTR = 4;

    public $childs;

    const FILE_PATH = '@app/web/uploads/menu';

    public static function tableName()
    {
        return 'menu';
    }
    public function behaviors()
    {
        $behaviors = [
            'fileUpload'=>[
                'class'=>FileUploadBehavior::className(),
                'attribute' => 'file',
                'FILE_PATH' => self::FILE_PATH,
            ]
        ];
        return $behaviors;
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['parent_id', 'type', 'sort', 'status'], 'default', 'value' => null],
            [['parent_id', 'type', 'sort','status'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
            ['file','safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Ид'),
            'title' => Yii::t('app', 'Заголовок'),
            'url' => Yii::t('app', 'Урл'),
            'parent_id' => Yii::t('app', 'Парент'),
            'type' => Yii::t('app', 'Тип'),
            'file' => Yii::t('app', 'Загрузить файл'),
            'sort' => Yii::t('app', 'Сортировка'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }


    public static function find()
    {
        return new MenuQuery(get_called_class());
    }
}
