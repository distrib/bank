<?php
namespace app\modules\menu\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;

class MenuQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
    public function type($type){
        return $this->andWhere([Menu::tableName().'.type'=>$type]);
    }
    public function parents(){
        return $this->andWhere([Menu::tableName().'parent_id'=>0]);
    }
    public function childrens(){
        return $this->joinWith('childrens');
    }

    public function active(){
        return $this->andWhere(['status'=>Menu::STATUS_ACTIVE]);
    }

    public function sort(){
        return $this->orderBy('sort');
    }

    public function self($id){
        return $this->andWhere(['id'=>$id]);
    }
}