<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.07.2019
 * Time: 18:51
 */

namespace app\modules\menu\models;



use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class MenuCore extends Menu
{

    public static $_singleton = null;

    /**
     * удаленние внутренных дочерных елементов из базы данных
     * @var Menu id
     */
    public function afterDelete()
    {

        self::deleteAll(['parent_id' => $this->id]);

    }


    /**
     * @param $data
     * @param bool $parent
     *  Сортировка элементов
     */
    public static function sortTree($data, $parent = false)
    {
        $pos = 0;

        foreach ($data as $item)
        {
            if ($link = self::findOne($item['id']))
            {
                $link->updateAttributes([
                    'parent_id' => $parent,
                    'sort'	    => $pos++,
                ]);

                if (isset($item['children']))
                    self::sortTree($item['children'], $link->id);
            }
        }
    }

    /**
     * @param $data
     * @param int $rootID
     * @return array
     * Строим массив элементов
     */
    protected function buildTree($data, $rootID = 0)
    {
        $tree = [];
        foreach ($data as $id => $node) {
            if ($node->parent_id == $rootID) {
                unset($data[$id]);
                $node->childs = $this->buildTree($data, $node->id);
                $tree[] = $node;
            }
        }
        return $tree;
    }

    /**
     * @param $type
     * @return array
     * Генерируем дерево элементов
     */
    public function getTree($type)
    {
        $data = Menu::find()->type($type)->active()->sort()->all();
        if(empty($data)){
            return [];
        }
        return $this->buildTree($data);
    }

    /**
     * @return array
     * Типы меню
     */
    public static function getTypes(){
        return [
            Menu::TYPE_HEADER    => Yii::t('app', 'Верхнее меню'),
            Menu::TYPE_FOOTER_SMALL    => Yii::t('app', 'Нижнее меню'),
            Menu::TYPE_FOOTER    => Yii::t('app', 'Тест-драйв'),
            Menu::TYPE_REGISTR    => Yii::t('app', 'Регистрация'),
        ];
    }

    /**
     * @return array
     * Статус лист меню
     */
    public static function getStatusList(){
        return [
            Menu::STATUS_ACTIVE => Yii::t('app','Активен'),
            Menu::STATUS_NO_ACTIVE => Yii::t('app','Не активный')
        ];
    }

    /**
     * @return mixed|string
     * генерируем урл для админки
     */
    public function getLink(){
        if(empty($this->url)&&!empty($this->file)){
            return Html::a('Скачать файл', Url::to(['/admin/menu/default/download','id'=>$this->id]));
        }else{
            return $this->url;
        }
    }

    /**
     * @return mixed|string
     * Генерируем урл для frontend
     */
    public function getGenerateUrls(){
        if(empty($this->url)&&!empty($this->file)){
            return  Url::to(['/site/download','id'=>$this->id]);
        }else{
            return $this->url;
        }
    }

    /**
     * @return Menu[]|MenuCore[]|array|\yii\db\ActiveRecord[]|null
     * singleton Для уменщение запросов в бд
     */
    public static function getAll(){

        if(self::$_singleton == null){
            return self::$_singleton = self::find()->sort()->active()->all();
        }else{
            return self::$_singleton;
        }
    }

}