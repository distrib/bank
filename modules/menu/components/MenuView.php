<?php

namespace app\modules\menu\components;



use app\modules\menu\models\Menu;

class MenuView
{
    public $menu;

    public $children;

    public function __construct(Menu $menu, array $children)
    {
        $this->menu = $menu;
        $this->children = $children;
    }
}