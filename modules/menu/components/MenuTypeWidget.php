<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21.07.2019
 * Time: 0:49
 */

namespace app\modules\menu\components;


use yii\base\Widget;
use yii\helpers\Html;

class MenuTypeWidget extends Widget
{

    public $classFile = 'file';

    public $classUrl = 'url';

    public $classParent = 'modal-body';

    public function run()
    {
        $this->registerJs();
        return $this->html();
    }

    public function html(){
      return  Html::button('Загрузить файл',['class'=>'btn btn-success tab-button']);
    }


    public function registerJs(){

        $js =<<<JS
        $('.tab-button').on('click',function() {
          var parent = $(this).parents('.{$this->classParent}');
          parent.find('.{$this->classFile}').toggleClass('hidden');
          parent.find('.{$this->classUrl}').toggleClass('hidden');
          $(this).toggleClass('active');
          if($(this).hasClass('active')){
                $(this).text('Написать ссылку')
          }else{
                $(this).text('Загрузить файл')
          }
        });

JS;
        return $this->view->registerJs($js);
    }


}