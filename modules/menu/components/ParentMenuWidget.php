<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 14.06.2019
 * Time: 12:47
 */

namespace app\modules\menu\components;
use yii\base\Widget;

class ParentMenuWidget extends Widget
{
    public function treeRecursive(&$menus, $parentId)
    {
        $items = [];
        foreach ($menus as $menu) {
            if ($menu->parent_id == $parentId) {
                $items[] = new MenuView($menu, $this->treeRecursive($menus, $menu->id));
            }
        }
        return $items;
    }

}