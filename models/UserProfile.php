<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $middlename
 * @property string $phone
 * @property string $gender
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserProfile extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    /**
     * значения поля "Пол"
     */
    const GENDER_MALE = 1; // мужской
    const GENDER_FEMALE = 2; // женский

    public static function tableName()
    {
        return 'user_profile';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['firstname', 'lastname', 'phone', 'connection', 'gender', 'user_id'], 'required'],
            [[ 'user_id'], 'required'],
            [['firstname', 'lastname', 'middlename'], 'string', 'max' => 255],
            ['phone', 'match', 'pattern' => '/^7[0-9]{10}$/', 'message' => 'Не верный формат' ],
            [['gender'], 'in', 'range' => [NULL, self::GENDER_FEMALE, self::GENDER_MALE]],
            [['created_at', 'updated_at', 'user_id'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'middlename' => 'Отчество',
            'photo' => 'Изображение',
            'town' => 'Город',
            'picture' => 'Изображение',
            'phone' => 'Телефон',
            'card_number' => 'Карта лояльности',
            'connection' => 'Желаемый способ связи',
            'gender' => 'Пол',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
            'user_id' => 'Юзер',
            'bmw_id' => 'ID CRM',
            'birthday' => 'Дата рождения',
            'email' => 'E-mail',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function checkUniquePhone()
    {
        $res = $this::find()->where(['phone'=>$this->phone])->andWhere(['not',['id' => $this->id]])->one();

        if( $res ){
            $this->addError('phone', 'Телефон не уникальный');
            return false;
        }else{
            return true;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getFullName()
    {
        return $this->lastname.' '.$this->firstname.' '.$this->middlename;
    }
}
