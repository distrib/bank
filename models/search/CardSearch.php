<?php

namespace app\models\search;

use app\models\Card;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\db\Expression;

/**
 * EventToUsersSearch represents the model behind the search form of `app\models\EventToUsers`.
 */
class CardSearch extends Card
{
    public $sum;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'user_id', 'card_number', 'month', 'year', 'pin', 'updated_at'
                ],
                'integer'
            ],
            [
                [
                    'card_number', 'pin',
                ],
                'string'
            ],
            [['created_at', 'sum'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Card::find()
            ->select('card.*')
            ->addSelect(['sum' => new Expression('(SELECT round(sum(if(sender=card.id,-`sum`,`sum`)),2) FROM cash_flow WHERE cash_flow.`sender`=card.`id` OR cash_flow.`recipient`=card.`id`)')])
            
        //(SELECT round(sum(if(sender=card.id,-`sum`,`sum`)),2) FROM cash_flow WHERE cash_flow.`sender`=card.`id` OR cash_flow.`recipient`=card.`id`) as sum')
            ->joinWith(['user'])
            //->leftJoin(['cashFlow' => $subQuery], 'cashFlow.id = card.id');
            ->where(['user_id'=>\Yii::$app->user->identity->getId()])
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'card_number' => $this->card_number,
            'month' => $this->month,
            'year' => $this->year,
            'pin' => $this->pin,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(["DATE_FORMAT(FROM_UNIXTIME(updated_at), '%d.%m.%Y')" => $this->updated_at]);

        return $dataProvider;
    }
}
