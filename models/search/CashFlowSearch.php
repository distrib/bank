<?php

namespace app\models\search;

use app\models\Card;
use app\models\CashFlow;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * EventToUsersSearch represents the model behind the search form of `app\models\EventToUsers`.
 */
class CashFlowSearch extends CashFlow
{
    public $sum;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sender', 'recipient', 'updated_at'], 'integer'],
            [['sum'], 'number','min' => '0.01'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id)
    {
        $query = CashFlow::find()
            ->joinWith(['recipientCard recipientCard','senderCard senderCard'])
            ->where(['sender'=>$id])
            ->orWhere(['recipient' => $id])
            ->orderBy(['cash_flow.id'=> SORT_DESC]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
