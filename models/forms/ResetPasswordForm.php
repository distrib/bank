<?php
namespace app\models\forms;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use app\models\User;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $password_confirm;

    /**
     * @var app\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws InvalidArgumentException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidArgumentException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password','password_confirm'], 'required'],
            [['password'], 'string', 'min' => 8],
            [['password'], 'validatePassword'],
            ['password_confirm', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_confirm' => 'Подтвердите пароль'
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }

    public function validatePassword()
    {
        if( preg_match('/[A-Za-zА-Яа-я]/', $this->password) && preg_match('/[0-9]/', $this->password) ){

        }else{
            $this->addError('password', 'Необходимо наличие букв и цифр');
        }
    }

}
