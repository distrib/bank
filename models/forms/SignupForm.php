<?php
namespace app\models\forms;

use app\models\UserCars;
use app\models\UserProfile;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $gender;
    public $lastname;
    public $firstname;
    public $middlename;
    public $phone;
    public $email;
    public $card_number;
    public $vin;
    public $connection;

    private $error = [];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'phone', 'gender', 'email'], 'required'],
            [['firstname', 'lastname', 'middlename'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            //['email', 'unique', 'targetClass ' => User::className()],
            [['gender'], 'in', 'range' => [ UserProfile::GENDER_FEMALE, UserProfile::GENDER_MALE]],
//            ['password', 'required'],
//            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'middlename' => 'Отчество',
            'phone' => 'Телефон',
            'gender' => 'Пол',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
            'user_id' => 'Юзер',
        ];
    }

    /**
     * 1 - Vin занят;
     * 2 - Пользователь с таким емейлом уже есть.
     * 3 - Пользователь с таким телефоном уже есть.
     * 4 - Повторная регистрация; Повторяю отправку.
     * 5 - Пользователь создан

     *
     * @return int
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        $phone = preg_replace('/\D/', '', $this->phone);

        if ($user = User::findByEmailSignUp($this->email))
        {
            return $this->error[] = 'User already exist';
        }
        else
        {
            $user = new User();
            $user->username = $this->email;
            $user->email = $this->email;
            $user->status = User::STATUS_ACTIVE;
            $user->auth_key = Yii::$app->security->generateRandomString();
            $user->password_hash = Yii::$app->security->generatePasswordHash(123456789);//Yii::$app->security->generateRandomString(10));

            if( !$user->save() ){
                return $this->error[] = 'Ошибка регистрации, обратитесь через форму обращений';
            }

            $profile = new UserProfile();
            $profile->user_id = $user->id;
            $profile->firstname = $this->firstname;
            $profile->lastname = $this->lastname;
            $profile->middlename = $this->middlename;
            $profile->phone = $phone;
            $profile->gender = $this->gender;

            if( !$profile->save() ){
                return $this->error[] = 'Ошибки при регистрации, проверьте свои данные в личном кабинете';
            }

            return $user->id;
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
//    protected function sendEmail($user)
//    {
//        return Yii::$app
//            ->mailer
//            ->compose(
//                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
//                ['user' => $user]
//            )
//            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
//            ->setTo($this->email)
//            ->setSubject('Account registration at ' . Yii::$app->name)
//            ->send();
//    }

}
