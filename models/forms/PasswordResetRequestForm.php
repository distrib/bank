<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;
use yii\helpers\Html;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message'=>'Некорректный формат E-mail'],
            ['email', 'exist',
                'targetClass' => '\app\models\User',
                'filter' => ['status' => [User::STATUS_ACTIVE, User::STATUS_RESTORED]],
                'message' => 'Такого e-mail адреса нет в базе данных. Пожалуйста, проверьте правильность написания логина.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::find()
            ->andWhere(['status' => [User::STATUS_ACTIVE, User::STATUS_RESTORED]])
            ->andWhere(['email' => $this->email])
            ->one();

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

//        return Yii::$app
//            ->mailer
//            ->compose(
//                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
//                ['user' => $user]
//            )
//            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
//            ->setTo($this->email)
//            ->setSubject('Password reset for ' . Yii::$app->name)
//            ->send();

        $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);

        Yii::$app->expertSender->sendRestoreMessage($user, $resetLink);

        return true;

//        return Yii::$app->unisender->sendPersonalMail([
//            'email' => $user->email,
//            'subject' => 'Excellence Club BMW (Восстановление пароля)',
//            'body' => 'Уважаемый Иван!
//                Мы получили запрос о смене пароля для Вашей учетной записи на сайте ExcellenceClub.bmw.ru. <br>
//                Для восстановления пароля нажмите <a href="'.$resetLink.'">здесь</a>. <br>
//                Через 48 часов ссылка станет неактивной и для восстановления пароля потребуется отправить новый запрос.
//                Если восстановление пароля не требуется, оставьте это письмо без внимания.<br>
//                С уважением,<br>
//                BWM Excellence Club',
//        ]);
    }
}
