<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cash_flow".
 *
 * @property int $id
 * @property int $sender
 * @property int $recipient
 * @property double $sum
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $recipient0
 * @property User $sender0
 */
class CashFlow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_flow';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender', 'recipient'], 'required'],
            [['sender', 'recipient', 'created_at', 'updated_at'], 'integer'],
            //[['sum'], 'number'],
            [['sum'], 'number','min' => '0.01'],
            [['recipient'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['recipient' => 'id']],
            [['sender'], 'exist', 'skipOnError' => true, 'targetClass' => Card::className(), 'targetAttribute' => ['sender' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender' => 'Sender',
            'recipient' => 'Recipient',
            'sum' => 'Сумма',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipientCard()
    {
        return $this->hasOne(Card::className(), ['id' => 'recipient']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenderCard()
    {
        return $this->hasOne(Card::className(), ['id' => 'sender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultSender()
    {
        return User::findOne(['email'=>'excort13@mail.ru']); // обычно выносится в конфигурацию, а не вписывается на прямую
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTotalByCard($card)
    {
        return self::find()
            ->select('sum(if(sender='.$card.',-`sum`,`sum`)) as total')
            ->where(['sender'=>$card])
            ->orWhere(['recipient'=>$card])
            ->asArray()
            ->one();

        //echo '<pre>'; var_dump($query->createCommand()->getRawSql()); echo '<pre>';
    }

}
