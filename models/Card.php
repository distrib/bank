<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\CashFlow;

/**
 * This is the model class for table "card".
 *
 * @property int $id
 * @property int $user_id
 * @property string $card_number
 * @property int $month
 * @property int $year
 * @property string $pin
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property int $pass
 */
class Card extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'card';
    }

    public $pass;

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'card_number', 'pin'], 'required'],
            [['user_id',  'month', 'year',  'created_at', 'updated_at',], 'integer'],
            [['pin','card_number',], 'string'],
            [['pass'], 'match', 'pattern' => '/^[0-9]*$/', 'message' => 'Только цифры'],
            [['pass'],'string','min' => 4,'max' => 4,'message' => 'Не корректная длина. Должно быть 4 цифры',],
            [['card_number'], 'integer', 'integerOnly' => false],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'card_number' => 'Card Number',
            'month' => 'Month',
            'year' => 'Year',
            'pin' => 'Pin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getDefaultCard()
    {
        return self::findOne(['card_number'=>'1111111111111111']); // обычно выносится в конфигурацию, а не вписывается на прямую
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function checkPinByCard($cardId, $pass)
    {
        $curCard = self::findOne($cardId);

        if( $curCard )
        {
            return Yii::$app->security->validatePassword($pass, $curCard->pin);
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function isThisUserCard($cardId, $userId)
    {
        $curCard = self::findOne($cardId);

        if( $curCard )
        {
            if( $curCard->user->getId() == $userId ){
                return true;
            }
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardMinus()
    {
        return $this->hasMany(CashFlow::className(), ['sender' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardPlus()
    {
        return $this->hasMany(CashFlow::className(), ['recipient' => 'id']);
    }
}
